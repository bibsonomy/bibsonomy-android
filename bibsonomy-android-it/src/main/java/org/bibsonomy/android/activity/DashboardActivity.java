package org.bibsonomy.android.activity;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.App;
import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.R;
import org.bibsonomy.android.activity.service.LoginActivity;
import org.bibsonomy.android.activity.service.SettingsActivity;
import org.bibsonomy.android.adapter.DashboardAdapter;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.utils.CustomExceptionHandler;
import org.bibsonomy.android.view.ShelfView;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Represents the main starting point of the application. Acts as a dashboard to
 * get to either {@link BookmarkShelfDisplayActivity} or
 * {@link PublicationShelfDisplayActivity}.
 * 
 * @author Alex Plischke
 * 
 */
@Deprecated
public class DashboardActivity extends Activity {

    private static final String INTENT_HANDLED = "INTENT_HANDLED";

	@Override
    protected void onCreate(final Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	/*
    	 * set the default values for all preferences
    	 */
    	PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
	
		/*
		 * For beta testing purpose only.
		 * TODO: remove this part before release
		 */
		this.registerUncaughtExceptionHandler();
		
		setContentView(R.layout.main);
	
		final ShelfView shelfView = (ShelfView) findViewById(R.id.grid_shelfview);
		shelfView.setOnItemClickListener(new OnItemClickListener() {
	
		    @Override
		    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				Intent intent;
				switch (DashboardAdapter.DASHBOARD_TITLES[position]) {
				case R.string.bookmarks:
				    intent = new Intent(DashboardActivity.this, BookmarkShelfDisplayActivity.class);
				    startActivity(intent);
				    break;
				case R.string.publications:
				    intent = new Intent(DashboardActivity.this, PublicationShelfDisplayActivity.class);
				    startActivity(intent);
				    break;
				case R.string.tags:
					// TODO:
					break;
				default:
				    break;
				}
		    }
		});
	
		final DashboardAdapter dashboardAdapter = new DashboardAdapter(this);
		shelfView.setAdapter(dashboardAdapter);

		/*
		 * If credentials are not set, start the login activity.
		 */
		if (!App.validSettings(this)) {
		    final Intent startLogin = new Intent(this, LoginActivity.class);
		    startActivityForResult(startLogin, 0);
		}
		
		final Intent intent = this.getIntent();
		if (present(intent) && (!present(savedInstanceState) || !savedInstanceState.getBoolean(INTENT_HANDLED, false))) {
			handleIntent(intent);
		}
    }

	private void handleIntent(final Intent intent) {
		if (intent.getBooleanExtra(Intents.ASK_FOR_SYNC, false)) {
			intent.removeExtra(Intents.ASK_FOR_SYNC);
			final Builder builder = new Builder(this);
			final String syncTitle = this.getString(R.string.sync);
			builder.setTitle(syncTitle);
			builder.setMessage(R.string.first_sync);
			builder.setPositiveButton(syncTitle, new OnClickListener() {
				
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					final Intent syncIntent = new Intent(DashboardActivity.this, BibsonomyService.class);
					syncIntent.setAction(Intents.ACTION_SYNC);
					startService(syncIntent);
				}
			});
			builder.setNegativeButton(R.string.cancel, null);
			builder.show();
		}
	}

	protected void registerUncaughtExceptionHandler() {
		/*
		 * register only one handler 
		 */
		if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
			Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(App.LOGGING_SERVICE));
		}
	}
	
	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putBoolean(INTENT_HANDLED, true);
	}

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
    	this.getMenuInflater().inflate(R.menu.dashboard, menu);
    	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
    	Intent intent;

    	switch (item.getItemId()) {
    	case R.id.menu_item_sync:
		    intent = new Intent(this, BibsonomyService.class);
		    intent.setAction(Intents.ACTION_SYNC);
		    startService(intent);
		    break;
    	case R.id.menu_item_settings:
		    intent = new Intent(this, SettingsActivity.class);
		    startActivity(intent);
		    break;
    	}

    	return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    	if (resultCode == RESULT_CANCELED) {
    		finish();
    	}
    }

}
