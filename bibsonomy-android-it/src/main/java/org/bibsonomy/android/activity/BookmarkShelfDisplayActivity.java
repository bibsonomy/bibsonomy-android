package org.bibsonomy.android.activity;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.ThirdPartyApps;
import org.bibsonomy.android.R;
import org.bibsonomy.android.activity.details.VisitBookmarkActivity;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditBookmarkActivity;
import org.bibsonomy.android.activity.service.SettingsActivity;
import org.bibsonomy.android.adapter.BookmarkAdapter;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.BookmarkColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.util.UrlUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Browser;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * {@link Activity} for displaying {@link Bookmark}s on a shelf.
 * 
 * @author Alex Plischke
 * 
 */
public class BookmarkShelfDisplayActivity extends AbstractPostShelfActivity<Bookmark> {
	private static final int REQUEST_CODE = 42;
	private static final String QRCODE_NOT_VALID_DIALOG = "qrCodeNotValidDialog";
	private static final String NO_BOOKMARKS_DIALOG = "noBookmarksFoundDialog";
	private static final String BOOKMARK_IMPORT_DIALOG = "bookmarkImportDialog";
	private static final String BOOKMARK_IMPORT_TITLES = "bookmarkImportTitles";
	private static final String BOOKMARK_IMPORT_URLS = "bookmarkImportURLs";
	private static final String BOOKMARK_IMPORT_CHECKED = "bookmarkImportChecked";
	
	private static final String[] BOOKMARK_PROJECTION = { ResourceColumns._ID, ResourceColumns.TITLE, ResourceColumns.INTRAHASH, BookmarkColumns.URL };
	
	private static final String mSelectionRestricted = ResourceColumns.TITLE + " LIKE ?";
    
    private String[] mBookmarkTitles;
    private String[] mBookmarkURLs;
    private boolean[] mBookmarksChecked;

    private AlertDialog mNoBoomarksFoundDialog;
    private AlertDialog mBookmarkImportDialog;
    private AlertDialog mQRCodeNotValidDialog;
    
    @Override
    protected Class<Bookmark> getResourceType() {
    	return Bookmark.class;
    }
    
    @Override
    public void onItemClick(final AdapterView<?> paramAdapterView, final View paramView, final int position, final long id) {
    	this.cursor.moveToPosition(position);
    	final String intraHash = this.cursor.getString(2);
    	final String urlString = this.cursor.getString(3);
    	
    	final Intent intent = new Intent(this, VisitBookmarkActivity.class);
    	intent.putExtra(Intents.EXTRA_URL, urlString);
    	intent.putExtra(Intents.EXTRA_INTRAHASH, intraHash);
    	this.startActivity(intent);
    }

    protected void restoreInstanceState(final Bundle savedInstanceState) {
    	if (savedInstanceState.getBoolean(NO_BOOKMARKS_DIALOG)) {
	    	showNoBookmarksFoundDialog();
	    } else if (savedInstanceState.getBoolean(BOOKMARK_IMPORT_DIALOG)) {
	    	this.mBookmarkTitles = savedInstanceState.getStringArray(BOOKMARK_IMPORT_TITLES);
	    	this.mBookmarkURLs = savedInstanceState.getStringArray(BOOKMARK_IMPORT_URLS);
	    	this.mBookmarksChecked = savedInstanceState.getBooleanArray(BOOKMARK_IMPORT_CHECKED);
	    	showBrowserImportDialog(mBookmarkTitles, mBookmarkURLs, mBookmarksChecked);
	    } else if (savedInstanceState.getBoolean(QRCODE_NOT_VALID_DIALOG)) {
	    	showQRCodeNotValidDialog();
	    }
	}

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (mBookmarkImportDialog != null && mBookmarkImportDialog.isShowing()) {
    		outState.putBoolean(BOOKMARK_IMPORT_DIALOG, true);
    		outState.putStringArray(BOOKMARK_IMPORT_TITLES, mBookmarkTitles);
    		outState.putStringArray(BOOKMARK_IMPORT_URLS, mBookmarkURLs);
    		outState.putBooleanArray(BOOKMARK_IMPORT_CHECKED, mBookmarksChecked);
    	}
    	
    	outState.putBoolean(NO_BOOKMARKS_DIALOG, mNoBoomarksFoundDialog != null && mNoBoomarksFoundDialog.isShowing());
    	outState.putBoolean(QRCODE_NOT_VALID_DIALOG, mQRCodeNotValidDialog != null && mQRCodeNotValidDialog.isShowing());
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
    	getMenuInflater().inflate(R.menu.bookmark_shelf_menu, menu);
    	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
    	super.onCreateContextMenu(menu, v, menuInfo);
    	getMenuInflater().inflate(R.menu.bookmark_shelf_context, menu);

		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		final TextView titleView = (TextView) info.targetView.findViewById(R.id.title);
		menu.setHeaderTitle(titleView.getText());
    }
    
    @Override
    protected Class<? extends AbstractEditPostActivity<Bookmark>> getEditActivityClass() {
    	return EditBookmarkActivity.class;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
		Intent intent;
	
		switch (item.getItemId()) {
		case R.id.menu_item_add:
		    intent = new Intent(this, EditBookmarkActivity.class);
		    startActivity(intent);
		    break;
		case R.id.menu_item_scan:
		    intent = new Intent(ThirdPartyApps.ZXing.INTENT_NAME);
		    intent.setPackage(ThirdPartyApps.ZXing.PACKAGE);
		    intent.putExtra(ThirdPartyApps.ZXing.EXTRA_SCAN_MODE, ThirdPartyApps.ZXing.QR_CODE);
	
		    try {
		    	startActivityForResult(intent, REQUEST_CODE);
		    } catch (final ActivityNotFoundException e) {
				this.createZXIngDialog().show();
		    }
		    break;
		case R.id.menu_item_sync:
		    intent = new Intent(this, BibsonomyService.class);
		    intent.setAction(Intents.ACTION_SYNC);
		    startService(intent);
		    break;
		case R.id.menu_item_search:
		    this.onSearchRequested();
		    break;
		case R.id.menu_item_settings:
		    intent = new Intent(this, SettingsActivity.class);
		    startActivity(intent);
		    break;
		case R.id.menu_item_import:
		    showBrowserImportDialog(null, null, null);
		    break;
		default:
		    break;
		}
	
		return super.onOptionsItemSelected(item);
    }

	/**
     * Shows an {@link AlertDialog} to import bookmarks from the Android
     * {@link Browser}. Input parameters are optional, but if set, they must be
     * of equal length and contain no <code>null</code> values.
     * 
     * 
     * @param titles
     *            titles of the bookmarks (optional)
     * @param urls
     *            corresponding urls of the bookmarks (optional)
     * @param checked
     *            indicates whether any of the bookmarks are preselected for
     *            import (optional)
     */
    private void showBrowserImportDialog(final String[] titles, final String[] urls, final boolean[] checked) {
    	// TODO: move to method
    	Cursor c = null;
    	if (titles == null || urls == null || checked == null) {
    		c = managedQuery(Browser.BOOKMARKS_URI, new String[] {
    			android.provider.Browser.BookmarkColumns._ID, android.provider.Browser.BookmarkColumns.BOOKMARK,
		    	android.provider.Browser.BookmarkColumns.TITLE, android.provider.Browser.BookmarkColumns.URL },
		    	android.provider.Browser.BookmarkColumns.BOOKMARK + "=?", new String[] { "1" }, null);
		}
    	// TODO: stop
		
    	final int length = (titles == null) ? c.getCount() : titles.length;
		if (length == 0) {
			showNoBookmarksFoundDialog();
	    	return;
		}
		
		final String[] bookmarkTitles = (titles == null) ? new String[length] : titles;
		final String[] bookmarkURLs = (urls == null) ? new String[length] : urls;
		final boolean[] bookmarksChecked = (checked == null) ? new boolean[length] : checked;

		if (c != null) {
		    for (int i = 0; c.moveToNext(); i++) {
		    	bookmarkTitles[i] = c.getString(2);
		    	bookmarkURLs[i] = c.getString(3);
		    }
		}

		// TODO: start move
		mBookmarkTitles = bookmarkTitles;
		mBookmarkURLs = bookmarkURLs;
		mBookmarksChecked = bookmarksChecked;
		
		// TODO: end

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(final DialogInterface dialog, final int which) {
		    	for (int i = 0; i < length; i++) {
		    		bookmarkTitles[i] = null;
		    		bookmarkURLs[i] = null;
		    	}

				startImportService(bookmarkTitles, bookmarkURLs, bookmarksChecked);
		    }
		});
		builder.setNeutralButton(R.string.add_all, new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(final DialogInterface dialog, final int which) {
		    	final boolean[] allIn = new boolean[length];
		    	for (int i = 0; i < length; i++) {
		    		allIn[i] = true;
		    	}
		    	startImportService(bookmarkTitles, bookmarkURLs, allIn);
		    }
		});
		builder.setNegativeButton(R.string.cancel, null);
		builder.setMultiChoiceItems(bookmarkTitles, bookmarksChecked, new OnMultiChoiceClickListener() {

		    @Override
		    public void onClick(final DialogInterface dialog, final int which, final boolean isChecked) {
		    	bookmarksChecked[which] = isChecked;
		    }
		});
		mBookmarkImportDialog = builder.show();
    }

    /**
     * Starts {@link BibsonomyService} with the request the construct and add
     * new {@link Bookmark}s out of the given titles/urls.
     * 
     * @param titles
     *            the titles
     * @param urls
     *            the urls
     * @param checked TODO: unused
     *            whether the user selected to import the {@link Bookmark} out
     *            of the browser
     */
    private void startImportService(final String[] titles, final String[] urls, final boolean[] checked) {
    	final Intent intent = new Intent(this, BibsonomyService.class);
    	intent.setAction(Intents.ACTION_IMPORT_BOOKMARKS);
    	intent.putExtra(Intents.EXTRA_TITLES, titles);
    	intent.putExtra(Intents.EXTRA_URLS, urls);
    	startService(intent);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if (requestCode == REQUEST_CODE) {
    		if (resultCode == RESULT_OK) {
    			final String scanResult = UrlUtils.cleanUrl(data.getStringExtra(ThirdPartyApps.ZXing.EXTRA_RESULT));    			
    			if (present(scanResult)) {    				
				    final Intent intent = new Intent(this, EditBookmarkActivity.class);
				    final Post<Bookmark> post = new Post<Bookmark>();
				    final Bookmark bookmark = new Bookmark();
				    post.setResource(bookmark);
				    bookmark.setUrl(scanResult);
				    intent.setAction(Intents.ACTION_CREATE_POST);
				    intent.putExtra(Intents.EXTRA_POST, new PostParcel<Bookmark>(post));
				    this.startActivity(intent);
    			} else {
    				this.showQRCodeNotValidDialog();
    			}
    		}
    	}
    }

    /**
     * Shows a dialog to inform the user that the QR-Code was not valid.
     */
    private void showQRCodeNotValidDialog() {
    	final AlertDialog.Builder builder = getDefaultAlertDialog(this);
    	builder.setTitle(R.string.failed).setMessage(R.string.qrcode_url_not_valid);
    	mQRCodeNotValidDialog = builder.show();
    }
    
    private static final AlertDialog.Builder getDefaultAlertDialog(final Context context) {
    	final AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	return builder.setPositiveButton(R.string.ok, null);
    }

    /**
     * Shows a dialog to inform the user that no {@link Bookmark}s were found to
     * import from the browser.
     */
    private void showNoBookmarksFoundDialog() {
    	final AlertDialog.Builder builder = getDefaultAlertDialog(this);
    	builder.setMessage(R.string.no_bookmarks_to_import);
    	mNoBoomarksFoundDialog = builder.show();
    }

	@Override
	protected String[] getProjection() {
		return BOOKMARK_PROJECTION;
	}
	
	@Override
	protected Drawable createDefaultDrawable() {
		final Resources resources = getResources();
		return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, R.drawable.web_page));
	}

	@Override
	protected CursorAdapter createAdapter(final Cursor cursor, final Drawable defaultDrawable) {
		return new BookmarkAdapter(this, cursor, defaultDrawable);
	}

	@Override
	protected String getDefaultSortOrder() {
		return Database.BOOKMARKS_DEFAULT_SORT_ORDER;
	}

	@Override
	protected String getRestictedSelection() {
		return mSelectionRestricted;
	}
}
