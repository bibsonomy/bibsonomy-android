package org.bibsonomy.android.activity;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.android.App;
import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.App.ThirdPartyApps;
import org.bibsonomy.android.R;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditPublicationActivity;
import org.bibsonomy.android.activity.service.GoogleCoverSearchActivity;
import org.bibsonomy.android.activity.service.SettingsActivity;
import org.bibsonomy.android.adapter.PublicationAdapter;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.PublicationColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.task.ScrapePublicationTask;
import org.bibsonomy.android.utils.AsyncTask.AsyncTaskCallback;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.NetworkUtils;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.android.view.ShelfView;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.TextView;

/**
 * Uses a {@link GridView} to display all {@link BibTex} in a {@link ShelfView}.
 * 
 * @author Alex Plischke
 * 
 */
public class PublicationShelfDisplayActivity {
	private static final int REQUEST_CODE = 42;
	
	private static final String PROGRESS_SHOWING = "progressShowing";
	private static final String ISBN_NOT_FOUND_DIALOG = "isbnNotFoundDialog";
	private static final String NO_NETWORK_DIALOG = "noNetworkDialog";
	
	private static final Pattern SYSTEM_PATTERN = Pattern.compile("https?://[www.]?" + App.WEB_SERVICE + "/[bibtex|publication]/2([0-9a-fA-F]{32})/(.+)");
	
	private static final String[] PUBLICATION_PROJECTION = { ResourceColumns._ID, PublicationColumns.CLEANED_TITLE, ResourceColumns.INTRAHASH, PublicationColumns.AUTHOR };

	private static final String mSelectionRestricted = ResourceColumns.TITLE + " LIKE ? OR " + PublicationColumns.AUTHOR + " LIKE ?";
	
	
    
    
    @Override
    protected Class<BibTex> getResourceType() {
    	return BibTex.class;
    }
    
    @Override
    protected Drawable createDefaultDrawable() {
    	final Resources resources = getResources();
		return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, R.drawable.page_blank));
    }
    
    @Override
    protected CursorAdapter createAdapter(final Cursor cursor, final Drawable defaultDrawable) {
    	return new PublicationAdapter(this, cursor, defaultDrawable);
    }
    
    @Override
    protected String[] getProjection() {
    	return PUBLICATION_PROJECTION;
    }
    
    @Override
    public void onItemClick(final AdapterView<?> parentView, final View view, final int position, final long id) {
    	// TODO: show details view
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	outState.putBoolean(PROGRESS_SHOWING, this.scrapeProgressDialog != null && this.scrapeProgressDialog.isShowing());
    	outState.putBoolean(ISBN_NOT_FOUND_DIALOG, this.scrapingFailedDialog != null && this.scrapingFailedDialog.isShowing());
    	outState.putBoolean(NO_NETWORK_DIALOG, noNetworkDialog != null && noNetworkDialog.isShowing());
    }
    
    @Override
	protected void restoreSavedInstanceState(final Bundle savedInstanceState) {
    	if (savedInstanceState.getBoolean(PROGRESS_SHOWING)) {
			this.scrapeTask = (ScrapePublicationTask) getLastNonConfigurationInstance();
			this.showProgressDialog();
	    } else if (savedInstanceState.getBoolean(ISBN_NOT_FOUND_DIALOG)) {
	    	this.scrapingFailedDialog();
	    } else if (savedInstanceState.getBoolean(NO_NETWORK_DIALOG)) {
	    	this.showNoNetworkAlertDialog();
	    }
    }

    private void showProgressDialog() {
    	this.scrapeProgressDialog = ProgressDialog.show(this, getText(R.string.fetching_data), getText(R.string.in_progress), true, false);
	}

	private void scrapingFailedDialog() {
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.failed);
    	builder.setMessage(R.string.no_entries_found);
    	builder.setPositiveButton(R.string.ok, null);
    	this.scrapingFailedDialog = builder.show();
	}

	private void showNoNetworkAlertDialog() {
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.failed).setMessage(R.string.error_no_network).setPositiveButton(R.string.ok, null);
    	this.noNetworkDialog = builder.show();
	}

    @Override
    public Object onRetainNonConfigurationInstance() {
    	return this.scrapeTask;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
    	getMenuInflater().inflate(R.menu.bibtex_shelf_menu, menu);
    	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
    	super.onCreateContextMenu(menu, v, menuInfo);
    	getMenuInflater().inflate(R.menu.bibtex_shelf_context, menu);

    	final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
    	final TextView titleView = (TextView) info.targetView.findViewById(R.id.title);
    	menu.setHeaderTitle(titleView.getText());
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {    	
    	if (item.getItemId() == R.id.context_item_search_cover) {
    		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    		this.cursor.moveToPosition(info.position);
    		final Intent intent = new Intent(this, GoogleCoverSearchActivity.class);
		    
    		final String title = this.cursor.getString(1);
    		final String intrahash = this.cursor.getString(2);
		    intent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
		    intent.putExtra(PublicationColumns.TITLE, title);
	    	this.startService(intent);
		    return true;
    	}

    	return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
		Intent intent;
	
		switch (item.getItemId()) {
		case R.id.menu_item_add:
		    intent = new Intent(this, EditPublicationActivity.class);
		    startActivity(intent);
		    break;
		case R.id.menu_item_scan:
		    intent = new Intent(ThirdPartyApps.ZXing.INTENT_NAME);
		    intent.setPackage(ThirdPartyApps.ZXing.PACKAGE);
		    intent.putExtra(ThirdPartyApps.ZXing.EXTRA_SCAN_FORMATS, ThirdPartyApps.ZXing.EAN_13_FORMAT + "," + ThirdPartyApps.ZXing.QR_FORMAT);
	
		    try {
		    	startActivityForResult(intent, REQUEST_CODE);
		    } catch (final ActivityNotFoundException e) {
		    	this.createZXIngDialog().show();
		    }
		    break;
		case R.id.menu_item_sync:
		    intent = new Intent(this, BibsonomyService.class);
		    intent.setAction(Intents.ACTION_SYNC);
		    startService(intent);
		    break;
		case R.id.menu_item_search:
		    onSearchRequested();
		    break;
		case R.id.menu_item_settings:
		    intent = new Intent(this, SettingsActivity.class);
		    startActivity(intent);
		    break;
		default:
		    break;
		}
	
		return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected Class<? extends AbstractEditPostActivity<BibTex>> getEditActivityClass() {
    	return EditPublicationActivity.class;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == REQUEST_CODE) {
		    if (resultCode == RESULT_OK) {
				final String scanResult = data.getStringExtra(ThirdPartyApps.ZXing.EXTRA_RESULT);
				
				if (present(scanResult)) {
					final Matcher matcher = SYSTEM_PATTERN.matcher(scanResult);
					
					if (matcher.matches()) {
						final String intrahash = matcher.group(1);
						final String username = matcher.group(2);
						
						final Cursor cursor = this.managedQuery(ContentUrisUtils.withAppendedId(ItemProvider.POST_CONTENT_URI, intrahash), null, null, null, null);
						final int count = cursor.getCount();
						cursor.close();
						
						final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
						final String loggedInUser = prefs.getString(Settings.USERNAME, "");
						final List<String> options = new LinkedList<String>();
						final boolean inDb = count > 0;
						final boolean ownEntry = inDb && loggedInUser.equalsIgnoreCase(username);
						if (inDb) {
							// show, edit and delete
							options.add(getString(R.string.show_publication_details));
							options.add(getString(R.string.edit_publication));
							options.add(getString(R.string.delete_publication));
						} else if (ownEntry) {
							// FIXME: sync 
						} else {
							// copy to own collection
							options.add(getString(R.string.copy_publication));
							
						}
						options.add("add to clipboard");
						options.add(getString(R.string.cancel));
						
						final Builder dialog = new Builder(this);
					    dialog.setTitle(getString(R.string.options));
					    dialog.setItems(options.toArray(new String[options.size()]), new DialogInterface.OnClickListener() {
					        @Override
							public void onClick(final DialogInterface dialog, final int item) {
					            if (ownEntry) {
					            	switch (item) {
									case 0:
										// TODO: show publication details activity
										break;
					            	case 1:
					            		startEditAction(intrahash);
					            		break;
					            	case 2:
					            		deletePost(intrahash);
					            		break;
					            	}
					            } else {
					            	switch (item) {
									case 0:
										// copy first scrape the publication
										// TODO: use the api for that request?
										scrape(scanResult, null);
										break;
									}
					            }
					            // else cancel 
					        }
					    }).show();
					} else {
						// try to scrape with isbn
						this.scrape(null, scanResult);
					}
				} else {
					// TODO: show error message? canceled
				}
		    }
		}
    }
    
    /**
	 * @param url
     * @param selectedText
	 */
	protected void scrape(final String url, final String selectedText) {
		if (NetworkUtils.isNetworkAvailable(this)) {
			/*
			 * show progress dialog
			 */
			this.scrapeTask = new ScrapePublicationTask();
			this.scrapeTask.setCallback(new AsyncTaskCallback<Post<BibTex>>() {

				@Override
				public void gotResult(final Post<BibTex> result) {
					if (result == null) {
						scrapeProgressDialog.dismiss();
						scrapingFailedDialog();
						return;
					}
					
					startEditAction(result);
				}
				
			});
			this.scrapeTask.execute(url, selectedText);
		} else {
			this.showNoNetworkAlertDialog();
		}
		
		this.showProgressDialog();
	}

	protected void startEditAction(final Post<BibTex> result) {
		final Intent intent = new Intent(PublicationShelfDisplayActivity.this, EditPublicationActivity.class);
		intent.setAction(Intents.ACTION_CREATE_POST);
		intent.putExtra(Intents.EXTRA_POST, new PostParcel<BibTex>(result));
		startActivity(intent);
	}

	@Override
	protected String getDefaultSortOrder() {
		return Database.PUBLICATIONS_DEFAULT_SORT_ORDER;
	}

	@Override
	protected String getRestictedSelection() {
		return mSelectionRestricted;
	}
}
