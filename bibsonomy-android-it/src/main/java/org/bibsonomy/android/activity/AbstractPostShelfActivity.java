package org.bibsonomy.android.activity;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.App.ThirdPartyApps;
import org.bibsonomy.android.R;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.adapter.holder.ResourceHolder;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.FileManager;
import org.bibsonomy.android.utils.gesture.BackSwipeListener;
import org.bibsonomy.android.view.ShelfView;
import org.bibsonomy.model.factories.ResourceFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageButton;

/**
 * 
 * @author dzo
 * @param <RESOURCE> 
 */
public abstract class AbstractPostShelfActivity<RESOURCE extends org.bibsonomy.model.Resource> extends Activity implements OnTouchListener, OnItemClickListener, OnScrollListener, OnItemSelectedListener {	
	
	private static final StringBuilder mBuddy = new StringBuilder();
	private static final String[] selectionArgsRestricted = new String[2];
	
	private static final int LOAD_COVERS_MESSAGE_ID = 84;
	private static final int DELAY_SHOW_COVERS = 550;
	
	
	protected static enum Sort {
		TITLE,
		POSTING_DATE,
		CHANGE_DATE,
		AUTHORS_EDITORS,
		YEAR
	}
	
	
	private int scrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
	private boolean pendingCoverUpdate = false;

	private GestureDetector gestureDetector;
	
	protected Cursor cursor;
	protected CursorAdapter adapter;
	
	private ShelfView shelfView;
	private CoverHandler handler;
	private Drawable defaultDrawable;
	private boolean fingerUp;
	private Sort sort;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);
		
		if (present(savedInstanceState)) {
			this.restoreSavedInstanceState(savedInstanceState);
		}
		
		this.shelfView = (ShelfView) findViewById(R.id.grid_shelfview);
		this.registerForContextMenu(this.shelfView);
		this.shelfView.setOnTouchListener(this);
		this.shelfView.setOnItemClickListener(this);
		this.handler = new CoverHandler();
		
		this.sort = Sort.TITLE;
		this.defaultDrawable = this.createDefaultDrawable();
		
		final ImageButton tagButton = (ImageButton) this.findViewById(R.id.tagSelector);
		tagButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				final Intent chooseTagsIntent = new Intent(AbstractPostShelfActivity.this, ChooseTagsActivity.class);
				startActivityForResult(chooseTagsIntent, 42);
			}
		});
		
		this.shelfView.setOnScrollListener(this);
		this.shelfView.setOnItemSelectedListener(this);
		
		this.gestureDetector = new GestureDetector(this, BackSwipeListener.createBackSwipeListener(this));
	}

	protected void restoreSavedInstanceState(final Bundle savedInstanceState) {
		// noop
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		this.updateQuery();
	}

	private void updateQuery() {
		if (present(this.cursor)) {
			this.cursor.close();
		}
		
		final android.net.Uri.Builder builder = ItemProvider.POST_CONTENT_URI.buildUpon();
		ContentUrisUtils.appendParam(builder, ItemProvider.RESOUCRE_TYPE_PARAM, ResourceFactory.getResourceName(this.getResourceType()));
		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		final String tags = prefs.getString(Settings.FILTERED_TAGS, null);
		if (present(tags)) {
			ContentUrisUtils.appendParam(builder, ItemProvider.TAGS_PARAM, tags);
		}
		this.cursor = this.managedQuery(builder.build(), this.getProjection(), null, null, this.getDefaultSortOrder());
		
		this.adapter = this.createAdapter(this.cursor, this.defaultDrawable);
		this.shelfView.setAdapter(this.adapter);
	}

	protected abstract Class<RESOURCE> getResourceType();
	
	protected abstract String[] getProjection();

	protected abstract Drawable createDefaultDrawable();
	
	protected abstract CursorAdapter createAdapter(final Cursor cursor, final Drawable defaultDrawable);

	@Override
    public boolean onTouch(final View v, final MotionEvent event) {
    	if (this.gestureDetector.onTouchEvent(event)) {
    		finish();
    		return true;
    	}
    	
    	final int action = event.getAction();
    	this.fingerUp = action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL;
    	
    	if (this.fingerUp && this.scrollState != SCROLL_STATE_FLING) {
            this.initCoverLoading();
        }
    	
    	return false;
    }
	
	private void initCoverLoading() {		
		final Handler handler = this.handler;
		final Message message = handler.obtainMessage(LOAD_COVERS_MESSAGE_ID, this);
        handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
        this.pendingCoverUpdate = true;
        handler.sendMessage(message);
	}
	
	private void loadCovers() {
		this.pendingCoverUpdate = false;
		final GridView gridView = this.shelfView;
		
		final int firstVisiblePosition = gridView.getFirstVisiblePosition();
		final int lastVisiblePosition = gridView.getLastVisiblePosition();
		
		for (int i = firstVisiblePosition; i <= lastVisiblePosition; i++) {
			final View view = gridView.getChildAt(i);
			if (!present(view)) {
				// TODO: check me
				Log.e("", "position was " + i + " (" + firstVisiblePosition + "-" + lastVisiblePosition + ")");
				continue;
			}
            final ResourceHolder holder = (ResourceHolder) view.getTag();
            if (holder.queryCover) {
            	final String intraHash = holder.intraHash;
            	
            	Drawable cover = FileManager.getCover(this.getResources(), intraHash);
            	if (cover == null) {
            		cover = this.defaultDrawable;
            	}
            	
            	holder.image.setImageDrawable(cover);
            	holder.queryCover = false;
            }
		}
	}

	@Override
    protected void onNewIntent(final Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {			
		    final String query = intent.getStringExtra(SearchManager.QUERY);
		    final StringBuilder buddy = mBuddy;
		    buddy.setLength(0);
		    final String selectionRestricted = buddy.append('%').append(query).append('%').toString();
			selectionArgsRestricted[0] = selectionRestricted;
		    selectionArgsRestricted[1] = selectionRestricted;
		    
		    this.cursor.close();
		    this.cursor = managedQuery(ItemProvider.POST_CONTENT_URI, this.getProjection(), this.getRestictedSelection(), selectionArgsRestricted, this.getDefaultSortOrder());
		    this.adapter.changeCursor(this.cursor);
		}
    }
	
	@Override
    public boolean onContextItemSelected(final MenuItem item) {
    	final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    	this.cursor.moveToPosition(info.position);
    	final String intrahash = this.cursor.getString(2);
		switch (item.getItemId()) {
		case R.id.context_item_edit:
			this.startEditAction(intrahash);
			return true;
		case R.id.context_item_delete:
		    this.deletePost(intrahash);
		    return true;
		}
		
		return super.onContextItemSelected(item);
    }

	/**
	 * @param intrahash
	 */
	protected void deletePost(final String intrahash) {
		final Intent intent = new Intent(this, BibsonomyService.class);
		intent.setAction(Intents.ACTION_DELETE_POST);
		intent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
		this.startService(intent);
	}

	/**
	 * @param intrahash
	 */
	protected void startEditAction(final String intrahash) {
		final Intent editIntent = new Intent(this, this.getEditActivityClass());
		editIntent.setAction(Intents.ACTION_UPDATE_POST);
		editIntent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
		this.startActivity(editIntent);
	}

	protected abstract Class<? extends AbstractEditPostActivity<RESOURCE>> getEditActivityClass();

	protected abstract String getDefaultSortOrder();

	protected abstract String getRestictedSelection();

	/**
	 * @return the builder for the dialog
	 * 
	 */
	protected Builder createZXIngDialog() {
		final AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
		downloadDialog.setTitle(R.string.barcode_scanner);
		downloadDialog.setMessage(R.string.barcore_requirement);
		downloadDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(final DialogInterface dialogInterface, final int i) {
		    	final Intent intent = new Intent(Intent.ACTION_VIEW, ThirdPartyApps.ZXing.MARKET_URI);
		    	startActivity(intent);
		    }
		});
		downloadDialog.setNegativeButton(R.string.no, null);
		return downloadDialog;
	}
	
	@Override
	public void onScrollStateChanged(final AbsListView view, final int scrollState) {
		/*
		 * scroll fling to other scroll state 
		 */
		final Handler handler = this.handler;
		if (this.scrollState == SCROLL_STATE_FLING && scrollState != SCROLL_STATE_FLING) {
            final Message message = handler.obtainMessage(LOAD_COVERS_MESSAGE_ID, this);
            handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
            handler.sendMessageDelayed(message, this.fingerUp ? 0 : DELAY_SHOW_COVERS);
            this.pendingCoverUpdate = true;
        } else if (scrollState == SCROLL_STATE_FLING) {
            this.pendingCoverUpdate = false;
            handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
        }
		
		this.scrollState = scrollState;
	}
	
	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
		// nothing to do yet
	}
	
	private class CoverHandler extends Handler {
		@Override
		public void handleMessage(final Message msg) {
			switch (msg.what) {
			case LOAD_COVERS_MESSAGE_ID:
				final AbstractPostShelfActivity<?> activity = (AbstractPostShelfActivity<?>) msg.obj;
				activity.loadCovers();
				break;
			}
		}
	}
	
	@Override
	public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
		if (this.scrollState != SCROLL_STATE_IDLE) {
            this.scrollState = SCROLL_STATE_IDLE;
            this.initCoverLoading();
        }
	}
	
	@Override
	public void onNothingSelected(final AdapterView<?> adapterView) {
		// nothing to do
	}
	
	/**
	 * @return the pendingCoverUpdate
	 */
	public boolean isPendingCoverUpdate() {
		return pendingCoverUpdate;
	}
	
	/**
	 * @return the scrollState
	 */
	public int getScrollState() {
		return scrollState;
	}
}
