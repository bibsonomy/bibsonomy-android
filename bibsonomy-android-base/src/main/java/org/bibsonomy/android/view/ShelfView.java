package org.bibsonomy.android.view;

import org.bibsonomy.android.base.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 
 * @author Alex Plischke
 *
 */
public class ShelfView extends GridView {

    private Bitmap mBackground;
    private int mShelfWidth;
    private int mShelfHeight;

    /**
     * @param context the context
     */
    public ShelfView(final Context context) {
    	super(context);
    }

    /**
     * 
     * @param context
     * @param attrs
     */
    public ShelfView(final Context context, final AttributeSet attrs) {
    	super(context, attrs);
		this.setup(context, attrs, 0);
    }

    /**
     * 
     * @param context
     * @param attrs
     * @param defStyle
     */
    public ShelfView(final Context context, final AttributeSet attrs, final int defStyle) {
    	super(context, attrs, defStyle);
    	this.setup(context, attrs, defStyle);
    }

    private void setup(final Context context, final AttributeSet attrs, final int defStyle) {
		final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ShelfView, defStyle, 0);
		final int backgroundID = typedArray.getResourceId(R.styleable.ShelfView_shelfBackground, 0);
		final Bitmap shelfBackground = BitmapFactory.decodeResource(this.getResources(), backgroundID);
		if (shelfBackground != null) {
		    this.mShelfWidth = shelfBackground.getWidth();
		    this.mShelfHeight = shelfBackground.getHeight();
		    this.mBackground = shelfBackground;
		}
	
		typedArray.recycle();
    }

    @Override
    protected void dispatchDraw(final Canvas canvas) {
		final int count = this.getChildCount();
		final int top = count > 0 ? this.getChildAt(0).getTop() : 0;
		final int shelfWidth = this.mShelfWidth;
		final int shelfHeight = this.mShelfHeight;
		final int width = this.getWidth();
		final int height = this.getHeight();
		final Bitmap background = this.mBackground;
	
		for (int x = 0; x < width; x += shelfWidth) {
		    for (int y = top; y < height; y += shelfHeight) {
			canvas.drawBitmap(background, x, y, null);
		    }
		}
	
		super.dispatchDraw(canvas);
    }

}
