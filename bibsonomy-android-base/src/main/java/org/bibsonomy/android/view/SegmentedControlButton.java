package org.bibsonomy.android.view;

/*
 * Copyright 2011 Benjamin Ferrari
 * http://bookworm.at
 * https://github.com/bookwormat/segcontrol
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.bibsonomy.android.base.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.RadioButton;

/**
 * @author benjamin ferrari
 * @author dzo
 */
public class SegmentedControlButton extends RadioButton {

    private Drawable backgroundSelected;
    private Drawable backgroundUnselected;

    private int lineColor;
    private int lineColorUnselected;
    private int lineHeightSelected;
    private int lineHeightUnselected;

    private float centerScreen;

    private int textColorSelected;
    private int textColorUnselected;

    private int textDistanceFromLine;

    /** the drawings for the radiobutton */
    private Paint textPaint;
    private Paint line;
    
    /**
     * constructor
     * 
     * @param context context
     * @param attrs attributes
     */
    public SegmentedControlButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs);
    }
    
    /**
     * 
     * @param context
     * @param attrs
     * @param defStyle the style to use
     */
    public SegmentedControlButton(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        this.init(attrs);
    }

    private void init(final AttributeSet attrs) {
        if (attrs != null) {
            final TypedArray attributes = this.getContext().obtainStyledAttributes(attrs, R.styleable.SegmentedControlButton);
            if (this.backgroundSelected == null) {
                final Drawable d = attributes.getDrawable(R.styleable.SegmentedControlButton_backgroundSelected);
                this.backgroundSelected = d == null ? this.getBackground() : d;
            }

            if (this.backgroundUnselected == null) {
            	this.backgroundUnselected = this.getBackground();
            }            
            
            this.textColorUnselected = attributes.getColor(R.styleable.SegmentedControlButton_textColorUnselected, 0);
            this.textColorSelected = attributes.getColor(R.styleable.SegmentedControlButton_textColorSelected, 0);
            this.textDistanceFromLine = attributes.getDimensionPixelSize(R.styleable.SegmentedControlButton_textDistanceFromLine, 0);
            
            this.lineHeightUnselected = attributes.getDimensionPixelSize(R.styleable.SegmentedControlButton_lineHeightUnselected, 0);
            this.lineHeightSelected = attributes.getDimensionPixelSize(R.styleable.SegmentedControlButton_lineHeightSelected, 0);
            this.lineColor = attributes.getColor(R.styleable.SegmentedControlButton_lineColor, 0);
            this.lineColorUnselected = attributes.getColor(R.styleable.SegmentedControlButton_lineColorUnselected, 0);
            
            this.textPaint = new Paint();
            this.textPaint.setAntiAlias(true);
            this.textPaint.setTextSize(this.getTextSize());
            this.textPaint.setTextAlign(Paint.Align.CENTER);
            
            this.line = new Paint();
            
            this.line.setColor(this.getLineColor());
           
            this.line.setColor(this.lineColor);
            this.line.setStyle(Style.FILL);
        }

        this.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                if (isChecked) {
                    SegmentedControlButton.this.setBackgroundDrawable(SegmentedControlButton.this.backgroundSelected);
                } else {
                    SegmentedControlButton.this.setBackgroundDrawable(SegmentedControlButton.this.backgroundUnselected);
                }
            }
        });
    }

    private int getLineColor() {
    	 return this.isChecked() ? this.lineColor : this.lineColorUnselected;
	}

	@Override
    public void onDraw(final Canvas canvas) {
        final String text = this.getText().toString();
        final int lineHeight;
        if (this.isChecked()) {
            lineHeight = this.lineHeightSelected;
            this.textPaint.setColor(this.textColorSelected);
        } else {
            lineHeight = this.lineHeightUnselected;
            this.textPaint.setColor(this.textColorUnselected);
        }
        
        this.line.setColor(this.getLineColor());

        final int textHeightPos = this.getHeight() - this.lineHeightSelected - this.textDistanceFromLine;
        final float x = this.centerScreen;
        final Drawable background = this.getBackground();
        background.setBounds(0, 0, this.getWidth(), this.getHeight());
        background.draw(canvas);
        canvas.drawText(text, x, textHeightPos, this.textPaint);

        if (lineHeight > 0) {
            final Rect rect = new Rect(0, this.getHeight() - lineHeight, this.getWidth(), this.getHeight());
            canvas.drawRect(rect, this.line);
        }
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int ow, final int oh) {
        super.onSizeChanged(w, h, ow, oh);
        this.centerScreen = w * 0.5f; // remember the center of the screen
    }
}