package org.bibsonomy.android.task;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpHost;
import org.bibsonomy.android.App;
import org.bibsonomy.android.service.utils.PostRenderer;
import org.bibsonomy.android.utils.AsyncTask;
import org.bibsonomy.android.utils.IOUtils;
import org.bibsonomy.android.utils.httpclient.HttpClient;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.util.UrlBuilder;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;

/**
 * 
 * @author dzo
 */
public class ScrapePublicationTask extends AsyncTask<String, Integer, Post<BibTex>> {
	private static final String LOG_TAG = "ScrapePublicationTask";

	private static final PostRenderer POST_RENDERER = new PostRenderer();
	private static final UrlBuilder SCRAPER_SERVICE_URL_BUILDER = new UrlBuilder(App.SCRAPING_SERVICE);
	
	private final HttpHost serviceHost;
	
	/**
	 * @param webService the web service to use
	 */
	public ScrapePublicationTask(final String webService) {
		this.serviceHost = new HttpHost(webService);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Post<BibTex> doInBackground(final String... params) {		
		try {
			// reset and add format xml
			SCRAPER_SERVICE_URL_BUILDER.clearParameters().addParameter("format", "xml")
			.addParameter("url", params[0])
			.addParameter("selection", params[1]);
			
			final InputStream stream = HttpClient.getInstance().getContentAsStream(this.serviceHost, SCRAPER_SERVICE_URL_BUILDER.asString());
			if (stream != null) {
				try {
					POST_RENDERER.startSingleItem(new InputStreamReader(stream));
					return (Post<BibTex>) POST_RENDERER.next();
				} finally {
					IOUtils.close(stream);
				}
			}
		} catch (final IOException e) {
			Log.e(LOG_TAG, "exception while scraping publication", e);
		} catch (final XmlPullParserException e) {
			Log.e(LOG_TAG, "exception while scraping publication", e);
		}
		
		return null;
	}

}
