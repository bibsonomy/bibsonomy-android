package org.bibsonomy.android.service.utils;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import org.bibsonomy.android.utils.XMLSerializer;
import org.bibsonomy.model.sync.SynchronizationAction;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;

/**
 * @author dzo
 */
public class SynchronizationPostRenderer extends AbstractRenderer<SynchronizationPost> {
	private static final String TAG_SYNC_POSTS = "syncPosts";
    private static final String TAG_SYNC_POST = "syncPost";
    
    private static final String ATTRIBUTE_SYNC_HASH = "hash";
	private static final String ATTRIBUTE_CHANGE_DATE = "changeDate";
	private static final String ATTRIBUTE_CREATE_DATE = "createDate";
	private static final String ATTRIBUTE_ACTION = "action";
    
	
    @Override
	public void startList(final Reader reader) throws XmlPullParserException, IOException {
    	this.initParser(reader);
    	
    	this.consumeContentTill(TAG_SYNC_POSTS);
	}
	
	@Override
	public boolean hasNext() throws IOException, XmlPullParserException {
		return super.hasNext() && TAG_SYNC_POST.equals(this.parser.getName());
	}
	
	@Override
	public SynchronizationPost next() throws IOException, XmlPullParserException {
		final SynchronizationPost synchronizationPost = new SynchronizationPost();
		final String action = this.parser.getAttributeValue(null, ATTRIBUTE_ACTION);
		if (present(action)) {
			synchronizationPost.setAction(Enum.valueOf(SynchronizationAction.class, action));
		}
		
		synchronizationPost.setCreateDate(parseDate(this.parser.getAttributeValue(null, ATTRIBUTE_CREATE_DATE)));
		synchronizationPost.setChangeDate(parseDate(this.parser.getAttributeValue(null, ATTRIBUTE_CHANGE_DATE)));
		synchronizationPost.setIntraHash(this.parser.getAttributeValue(null, ATTRIBUTE_SYNC_HASH));
		
		while (true) {
			final int type = this.parser.next();
			final String name = this.parser.getName();
			
			/*
			 * end of sync post found exit loop
			 */
			if (type == XmlPullParser.END_TAG && TAG_SYNC_POST.equals(name)) {
				break;
			}
			
			if (type != XmlPullParser.START_TAG) {
				continue;
			}
			
			if (TAG_POST.equals(name)) {
				synchronizationPost.setPost(PostRenderer.parsePost(this.parser));
			}
		}
		
		return synchronizationPost;
	}

	@Override
	public void serializeList(final Writer writer, final List<? extends SynchronizationPost> items) throws IOException {
		final XMLSerializer serializer = this.createXMLSerializer(writer);
		
		serializer.startTag(null, TAG_SYNC_POSTS);
		
		for (final SynchronizationPost synchronizationPost : items) {
			this.serializeSynchronizationPost(serializer, synchronizationPost);
		}
		
		serializer.endTag(null, TAG_SYNC_POSTS);
		
		this.flush(serializer);
	}
	
	private void serializeSynchronizationPost(final XmlSerializer serializer, final SynchronizationPost synchronizationPost) throws IOException {
		serializer.startTag(null, TAG_SYNC_POST)
		.attribute(null, ATTRIBUTE_SYNC_HASH, synchronizationPost.getIntraHash());
		final long time = System.currentTimeMillis();
		serializer.attribute(null, ATTRIBUTE_CREATE_DATE, formatDate(synchronizationPost.getCreateDate()))
		.attribute(null, ATTRIBUTE_CHANGE_DATE, formatDate(synchronizationPost.getChangeDate()));
		Log.d("time", time - System.currentTimeMillis() + " format");
		serializer.endTag(null, TAG_SYNC_POST);
	}
}
