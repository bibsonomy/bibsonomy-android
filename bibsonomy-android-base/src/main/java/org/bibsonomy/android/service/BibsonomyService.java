package org.bibsonomy.android.service;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.bibsonomy.android.App;
import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.Database.PostColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.providers.database.Database.TagColumns;
import org.bibsonomy.android.providers.database.utils.DatabaseUtils;
import org.bibsonomy.android.service.utils.AbstractRenderer;
import org.bibsonomy.android.service.utils.PostRenderer;
import org.bibsonomy.android.service.utils.SynchronizationPostRenderer;
import org.bibsonomy.android.service.utils.TagRenderer;
import org.bibsonomy.android.service.utils.UserRenderer;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.FileManager;
import org.bibsonomy.android.utils.IOUtils;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.android.utils.NotificationUtils;
import org.bibsonomy.android.utils.ScraperRepository;
import org.bibsonomy.android.utils.httpclient.HttpClient;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.factories.ResourceFactory;
import org.bibsonomy.model.sync.SynchronizationAction;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.bibsonomy.model.sync.SynchronizationStatus;
import org.bibsonomy.model.sync.util.SynchronizationUtils;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.model.util.UserUtils;
import org.bibsonomy.util.UrlBuilder;
import org.bibsonomy.util.UrlUtils;
import org.xmlpull.v1.XmlPullParserException;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Main class for interacting with the api and web service on the server side
 * and the sqllite database on the device
 * 
 * @author Alex Plischke
 * @author dzo
 */
public class BibsonomyService extends IntentService {
	private static final String CONTENT_ENCODING = "UTF-8";
	private static final String LOG_TAG = "BibSonomyService";
	
	private static final int LIST_OFFSET = 100;
	private static final String ANDROID_SYNC_URI = UrlUtils.safeURIEncode(SynchronizationUtils.CLIENT_SPECIAL_SCHEME + "://android/");
	private static final String API_SYNC = "/api/sync/";
	
	private static final HttpGet SCRAPING_INFO = new HttpGet(App.SCRAPING_SERVICE + "?action=info&format=json");
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
	private static final int ONE_MONTH = 30 * 24 * 60 * 1000;
	
	private static final String[] INTRAHASH_PROJECTION = { ResourceColumns.INTRAHASH };
	private static final String[] SYNC_PROJECTION = { ResourceColumns.INTRAHASH, PostColumns.POSTINGDATE, PostColumns.CHANGEDATE };
    
	private static final TagRenderer TAG_RENDERER = new TagRenderer();
    private static final UserRenderer USER_RENDERER = new UserRenderer();
    private static final PostRenderer POST_RENDERER = new PostRenderer();
    private static final SynchronizationPostRenderer SYNC_POST_RENDERER = new SynchronizationPostRenderer();

    private HttpHost serviceHost;
    
    private String deviceURI;
    private String apiKey;
    private String username;
    private User user;
	private String syncStategy;
	private String syncDirection;
	private boolean syncInProgress;
    
    /**
     * the library service
     */
    public BibsonomyService() {
    	super("BibsonomyService");
    }

    @Override
    public IBinder onBind(final Intent intent) {
    	return new Binder() {
    		/**
    		 * @return the service to use
    		 */
    		@SuppressWarnings("unused")
			public BibsonomyService getService() {
        		return BibsonomyService.this;
        	}
    	};
    }

    @Override
    public void onCreate() {
    	super.onCreate();
    	
    	final String webService = this.getString(R.string.webservice);
    	this.serviceHost = new HttpHost(webService);
    	
    	final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	final String username = prefs.getString(Settings.USERNAME, null);
		this.username = UrlUtils.safeURIEncode(username);
    	this.apiKey = prefs.getString(Settings.API_KEY, null);
    	
    	this.syncDirection = prefs.getString(Settings.SYNC_DIRECTION, null);
    	this.syncStategy = prefs.getString(Settings.SYNC_CONFLICT_STATEGY, null);
    	
    	this.user = new User(username);
    	
    	this.deviceURI = ANDROID_SYNC_URI + getDeviceUUID(prefs);
    	
    	HttpClient.getInstance().setCredentials(this.username, this.apiKey, webService);
    }

    private static String getDeviceUUID(final SharedPreferences prefs) {
    	/*
    	 * check if device id already generated
    	 */
    	final String savedId = prefs.getString(Settings.DEVICE_ID, null);
    	if (present(savedId)) {
    		return savedId;
    	}
    	
    	/*
    	 * ... if not generate one and save it in settings
    	 */
    	final String uuid = UserUtils.generateApiKey();
    	final Editor editor = prefs.edit();
    	editor.putString(Settings.DEVICE_ID, uuid);
    	editor.commit();
		
		return uuid;
	}

	@Override
    protected void onHandleIntent(final Intent intent) {
    	final String action = intent.getAction();
    	final Bundle bundle = intent.getExtras();
    	final String intrahash = intent.getStringExtra(Intents.EXTRA_INTRAHASH);
    	
    	if (Intents.ACTION_SYNC.equals(action)) {
    		this.syncWithRemoteSerivce();
    	} else if (Intents.ACTION_DELETE_POST.equals(action)) {
			/*
			 * delete local post
			 * after that try to delete it on the remote service
			 * if deleteRemotePost fails the next sync will delete the post
			 */
			this.deleteLocalPost(intrahash);
			this.deleteRemotePost(intrahash);
		} else if (Intents.ACTION_IMPORT_BOOKMARKS.equals(action)) {
			this.createImportedBrowserBookmarks(bundle.getStringArray(Intents.EXTRA_TITLES), bundle.getStringArray(Intents.EXTRA_URLS));
		} else if (Intents.ACTION_DELETE_LOCAL_POST.equals(action)) {
		    this.deleteLocalPost(intrahash);
		} else if (Intents.ACTION_DELETE_REMOTE_POST.equals(action)) {
		    this.deleteRemotePost(intrahash);
		} else {
			final Post<?> post = this.getPost(intent);
			if (Intents.ACTION_CREATE_POST.equals(action)) {
				/*
				 * create the post on the device
				 */
				this.createLocalPost(post);
				
				/*
				 * try to create the post on the remote service
				 * if createRemotePost fails the next sync will create the post
				 */
				this.createRemotePost(post);
			} else if (Intents.ACTION_UPDATE_POST.equals(action)) {
				/*
				 * update the post on the device
				 */
			    this.updateLocalPost(intrahash, post);
			    
			    /*
			     * try to update the post on the remote system
			     */
			    this.updateRemotePost(intrahash, post);
			}
		}
    }
    
    private Post<?> getPost(final Intent intent) {
    	final PostParcel<?> postParcel = (PostParcel<?>) intent.getParcelableExtra(Intents.EXTRA_POST);
    	if (!present(postParcel)) {
    		return null;
    	}
    	
		final Post<?> post = postParcel.getPost();
		if (present(post)) {
			post.setUser(this.user);
		}
		return post;
    }
    
    private void syncWithRemoteSerivce() {
    	if (this.syncInProgress) {
    		Log.d(LOG_TAG, "sync in progress");
    		return;
    	}
    	
    	this.syncInProgress = true;
    	final Resources resources = this.getResources();
    	try {
	    	final Date date = new Date();
	    	NotificationUtils.sendNotification(this, resources.getString(R.string.synchronization), resources.getString(R.string.synchronization_started), App.Notifications.SYNC_NOTIFICATION_ID);
	    	// update groups
			final List<Group> groups = this.fetchGroups();
			
			/*
			 * save groups
			 */
			final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			final Editor editor = prefs.edit();
			
			if (groups.isEmpty()) {
				editor.remove(App.Settings.GROUPS);
			} else {
				editor.putString(App.Settings.GROUPS, ModelUtils.convertGroups(groups));
			}
			
			/*
			 * only sync the selected resources
			 */
			if (prefs.getBoolean(App.Settings.SHOW_BOOKMARKS, false)) {
				this.sync(Bookmark.class, true);
			}
			
			if (prefs.getBoolean(App.Settings.SHOW_PUBLICATIONS, false)) {
				this.sync(BibTex.class, true);
			}		
			
			// update tags
			this.syncTags();
			this.fetchFriendsAndGroupMembers(groups);
			
			this.refreshScraperInformations(date, prefs);
			
			NotificationUtils.sendNotification(this, resources.getText(R.string.synchronization), resources.getText(R.string.synchronization_finished), App.Notifications.SYNC_NOTIFICATION_ID);
			
			editor.putString(App.Settings.LAST_SYNC_DATE, DATE_FORMAT.format(date));
			editor.commit();
			
			this.updateCovers();
			
			Log.d(LOG_TAG, "sync took " + (System.currentTimeMillis() - date.getTime()) + " ms");
    	} catch (final Exception e) {
    		Log.e(LOG_TAG, "error while syncing", e);
    		NotificationUtils.sendNotification(this, resources.getText(R.string.synchronization), resources.getText(R.string.synchronization_finished), App.Notifications.SYNC_NOTIFICATION_ID);
    	}

		NotificationUtils.cancelNotification(this, App.Notifications.SYNC_NOTIFICATION_ID);
		this.syncInProgress = false;
    }

	private void updateCovers() {
		final Cursor cursor = this.getContentResolver().query(ItemProvider.getPostsContentUri(this), INTRAHASH_PROJECTION, null, null, null);
		while (cursor.moveToNext()) {
			final String intrahash = cursor.getString(cursor.getColumnIndex(ResourceColumns.INTRAHASH));
			try {
				this.updateCover(intrahash);
			} catch (final Exception e) {
				Log.e(LOG_TAG, "error while updating cover for '" + intrahash + "'", e);
			}
		}
		cursor.close();
	}

	private void updateCover(final String intrahash) {
		// TODO: update cover
	}

	/**
	 * @param date
	 * @param prefs
	 */
	protected void refreshScraperInformations(final Date date, final SharedPreferences prefs) {
		// update only once per month
		final String lastSync = prefs.getString(App.Settings.LAST_SYNC_DATE, null);
		boolean updateScrapers = false;
		if (present(lastSync)) {
			try {
				final Date lastSyncDate = DATE_FORMAT.parse(lastSync);
				if (Math.abs(lastSyncDate.getTime() - date.getTime()) > ONE_MONTH) {
					updateScrapers = true;
				}
			} catch (final ParseException e) {
				Log.i(LOG_TAG, "parsing date failed (" + lastSync + ")");
			}
		} else {
			updateScrapers = true;
		}
		if (updateScrapers) {
			try {
				final InputStream content = HttpClient.getInstance().getContentAsStream(this.serviceHost, SCRAPING_INFO);
				if (content != null) {
					this.deleteFile(App.SCRAPING_FILE_NAME);
					final Writer output = new OutputStreamWriter(this.openFileOutput(App.SCRAPING_FILE_NAME, MODE_PRIVATE));
					IOUtils.writeAndClose(content, output);
					output.flush();
					IOUtils.close(output);
					IOUtils.close(content);
					ScraperRepository.getInstance().markForReload();
				}
			} catch (final IOException e) {
				Log.e(LOG_TAG, "error downloading scraping info", e);
			}
		}
	}

    /**
     * to get autocompletion for "for" and "send" tags we add them to the tags
     * table with a constant count
     * @param groups the groups of the logged in user
     */
	private void fetchFriendsAndGroupMembers(final List<Group> groups) {
		final ContentResolver contentResolver = this.getContentResolver();
		final ContentValues values = new ContentValues(3);
		values.put(TagColumns.TAG_USER_COUNT, 10); // TODO: adapt?
		
		/*
		 * first query friends
		 */
		final Set<User> forUsers = this.fetchSet("/api/users/" + this.username + "/friends?relation=outgoing", USER_RENDERER);
		/*
		 * add "for" system tag for auto completion and add all members of the group
		 */
		final Uri tagsContentUri = ItemProvider.getTagsContentUri(this);
		for (final Group group : groups) {
			final String groupName = group.getName();
			values.put(TagColumns.TAG_NAME_LOWER, groupName);
			values.put(TagColumns.TAG_NAME, "for:" + groupName); // TODO: use constant of SystemTag
			contentResolver.insert(tagsContentUri, values);
			
			values.put(TagColumns.TAG_NAME, "sys:relevantfor:" + groupName); // TODO: use constant of SystemTag
			contentResolver.insert(tagsContentUri, values);
			/*
			 * send tag also allowed
			 */
			forUsers.addAll(this.fetchSet("/api/groups/" + groupName + "/users", USER_RENDERER));
		}
		
		/*
		 * add add send:<FRIEND> to tags for auto completion
		 */
		for (final User forUser : forUsers) {
			final String userName = forUser.getName();
			values.put(TagColumns.TAG_NAME_LOWER, userName);
			values.put(TagColumns.TAG_NAME, "send:" + userName); // TODO: use constant of SystemTag
			contentResolver.insert(tagsContentUri, values);
		}
	}

	/**
	 * @param url
	 * @param renderer the renderer to use
	 * @return the fetched set
	 */
	protected <T> Set<T> fetchSet(final String url, final AbstractRenderer<T> renderer) {
		final Set<T> result = new HashSet<T>();
		int count;
		int start = 0;
		
		do {
			count = 0;
			final String path = url + "&start=" + start + "&end=" + (start + LIST_OFFSET);
			
			try {
				final InputStream in = HttpClient.getInstance().getContentAsStream(this.serviceHost, path);
				if (in != null) {
	    			renderer.startList(new InputStreamReader(in));
	    			while (renderer.hasNext()) {
	    				final T one = renderer.next();
	    				result.add(one);
	    				count++;
	    			}
	    			IOUtils.close(in);
	    		}	
			} catch (final IOException e) {
				Log.e(LOG_TAG, "io while fetching list (" + path + ")", e);
			} catch (final XmlPullParserException e) {
				Log.e(LOG_TAG, "parsing failed while fetching list (" + path + ")", e);
			}
			// next sublist
	    	start += LIST_OFFSET;
		} while (count == LIST_OFFSET);
		
		return result;
	}

	private void syncTags() {
		final ContentResolver resolver = this.getContentResolver();
		final Uri tagsContentUri = ItemProvider.getTagsContentUri(this);
		resolver.delete(tagsContentUri, null, null);
		final Set<Tag> tags = this.fetchSet("/api/tags/?user=" + this.username, TAG_RENDERER);
		
		for (final Tag tag : tags) {
			final ContentValues contentValues = DatabaseUtils.contentValuesForTag(tag);
			resolver.insert(tagsContentUri, contentValues);
		}
	}

	private void sync(final Class<? extends Resource> resourceType, final boolean retry) throws SynchronizationException {
		/*
		 * build sync url for the resource
		 */
		final UrlBuilder urlBuilder = new UrlBuilder(API_SYNC + this.deviceURI);
		urlBuilder.addParameter("resourcetype", ResourceFactory.getResourceName(resourceType))
		.addParameter("strategy", this.syncStategy)
		.addParameter("direction", this.syncDirection)
		.addParameter("deviceInfo", Build.MODEL);
		
		InputStream in = null;		
		try {
			SynchronizationStatus newStatus = SynchronizationStatus.DONE;
			
			/*
			 * 1. get local posts and build sync post list
			 */
			final Cursor cursor = this.getContentResolver().query(ContentUrisUtils.withAppendedParam(ItemProvider.getPostsContentUri(this), ItemProvider.RESOUCRE_TYPE_PARAM, ResourceFactory.getResourceName(resourceType)), SYNC_PROJECTION, null, null, null);
			final List<SynchronizationPost> localPosts = DatabaseUtils.convertSyncPosts(cursor);
			cursor.close();
			
			final StringWriter writer = new StringWriter(100);
			
			final long time = System.currentTimeMillis();
			SYNC_POST_RENDERER.serializeList(writer, localPosts);
			Log.d("time", (time - System.currentTimeMillis())  + "");
			
			/*
			 * 2. get the sync plan from the server
			 */
			final String deviceApiSyncUrl = urlBuilder.asString();
			final HttpPost createSyncPlan = new HttpPost(deviceApiSyncUrl);
			createSyncPlan.setEntity(new StringEntity(writer.toString(), CONTENT_ENCODING));

			in = HttpClient.getInstance().getContentAsStream(this.serviceHost, createSyncPlan);
			
			if (in == null) {
				/*
				 * maybe a running sync was aborted
				 * set sync status to error and retry
				 */
				if (this.updateSyncStatus(deviceApiSyncUrl, SynchronizationStatus.ERROR)) {
					if (retry) {
						this.sync(resourceType, false);
						return;
					}
				}
				throw new SynchronizationException();
			}
			
		    SYNC_POST_RENDERER.startList(new InputStreamReader(IOUtils.copyStream(in)));
		    IOUtils.close(in);
		    
			try {
				/*
				 * 3. update sync status to running
				 */
				if (!this.updateSyncStatus(deviceApiSyncUrl, SynchronizationStatus.RUNNING)) {
					throw new SynchronizationException();
				}
				
				/*
				 * 4. execute sync plan post by post
				 */
				while (SYNC_POST_RENDERER.hasNext()) {
					final SynchronizationPost synchronizationPost = SYNC_POST_RENDERER.next();
					final String intraHash = synchronizationPost.getIntraHash();
					final SynchronizationAction action = synchronizationPost.getAction();
					final Post<? extends Resource> post = synchronizationPost.getPost();
					
					Log.d(LOG_TAG, intraHash + "=>" + action);
					
					switch (action) {
					case CREATE_CLIENT:
						this.createPost(post);
						break;
					case CREATE_SERVER:
						final Post<? extends Resource> createPost = this.getLocalPost(intraHash);
						this.createRemotePost(createPost);
						break;
					case UPDATE_CLIENT:
						this.updateLocalPost(intraHash, post);
						break;
					case UPDATE_SERVER:
						final Post<? extends Resource> updatePost = this.getLocalPost(intraHash);
						this.updateRemotePost(intraHash, updatePost);
						break;
					case DELETE_CLIENT:
						this.deleteLocalPost(intraHash);
						break;
					case DELETE_SERVER:
						this.deleteRemotePost(intraHash);
						break;
					default:
						Log.w(LOG_TAG, "action " +  action + " unknown");
						break;
					}
				}
				
			} catch (final Exception ex) {
				Log.e(LOG_TAG, "sync failed with error", ex);
				newStatus = SynchronizationStatus.ERROR;
			}
			
			/*
			 * 5. finish sync by updating the sync status to the new one (error / done)
			 */
			this.updateSyncStatus(deviceApiSyncUrl, newStatus);
		} catch (final IOException e) {
			Log.e(LOG_TAG, "io error while syncing", e);
		} catch (final XmlPullParserException e) {
			Log.e(LOG_TAG, "parsing xml failed while syncing", e);
		}
	}

	/**
	 * @param deviceApiSyncUrl
	 * @param running 
	 * @return <code>true</code> iff the update was successful
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	protected boolean updateSyncStatus(final String deviceApiSyncUrl, final SynchronizationStatus running) throws IOException {
		final HttpPut updateSyncStatusRequest = new HttpPut(deviceApiSyncUrl + "&status=" + running.toString());
		updateSyncStatusRequest.setEntity(new StringEntity(Build.MODEL, CONTENT_ENCODING));
		
		return HttpClient.getInstance().executeAndCheckForSuccess(this.serviceHost, updateSyncStatusRequest);
	}
	
    private Post<? extends Resource> getLocalPost(final String intraHash) {
		return DatabaseUtils.convertPost(this.getContentResolver().query(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intraHash), null, null, null, null));
    }

    /**
     * creates the {@link Post} on the server
     * 
     * @param post the post to create
     * @return <code>true</code> iff the post was created successfully on the server
     */
    private boolean createRemotePost(final Post<? extends Resource> post) {
    	try {
    		final String postXML = this.serializePost(post);

    		final HttpPost httpPost = new HttpPost("/api/users/" + this.username + "/posts");
    		httpPost.setEntity(new StringEntity(postXML, CONTENT_ENCODING));
    		
    		return HttpClient.getInstance().executeAndCheckForSuccess(this.serviceHost, httpPost);
    	} catch (final IOException e) {
    		Log.e(LOG_TAG, "creating remote post failed", e);
    	}
    	
    	return false;
    }

	protected String serializePost(final Post<? extends Resource> post) throws IOException {
		final StringWriter writer = new StringWriter(100);
		POST_RENDERER.serializeSingleItem(writer, post);
		return writer.toString();
	}

    /**
     * creates {@link Post} in local db.
     * 
     * @param post	the {@link Post} to be created
     */
    private void createLocalPost(final Post<? extends Resource> post) {
    	post.setDate(new Date());
    	post.setChangeDate(new Date());
    	this.createPost(post);
    }
    
    private void createPost(final Post<? extends Resource> post) {
    	final ContentValues values = DatabaseUtils.contentValuesForPost(post);
    	this.getContentResolver().insert(ItemProvider.getPostsContentUri(this), values);
    	
    	if (post.getResource() instanceof BibTex) {
			final BibTex publication = (BibTex) post.getResource();
			final List<Document> documents = publication.getDocuments();
			if (present(documents)) {				
				final String intraHash = publication.getIntraHash();
				final Uri postDocumentsUri = ItemProvider.getPostsContentUri(this).buildUpon().appendPath(intraHash).appendPath(ItemProvider.DOCUMENTS_SUB_PATH).build();
				for (final Document document : documents) {
					final ContentValues convertValuesForDocument = DatabaseUtils.convertValuesForDocument(document);
					this.getContentResolver().insert(postDocumentsUri, convertValuesForDocument);
					this.downloadDocument(intraHash, document);
				}
			}
		}
    }

    private void downloadDocument(final String intraHash, final Document document) {
		final String fileName = document.getFileName();
		final HttpGet get = new HttpGet("/api/users/" + this.username + "/posts/" + intraHash + "/documents/" + fileName);
		
		try {
			final HttpResponse response = HttpClient.getInstance().execute(this.serviceHost, get);
			if (HttpClient.isSuccessStatusCode(response.getStatusLine().getStatusCode())) {
				FileManager.saveDocument(this, intraHash, document, response.getEntity());
				HttpClient.closeContentStream(response);
			}
		} catch (final IOException e) {
			Log.e(LOG_TAG, "error while downloading document " + fileName, e);
		}
	}

	private boolean deleteRemotePost(final String intrahash) {
    	final HttpDelete delete = new HttpDelete("/api/users/" + this.username + "/posts/" + intrahash);
		try {
		    final HttpResponse response = HttpClient.getInstance().execute(this.serviceHost, delete);
		    final int statusCode = response.getStatusLine().getStatusCode();
		    HttpClient.closeContentStream(response);
		    /*
		     * OK... we have to make an assumption here. In the case where we
		     * try to delete a Post on the server that has been already deleted
		     * or doesn't exist, the server responds with an internal server
		     * error (500). For the sake of not "leaking" any leftover Posts in
		     * the database on the device, we just assume that the Post was
		     * correctly deleted on the server.
		     */
		    if (HttpClient.isSuccessStatusCode(statusCode) || (statusCode == HttpStatus.SC_INTERNAL_SERVER_ERROR)) {
		    	return true;
		    }
		} catch (final IOException ioe) {
		    Log.e(LOG_TAG, "deleting remote post failed", ioe);
		}
		return false;
    }

    private void deleteLocalPost(final String intrahash) {
    	final ContentResolver cr = this.getContentResolver();
    	cr.delete(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash), null, null);
    	
    	FileManager.deleteAll(this, intrahash);
    }

    private boolean updateRemotePost(final String intrahash, final Post<? extends Resource> post) {
    	post.setChangeDate(new Date());
		try {
			final HttpPut put = new HttpPut("/api/users/" + this.username + "/posts/" + intrahash);
			final String xmlString = this.serializePost(post);
		    put.setEntity(new StringEntity(xmlString));
		    return HttpClient.getInstance().executeAndCheckForSuccess(this.serviceHost, put);
		} catch (final IOException ioe) {
		    Log.e(LOG_TAG, "updating remote post failed", ioe);
		}
		return false;
    }

    private void updateLocalPost(final String intrahash, final Post<? extends Resource> remotePost) {
    	final ContentResolver cr = this.getContentResolver();
    	final Uri postUri = ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash);
		
		final Resource resource = remotePost.getResource();
		resource.recalculateHashes();
		final String newIntrahash = resource.getIntraHash();
		
		final ContentValues values = DatabaseUtils.contentValuesForPost(remotePost);
		cr.update(postUri, values, null, null);
		
		if (!intrahash.equals(newIntrahash)) {
			FileManager.moveAll(this, intrahash, newIntrahash);
		}
    }

    private List<Group> fetchGroups() {
    	try {
    		final InputStream in = HttpClient.getInstance().getContentAsStream(this.serviceHost, "/api/users/" + this.username);
    		if (in != null) {
    			try {
	    			USER_RENDERER.startSingleItem(new InputStreamReader(in));
	        		if (USER_RENDERER.hasNext()) {
	        			final User user = USER_RENDERER.next();            			
	            		return user.getGroups();    					
	        		}
	    		} finally {
	    			IOUtils.close(in);
	    		}
    		}
    	} catch (final IOException e) {
    		Log.e(LOG_TAG, "error while syncing groups", e);
    	} catch (final XmlPullParserException e) {
			Log.e(LOG_TAG, "error while syncing groups", e);
		}
    	
    	return null;
    }

    private void createImportedBrowserBookmarks(final String[] titles, final String[] urls) {
    	final ContentResolver cr = this.getContentResolver();
    	final int length = titles.length;
    	final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	final String systemTag = prefs.getString(App.Settings.BOOKMARK_IMPORT_TAG, null);

    	final List<Post<Bookmark>> posts = new LinkedList<Post<Bookmark>>();
    	final Tag importTag = new Tag(systemTag);
    	final Bookmark dummy = new Bookmark();
		for (int i = 0; i < length; i++) {
			final String title = titles[i];
			final String url = urls[i];
		    if (title == null) {
				continue;
		    }
		    
		    dummy.setUrl(url);
		    dummy.recalculateHashes();
		    final Cursor c = cr.query(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), dummy.getIntraHash()), INTRAHASH_PROJECTION, null, null, null);
		    if (c.getCount() == 0) {
		    	final Post<Bookmark> post = new Post<Bookmark>();
			    post.setUser(this.user);
				post.getTags().add(importTag);
			    post.getGroups().add(GroupUtils.getPrivateGroup()); // TODO: make setable
			    
			    final Bookmark bookmark = new Bookmark();
			    
			    bookmark.setTitle(title);
			    bookmark.setUrl(url);
			    bookmark.recalculateHashes();
			    
			    post.setResource(bookmark);
		    	posts.add(post);
		    	this.createLocalPost(post);
		    }		    
		    c.close();
		}
		
		/*
		 * try to create the posts on the remote system
		 */
		for (final Post<Bookmark> post : posts) {
			this.createRemotePost(post);
		}
    }
}