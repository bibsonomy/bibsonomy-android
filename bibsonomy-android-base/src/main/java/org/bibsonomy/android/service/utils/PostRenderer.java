package org.bibsonomy.android.service.utils;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.bibsonomy.android.utils.XMLSerializer;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.util.PersonNameUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

/**
 * 
 * @author dzo
 */
public class PostRenderer extends AbstractRenderer<Post<? extends Resource>> {
	private static final String TAG_POSTS = "posts";
    protected static final String TAG_USER = "user";
    private static final String RESPONSE_TAG_GROUP = "group";
    private static final String RESPONSE_TAG_TAG = "tag";
    private static final String RESPONSE_TAG_DOCUMENT = "document";
    private static final String RESPONSE_TAG_BIBTEX = "bibtex";
    private static final String RESPONSE_TAG_BOOKMARK = "bookmark";
    
    private static final String RESPONSE_ATTR_CHANGEDATE = "changedate";
    private static final String RESPONSE_ATTR_POSTINGDATE = "postingdate";
    private static final String RESPONSE_ATTR_DESCRIPTION = "description";
    private static final String RESPONSE_ATTR_NAME = "name";
    private static final String RESPONSE_ATTR_MD5HASH = "md5hash";
    private static final String RESPONSE_ATTR_FILENAME = "filename";
    private static final String RESPONSE_ATTR_INTERHASH = "interhash";
    private static final String RESPONSE_ATTR_INTRAHASH = "intrahash";
    private static final String RESPONSE_ATTR_YEAR = "year";
    private static final String RESPONSE_ATTR_PUBLISHER = "publisher";
    private static final String RESPONSE_ATTR_AUTHOR = "author";
    private static final String RESPONSE_ATTR_EDITOR = "editor";
    private static final String RESPONSE_ATTR_EDITION = "edition";
    private static final String RESPONSE_ATTR_ENTRYTYPE = "entrytype";
    private static final String RESPONSE_ATTR_MISC = "misc";
    private static final String RESPONSE_ATTR_BIBTEXKEY = "bibtexKey";
    private static final String RESPONSE_ATTR_BKEY = "bKey";
    private static final String RESPONSE_ATTR_BIBTEXABSTRACT = "bibtexAbstract";
    private static final String RESPONSE_ATTR_TITLE = "title";
    private static final String RESPONSE_ATTR_ADDRESS = "address";
    private static final String RESPONSE_ATTR_ANNOTE = "annote";
    private static final String RESPONSE_ATTR_BOOKTITLE = "booktitle";
    private static final String RESPONSE_ATTR_CHAPTER = "chapter";
    private static final String RESPONSE_ATTR_CROSSREF = "crossref";
    private static final String RESPONSE_ATTR_HOWPUBLISHED = "howpublished";
    private static final String RESPONSE_ATTR_INSTITUTION = "institution";
    private static final String RESPONSE_ATTR_ORGANIZATION = "organization";
    private static final String RESPONSE_ATTR_JOURNAL = "journal";
    private static final String RESPONSE_ATTR_NOTE = "note";
    private static final String RESPONSE_ATTR_NUMBER = "number";
    private static final String RESPONSE_ATTR_PAGES = "pages";
    private static final String RESPONSE_ATTR_SCHOOL = "school";
    private static final String RESPONSE_ATTR_SERIES = "series";
    private static final String RESPONSE_ATTR_VOLUME = "volume";
    private static final String RESPONSE_ATTR_DAY = "day";
    private static final String RESPONSE_ATTR_MONTH = "month";
    private static final String RESPONSE_ATTR_TYPE = "type";
    private static final String RESPONSE_ATTR_URL = "url";
    private static final String RESPONSE_ATTR_PRIVNOTE = "privnote";
    
	
	@Override
	public void startList(final Reader reader) throws XmlPullParserException, IOException {
		this.initParser(reader);
		this.consumeContentTill(TAG_POSTS);
	}
	
	@Override
	public void startSingleItem(final Reader reader) throws XmlPullParserException ,IOException {
		this.initParser(reader);
		
		this.consumeContentTill(TAG_SYSTEM);
	}

	/**
	 * @return <true> iff parser has more items
	 */
	@Override
	public boolean hasNext() throws IOException, XmlPullParserException {
		return super.hasNext() && AbstractRenderer.TAG_POST.equals(this.parser.getName());
	}
	
	@Override
	public Post<? extends Resource> next() throws IOException, XmlPullParserException {
		return parsePost(this.parser);
	}

	protected static Post<? extends Resource> parsePost(final XmlPullParser parser) throws XmlPullParserException, IOException {
		final Post<Resource> post = new Post<Resource>();
		post.setChangeDate(parseDate(parser.getAttributeValue(null, RESPONSE_ATTR_CHANGEDATE)));
    	post.setDate(parseDate(parser.getAttributeValue(null, RESPONSE_ATTR_POSTINGDATE)));
    	post.setDescription(parser.getAttributeValue(null, RESPONSE_ATTR_DESCRIPTION));
    	List<Document> documents = null;
		Resource resource = null;
		while (true) {
			final int type = parser.next();
			final String name = parser.getName();
			
			if ((type == XmlPullParser.END_TAG) && AbstractRenderer.TAG_POST.equals(name)) {
				/*
				 * post successfully parsed
				 */
				break;
			}
			
			if (type != XmlPullParser.START_TAG) {
				continue;
			}
			
			if (TAG_USER.equals(name)) {
		    	post.setUser(UserRenderer.parseUser(parser));
		    } else if (RESPONSE_TAG_GROUP.equals(name)) {
		    	post.getGroups().add(new Group(parser.getAttributeValue(null, RESPONSE_ATTR_NAME)));
		    } else if (RESPONSE_TAG_TAG.equals(name)) {
		    	post.getTags().add(new Tag(parser.getAttributeValue(null, RESPONSE_ATTR_NAME)));
		    } else if (RESPONSE_TAG_DOCUMENT.equals(name)) {
		    	final Document document = new Document();
		    	document.setMd5hash(parser.getAttributeValue(null, RESPONSE_ATTR_MD5HASH));
		    	document.setFileName(parser.getAttributeValue(null, RESPONSE_ATTR_FILENAME));
		    	if (documents == null) {
		    		documents = new LinkedList<Document>();	
		    	}
		    	documents.add(document);
		    } else if (RESPONSE_TAG_BIBTEX.equals(name)) {
		    	final BibTex publication = parsePublication(parser);
		    	resource = publication;
				post.setResource(publication);
				if (present(documents)) {
					publication.setDocuments(documents);
				}
		    } else if (RESPONSE_TAG_BOOKMARK.equals(name)) {
		    	final Bookmark bookmark = parseBookmark(parser);
				resource = bookmark;
				post.setResource(bookmark);
		    }
		}
		
		return post;
	}
	
	protected static Bookmark parseBookmark(final XmlPullParser parser) {
		final Bookmark bookmark = new Bookmark();
		bookmark.setTitle(parser.getAttributeValue(null, RESPONSE_ATTR_TITLE));
		bookmark.setUrl(parser.getAttributeValue(null, RESPONSE_ATTR_URL));
		bookmark.setInterHash(parser.getAttributeValue(null, RESPONSE_ATTR_INTERHASH));
		bookmark.setIntraHash(parser.getAttributeValue(null, RESPONSE_ATTR_INTRAHASH));
		return bookmark;
	}

	protected static BibTex parsePublication(final XmlPullParser parser) {
		final BibTex publication = new BibTex();
		publication.setTitle(parser.getAttributeValue(null, RESPONSE_ATTR_TITLE));
		publication.setBibtexKey(parser.getAttributeValue(null, RESPONSE_ATTR_BIBTEXKEY));
		publication.setKey(parser.getAttributeValue(null, RESPONSE_ATTR_BKEY));				    	
		publication.setMisc(parser.getAttributeValue(null, RESPONSE_ATTR_MISC));
		publication.setAbstract(parser.getAttributeValue(null, RESPONSE_ATTR_BIBTEXABSTRACT));
		publication.setEntrytype(parser.getAttributeValue(null, RESPONSE_ATTR_ENTRYTYPE));
		publication.setAddress(parser.getAttributeValue(null, RESPONSE_ATTR_ADDRESS));
		publication.setAnnote(parser.getAttributeValue(null, RESPONSE_ATTR_ANNOTE));
		final String authors = parser.getAttributeValue(null, RESPONSE_ATTR_AUTHOR);
		if (authors != null) {
			publication.setAuthor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(authors));
		}
		final String editors = parser.getAttributeValue(null, RESPONSE_ATTR_EDITOR);
		if (editors != null) {
			publication.setEditor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(editors));
		}
		publication.setBooktitle(parser.getAttributeValue(null, RESPONSE_ATTR_BOOKTITLE));
		publication.setChapter(parser.getAttributeValue(null, RESPONSE_ATTR_CHAPTER));
		publication.setCrossref(parser.getAttributeValue(null, RESPONSE_ATTR_CROSSREF));
		publication.setEdition(parser.getAttributeValue(null, RESPONSE_ATTR_EDITION));
		publication.setHowpublished(parser.getAttributeValue(null, RESPONSE_ATTR_HOWPUBLISHED));
		publication.setInstitution(parser.getAttributeValue(null, RESPONSE_ATTR_INSTITUTION));
		publication.setOrganization(parser.getAttributeValue(null, RESPONSE_ATTR_ORGANIZATION));
		publication.setJournal(parser.getAttributeValue(null, RESPONSE_ATTR_JOURNAL));
		publication.setNote(parser.getAttributeValue(null, RESPONSE_ATTR_NOTE));
		publication.setNumber(parser.getAttributeValue(null, RESPONSE_ATTR_NUMBER));
		publication.setPages(parser.getAttributeValue(null, RESPONSE_ATTR_PAGES));
		publication.setPublisher(parser.getAttributeValue(null, RESPONSE_ATTR_PUBLISHER));
		publication.setSchool(parser.getAttributeValue(null, RESPONSE_ATTR_SCHOOL));
		publication.setSeries(parser.getAttributeValue(null, RESPONSE_ATTR_SERIES));
		publication.setVolume(parser.getAttributeValue(null, RESPONSE_ATTR_VOLUME));
		publication.setDay(parser.getAttributeValue(null, RESPONSE_ATTR_DAY));
		publication.setMonth(parser.getAttributeValue(null, RESPONSE_ATTR_MONTH));
		publication.setYear(parser.getAttributeValue(null, RESPONSE_ATTR_YEAR));
		publication.setType(parser.getAttributeValue(null, RESPONSE_ATTR_TYPE));
		publication.setUrl(parser.getAttributeValue(null, RESPONSE_ATTR_URL));
		publication.setPrivnote(parser.getAttributeValue(null, RESPONSE_ATTR_PRIVNOTE));
		publication.setInterHash(parser.getAttributeValue(null, RESPONSE_ATTR_INTERHASH));
		publication.setIntraHash(parser.getAttributeValue(null, RESPONSE_ATTR_INTRAHASH));
		
		return publication;
	}

	@Override
	public void serializeSingleItem(final Writer writer, final Post<? extends Resource> post) throws IOException {
		final XMLSerializer serializer = this.createXMLSerializer(writer);
		serializer.startTag(null, TAG_POST)
		.attribute(null, RESPONSE_ATTR_DESCRIPTION, post.getDescription());
		
		/*
		 * user
		 */
		serializer.startTag(null, TAG_USER)
		.attribute(null, RESPONSE_ATTR_NAME, post.getUser().getName())
		.endTag(null, TAG_USER);
		
		/*
		 * tags
		 */
		for (final Tag tag : post.getTags()) {
			serializer.startTag(null, RESPONSE_TAG_TAG)
			.attribute(null, RESPONSE_ATTR_NAME, tag.getName()) // TODO: use other constant
			.endTag(null, RESPONSE_TAG_TAG);
		}
		
		/*
		 * groups
		 */
		for (final Group group : post.getGroups()) {
			serializer.startTag(null, RESPONSE_TAG_GROUP)
			.attribute(null, RESPONSE_ATTR_NAME, group.getName()) // TODO: use other constant
			.endTag(null, RESPONSE_TAG_GROUP);
		}
		
		/*
		 * resource
		 */
		final Resource resource = post.getResource();
		this.serializeResource(serializer, resource);
		
		serializer.endTag(null, TAG_POST);
		
		this.flush(serializer);
	}
	
	private void serializeResource(final XmlSerializer serializer, final Resource resource) throws IllegalArgumentException, IllegalStateException, IOException {
		if (resource instanceof BibTex) {
			final BibTex publication = (BibTex) resource;
			serializer.startTag(null, RESPONSE_TAG_BIBTEX)
			.attribute(null, RESPONSE_ATTR_ADDRESS, publication.getAddress())
			.attribute(null, RESPONSE_ATTR_ANNOTE, publication.getAnnote());
			if (!present(publication.getAuthor())) {
				serializer.attribute(null, RESPONSE_ATTR_AUTHOR, PersonNameUtils.serializePersonNames(publication.getAuthor()));
			}
			
			if (publication.getEditor() != null) {
				serializer.attribute(null, RESPONSE_ATTR_EDITOR, PersonNameUtils.serializePersonNames(publication.getEditor()));
			}
			
			serializer.attribute(null, RESPONSE_ATTR_BIBTEXABSTRACT, publication.getAbstract())
			.attribute(null, RESPONSE_ATTR_BIBTEXKEY, publication.getBibtexKey())
			.attribute(null, RESPONSE_ATTR_BKEY, publication.getKey())
			.attribute(null, RESPONSE_ATTR_BOOKTITLE, publication.getBooktitle())
			.attribute(null, RESPONSE_ATTR_CHAPTER, publication.getChapter())
			.attribute(null, RESPONSE_ATTR_CROSSREF, publication.getCrossref())
			.attribute(null, RESPONSE_ATTR_DAY, publication.getDay())
			.attribute(null, RESPONSE_ATTR_EDITION, publication.getEdition())
			.attribute(null, RESPONSE_ATTR_ENTRYTYPE, publication.getEntrytype())
			.attribute(null, RESPONSE_ATTR_HOWPUBLISHED, publication.getHowpublished())
			.attribute(null, RESPONSE_ATTR_JOURNAL, publication.getJournal())
			.attribute(null, RESPONSE_ATTR_MISC, publication.getMisc())
			.attribute(null, RESPONSE_ATTR_MONTH, publication.getMonth())
			.attribute(null, RESPONSE_ATTR_NOTE, publication.getNote())
			.attribute(null, RESPONSE_ATTR_NUMBER, publication.getNumber())
			.attribute(null, RESPONSE_ATTR_ORGANIZATION, publication.getOrganization())
			.attribute(null, RESPONSE_ATTR_PAGES, publication.getPages())
			.attribute(null, RESPONSE_ATTR_PRIVNOTE, publication.getPrivnote())
			.attribute(null, RESPONSE_ATTR_PUBLISHER, publication.getPublisher())
			.attribute(null, RESPONSE_ATTR_SCHOOL, publication.getSchool())
			.attribute(null, RESPONSE_ATTR_SERIES, publication.getSeries())
			.attribute(null, RESPONSE_ATTR_TYPE, publication.getType())
			.attribute(null, RESPONSE_ATTR_TITLE, publication.getTitle())
			.attribute(null, RESPONSE_ATTR_URL, publication.getUrl())
			.attribute(null, RESPONSE_ATTR_VOLUME, publication.getVolume())
			.attribute(null, RESPONSE_ATTR_YEAR, publication.getYear())
			.endTag(null, RESPONSE_TAG_BIBTEX);
		} else if (resource instanceof Bookmark) {
			final Bookmark bookmark = (Bookmark) resource;
			serializer.startTag(null, RESPONSE_TAG_BOOKMARK)
			.attribute(null, RESPONSE_ATTR_TITLE, bookmark.getTitle())
			.attribute(null, RESPONSE_ATTR_URL, bookmark.getUrl())
			.endTag(null, RESPONSE_TAG_BOOKMARK);
		} else {
			// TODO: throw exception
		}
	}
}
