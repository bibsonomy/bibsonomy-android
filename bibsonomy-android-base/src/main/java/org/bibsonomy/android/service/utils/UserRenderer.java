package org.bibsonomy.android.service.utils;

import java.io.IOException;
import java.io.Reader;

import org.bibsonomy.model.Group;
import org.bibsonomy.model.User;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * 
 * @author dzo
 *
 */
public class UserRenderer extends AbstractRenderer<User> {
	private static final String TAG_GROUP = "group";
	private static final String TAG_USERS = "users";
	private static final String TAG_USER = "user";
	
	private static final String ATTRIBUTE_USER_NAME = "name";
	private static final String ATTRIBUTE_GROUP_NAME = ATTRIBUTE_USER_NAME;

	@Override
	public void startSingleItem(final Reader reader) throws XmlPullParserException, IOException {
		this.initParser(reader);
		
		this.consumeContentTill(TAG_SYSTEM);
	}
	
	@Override
	public void startList(final Reader reader) throws XmlPullParserException, IOException {
		this.initParser(reader);
		
		this.consumeContentTill(TAG_USERS);
	}
	
	@Override
	public boolean hasNext() throws IOException, XmlPullParserException {
		return super.hasNext() && TAG_USER.equals(this.parser.getName());
	}

	@Override
	public User next() throws IOException, XmlPullParserException {
		return parseUser(this.parser);
	}

	/**
	 * @return a new user
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected static User parseUser(final XmlPullParser parser) throws XmlPullParserException, IOException {
		final User user = new User();
		user.setName(parser.getAttributeValue(null, ATTRIBUTE_USER_NAME));
		
		while (true) {
			final int type = parser.next();
			final String name = parser.getName();
			
			if (type == XmlPullParser.END_TAG && TAG_USER.equals(name)) {
				break;
			}
			
			if (type != XmlPullParser.START_DOCUMENT) {
				continue;
			}
			
			if (TAG_GROUP.equals(name)) {
				final Group group = new Group();
				group.setName(parser.getAttributeValue(null, ATTRIBUTE_GROUP_NAME));
				user.addGroup(group);
			}
		}
		
		return user;
	}

}
