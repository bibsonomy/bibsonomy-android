package org.bibsonomy.android.service.utils;

import java.io.IOException;
import java.io.Reader;

import org.bibsonomy.model.Tag;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * 
 * @author dzo
 */
public class TagRenderer extends AbstractRenderer<Tag> {

	private static final String TAG_TAGS = "tags";
	private static final String TAG_TAG = "tag";
	
	private static final String ATTRIBUTE_GLOBAL_COUNT = "globalcount";
	private static final String ATTRIBUTE_USER_COUNT = "usercount";
	private static final String ATTRIBUTE_TAG_NAME = "name";

	@Override
	public void startList(final Reader reader) throws XmlPullParserException, IOException {
		this.initParser(reader);
		
		this.consumeContentTill(TAG_TAGS);
	}
	
	@Override
	public boolean hasNext() throws IOException, XmlPullParserException {
		return super.hasNext() && TAG_TAG.equals(this.parser.getName());
	}

	@Override
	public Tag next() throws IOException, XmlPullParserException {
		final Tag tag = new Tag();
		tag.setName(this.parser.getAttributeValue(null, ATTRIBUTE_TAG_NAME));
		tag.setUsercount(Integer.parseInt(this.parser.getAttributeValue(null, ATTRIBUTE_USER_COUNT)));
		tag.setGlobalcount(Integer.parseInt(this.parser.getAttributeValue(null, ATTRIBUTE_GLOBAL_COUNT)));
		
		/*
		 * consume content till tag end
		 */
		while (true) {
			final int type = parser.next();
			final String name = parser.getName();
			
			if (type == XmlPullParser.END_TAG && TAG_TAG.equals(name)) {
				break;
			}
		}
		
		return tag;
	}

}
