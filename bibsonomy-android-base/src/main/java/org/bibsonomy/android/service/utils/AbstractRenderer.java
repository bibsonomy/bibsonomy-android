package org.bibsonomy.android.service.utils;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.bibsonomy.android.utils.XMLSerializer;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;

/**
 * implementations are not thread save!
 * 
 * @author dzo
 *
 * @param <T>
 */
public abstract class AbstractRenderer<T> {
	
	private static final DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder()
		.append(ISODateTimeFormat.dateHourMinuteSecondMillis())
		.appendTimeZoneOffset("Z", true, 2, 2)
		.toFormatter();
	
	protected static final String TAG_SYSTEM = "bibsonomy";
	protected static final String TAG_POST = "post";
	
	protected static Date parseDate(final String attributeValue) {
		if (attributeValue == null) {
			return null;
		}
		
		return DATE_FORMAT.parseDateTime(attributeValue).toDate();
	}
	
	protected static String formatDate(final Date date) {
		return DATE_FORMAT.print(date.getTime());
	}
	
	
	protected XmlPullParser parser;
	
	/**
	 * starts parsing a single element 
	 * @param reader
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public void startSingleItem(final Reader reader) throws XmlPullParserException, IOException {
		throw new UnsupportedOperationException("single element not supported");
	}
	
	/**
	 * starts parsing a list
	 * @param reader
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public abstract void startList(final Reader reader) throws XmlPullParserException, IOException;
	
	/**
	 * @return parsed T
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public abstract T next() throws IOException, XmlPullParserException;
	
	/**
	 * @return <code>true</code> if there are more element to get via {@link #next()}
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public boolean hasNext() throws IOException, XmlPullParserException {
		int next = -1;
		do {
			next = this.parser.next();
		} while (next == XmlPullParser.TEXT);
		return next != XmlPullParser.END_DOCUMENT;
	}
	
	protected void initParser(final Reader reader) throws XmlPullParserException {
		this.parser = XmlPullParserFactory.newInstance().newPullParser();
		this.parser.setInput(reader);
	}

	protected void consumeContentTill(final String tagName) throws XmlPullParserException, IOException {
		while (true) {
			final int type = this.parser.next();
			final String name = this.parser.getName();
			// TODO: break loop if system tags end reached
			
			if (type != XmlPullParser.START_TAG) {
				continue;
			}
			
			if (tagName.equals(name)) {
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param writer
	 * @param items
	 * @throws IOException 
	 */
	public void serializeList(final Writer writer, final List<? extends T> items) throws IOException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * 
	 * @param writer
	 * @param item
	 * @throws IOException 
	 */
	public void serializeSingleItem(final Writer writer, final T item) throws IOException {
		throw new UnsupportedOperationException();
	}
	
	protected void flush(final XmlSerializer serializer) throws IOException {
		serializer.endTag(null, TAG_SYSTEM).flush();
	}

	protected XMLSerializer createXMLSerializer(final Writer writer) throws IOException {
		final XMLSerializer serializer = new XMLSerializer(Xml.newSerializer());
		serializer.setOutput(writer);
		serializer.startDocument(null, Boolean.valueOf(true));
		serializer.startTag(null, TAG_SYSTEM);
		return serializer;
	}
}
