package org.bibsonomy.android.providers.database.utils;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.BookmarkColumns;
import org.bibsonomy.android.providers.database.Database.DocumentColumns;
import org.bibsonomy.android.providers.database.Database.PostColumns;
import org.bibsonomy.android.providers.database.Database.PublicationColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.providers.database.Database.TagColumns;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.common.exceptions.UnsupportedResourceTypeException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.model.util.PersonNameUtils;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

/**
 * utils class for converting objects from and to
 * 
 * @author dzo
 */
public final class DatabaseUtils {
	private DatabaseUtils() {}
	
	/**
	 * @param cursor the cursor containing the result
	 * @return the converted post
	 */
	@SuppressWarnings("unchecked")
	public static <R extends Resource> Post<R> convertPost(final Cursor cursor) {
		if (cursor.getCount() == 0) {
			return null;
		}
		
		cursor.moveToFirst();
		final Post<R> post = new Post<R>();
	    post.setUser(new User(cursor.getString(cursor.getColumnIndex(PostColumns.USER))));
	    // TODO: adapt to multiple groups
	    post.getGroups().add(new Group(cursor.getString(cursor.getColumnIndex(PostColumns.GROUPS))));
	    post.setTags(ModelUtils.convertToTags(cursor.getString(cursor.getColumnIndex(PostColumns.TAGS))));
	    post.setDescription(cursor.getString(cursor.getColumnIndex(PostColumns.DESCRIPTION)));
	    post.setDate(new Date(cursor.getLong(cursor.getColumnIndex(PostColumns.POSTINGDATE))));
	    post.setChangeDate(new Date(cursor.getLong(cursor.getColumnIndex(PostColumns.CHANGEDATE))));
	    
	    final int contentType = cursor.getInt(cursor.getColumnIndex(ResourceColumns.CONTENT_TYPE));
	    
	    final Resource resource;
	    if (contentType == Database.BOOKMARK_CONTENT_TYPE) {
	    	resource = convertBookmark(cursor);
	    } else if (contentType == Database.PUBLICATION_CONTENT_TYPE) {
	    	resource = convertPublication(cursor);
	    } else {
			throw new UnsupportedResourceTypeException("unsupported resource type " + contentType);
		}
	    post.setPicked(cursor.getInt(cursor.getColumnIndex(PostColumns.CLIPBOARD)) == 1);
	    post.setResource((R) resource);
		
		return post;
	}

	private static BibTex convertPublication(final Cursor cursor) {
		final BibTex publication = new BibTex();
		setupResource(publication, cursor);
		
		final String authors = cursor.getString(cursor.getColumnIndex(PublicationColumns.AUTHOR));
		publication.setAuthor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(authors));
	    
		final String editors = cursor.getString(cursor.getColumnIndex(PublicationColumns.EDITOR));
	    publication.setEditor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(editors));
		publication.setBibtexKey(cursor.getString(cursor.getColumnIndex(PublicationColumns.BIBTEX_KEY)));
		publication.setKey(cursor.getString(cursor.getColumnIndex(PublicationColumns.KEY)));
		publication.setMisc(cursor.getString(cursor.getColumnIndex(PublicationColumns.MISC)));
		publication.setAbstract(cursor.getString(cursor.getColumnIndex(PublicationColumns.ABSTRACT)));
		publication.setEntrytype(cursor.getString(cursor.getColumnIndex(PublicationColumns.ENTRYTYPE)));
	    publication.setAddress(cursor.getString(cursor.getColumnIndex(PublicationColumns.ADDRESS)));
	    publication.setAnnote(cursor.getString(cursor.getColumnIndex(PublicationColumns.ANNOTE)));
	    publication.setBooktitle(cursor.getString(cursor.getColumnIndex(PublicationColumns.BOOKTITLE)));
	    publication.setChapter(cursor.getString(cursor.getColumnIndex(PublicationColumns.CHAPTER)));
	    publication.setCrossref(cursor.getString(cursor.getColumnIndex(PublicationColumns.CROSSREF)));
	    publication.setEdition(cursor.getString(cursor.getColumnIndex(PublicationColumns.EDITION)));
	    publication.setHowpublished(cursor.getString(cursor.getColumnIndex(PublicationColumns.HOWPUBLISHED)));
	    publication.setInstitution(cursor.getString(cursor.getColumnIndex(PublicationColumns.INSTITUTION)));
	    publication.setOrganization(cursor.getString(cursor.getColumnIndex(PublicationColumns.ORGANIZATION)));
	    publication.setJournal(cursor.getString(cursor.getColumnIndex(PublicationColumns.JOURNAL)));
	    publication.setNote(cursor.getString(cursor.getColumnIndex(PublicationColumns.NOTE)));
	    publication.setNumber(cursor.getString(cursor.getColumnIndex(PublicationColumns.NUMBER)));
	    publication.setPages(cursor.getString(cursor.getColumnIndex(PublicationColumns.PAGES)));
	    publication.setPublisher(cursor.getString(cursor.getColumnIndex(PublicationColumns.PUBLISHER)));
	    publication.setSchool(cursor.getString(cursor.getColumnIndex(PublicationColumns.SCHOOL)));
	    publication.setSeries(cursor.getString(cursor.getColumnIndex(PublicationColumns.SERIES)));
	    publication.setVolume(cursor.getString(cursor.getColumnIndex(PublicationColumns.VOLUME)));
	    publication.setDay(cursor.getString(cursor.getColumnIndex(PublicationColumns.DAY)));
	    publication.setMonth(cursor.getString(cursor.getColumnIndex(PublicationColumns.MONTH)));
	    publication.setYear(cursor.getString(cursor.getColumnIndex(PublicationColumns.YEAR)));
	    publication.setType(cursor.getString(cursor.getColumnIndex(PublicationColumns.TYPE)));
	    publication.setUrl(cursor.getString(cursor.getColumnIndex(PublicationColumns.URL)));
	    publication.setPrivnote(cursor.getString(cursor.getColumnIndex(PublicationColumns.PRIVNOTE)));
	    
		return publication;
	}

	private static Bookmark convertBookmark(final Cursor cursor) {
		final Bookmark bookmark = new Bookmark();
		setupResource(bookmark, cursor);
		bookmark.setUrl(cursor.getString(cursor.getColumnIndex(BookmarkColumns.URL)));
		return bookmark;
	}
	
	private static void setupResource(final Resource resource, final Cursor cursor) {
		resource.setIntraHash(cursor.getString(cursor.getColumnIndex(ResourceColumns.INTRAHASH)));
		resource.setInterHash(cursor.getString(cursor.getColumnIndex(ResourceColumns.INTERHASH)));
		resource.setTitle(cursor.getString(cursor.getColumnIndex(ResourceColumns.TITLE)));
	}

	/**
	 * 
	 * @param post the post
	 * @return the content values
	 */
	public static ContentValues contentValuesForPost(final Post<? extends Resource> post) {
		final ContentValues values = new ContentValues();
		
	    values.put(PostColumns.USER, post.getUser().getName());
	    final Set<Group> groups = post.getGroups();
	    if (!present(groups)) {
	    	Log.w("DatabaseUtils", "post without group " + post.getResource().getIntraHash());
			groups.add(GroupUtils.getPublicGroup());
	    }
	    values.put(PostColumns.GROUPS, groups.iterator().next().getName()); // TODO: multiple groups
	    values.put(PostColumns.TAGS, ModelUtils.convertTags(post.getTags()));
	    values.put(PostColumns.DESCRIPTION, post.getDescription());
	    values.put(PostColumns.POSTINGDATE, post.getDate().getTime());
	    values.put(PostColumns.CHANGEDATE, post.getChangeDate().getTime());
	    
	    values.putAll(contentValuesForResource(post.getResource()));
		
		return values;
	}

	private static ContentValues contentValuesForResource(final Resource resource) {
		final ContentValues values = new ContentValues();
		
	    values.put(ResourceColumns.TITLE, resource.getTitle());
	    values.put(ResourceColumns.INTERHASH, resource.getInterHash());
	    values.put(ResourceColumns.INTRAHASH, resource.getIntraHash());
	    
	    if (resource instanceof Bookmark) {
	    	DatabaseUtils.contentValuesForBookmark(values, (Bookmark) resource);
	    } else if (resource instanceof BibTex) {
	    	DatabaseUtils.contentValuesForPublication(values, (BibTex) resource);
	    }
		
		return values;
	}

	private static void contentValuesForBookmark(final ContentValues values, final Bookmark bookmark) {
		values.put(ResourceColumns.CONTENT_TYPE, Database.BOOKMARK_CONTENT_TYPE);
	    values.put(BookmarkColumns.URL, bookmark.getUrl());
	}
	
	private static void contentValuesForPublication(final ContentValues values, final BibTex publication) {
	    values.put(ResourceColumns.CONTENT_TYPE, Database.PUBLICATION_CONTENT_TYPE);
		values.put(ResourceColumns.TITLE, publication.getTitle());
	    values.put(PublicationColumns.BIBTEX_KEY, publication.getBibtexKey());
	    values.put(PublicationColumns.KEY, publication.getKey());
	    values.put(PublicationColumns.MISC, publication.getMisc());
	    values.put(PublicationColumns.ABSTRACT, publication.getAbstract());
	    values.put(PublicationColumns.ENTRYTYPE, publication.getEntrytype());
	    values.put(PublicationColumns.ADDRESS, publication.getAddress());
	    values.put(PublicationColumns.ANNOTE, publication.getAnnote());
	    
	    values.put(PublicationColumns.AUTHOR, PersonNameUtils.serializePersonNames(publication.getAuthor()));
	    values.put(PublicationColumns.EDITOR, PersonNameUtils.serializePersonNames(publication.getEditor()));
	
	    values.put(PublicationColumns.BOOKTITLE, publication.getBooktitle());
	    values.put(PublicationColumns.CHAPTER, publication.getChapter());
	    values.put(PublicationColumns.CROSSREF, publication.getCrossref());
	    values.put(PublicationColumns.EDITION, publication.getEdition());
	    
	    values.put(PublicationColumns.HOWPUBLISHED, publication.getHowpublished());
	    values.put(PublicationColumns.INSTITUTION, publication.getInstitution());
	    values.put(PublicationColumns.ORGANIZATION, publication.getOrganization());
	    values.put(PublicationColumns.JOURNAL, publication.getJournal());
	    values.put(PublicationColumns.NOTE, publication.getNote());
	    values.put(PublicationColumns.NUMBER, publication.getNumber());
	    values.put(PublicationColumns.PAGES, publication.getPages());
	    values.put(PublicationColumns.PUBLISHER, publication.getPublisher());
	    values.put(PublicationColumns.SCHOOL, publication.getSchool());
	    values.put(PublicationColumns.SERIES, publication.getSeries());
	    values.put(PublicationColumns.VOLUME, publication.getVolume());
	    values.put(PublicationColumns.DAY, publication.getDay());
	    values.put(PublicationColumns.MONTH, publication.getMonth());
	    values.put(PublicationColumns.YEAR, publication.getYear());
	    values.put(PublicationColumns.TYPE, publication.getType());
	    values.put(PublicationColumns.URL, publication.getUrl());
	    values.put(PublicationColumns.PRIVNOTE, publication.getPrivnote());
	}
	
	/**
	 * creates content values for a tag
	 * @param tag
	 * @return contentValues representation
	 */
	public static ContentValues contentValuesForTag(final Tag tag) {
		final ContentValues values = new ContentValues(3);
		
		values.put(TagColumns.TAG_NAME, tag.getName());
		values.put(TagColumns.TAG_USER_COUNT, tag.getUsercount());
		
		return values;
	}

	/**
	 * @param cursor
	 * @return a list of synchronization posts
	 */
	public static List<SynchronizationPost> convertSyncPosts(final Cursor cursor) {
		final List<SynchronizationPost> synchronizationPosts = new LinkedList<SynchronizationPost>();
		
		while (cursor.moveToNext()) {
			final SynchronizationPost post = new SynchronizationPost();
			post.setIntraHash(cursor.getString(cursor.getColumnIndex(ResourceColumns.INTRAHASH)));
			post.setCreateDate(new Date(cursor.getLong(cursor.getColumnIndex(PostColumns.POSTINGDATE))));
			post.setChangeDate(new Date(cursor.getLong(cursor.getColumnIndex(PostColumns.CHANGEDATE))));
			synchronizationPosts.add(post);
		}
		
		return synchronizationPosts;
	}

	/**
	 * @param documentsCursor documents cursor
	 * @return the documents
	 */
	public static List<Document> convertDocuments(final Cursor documentsCursor) {
		final List<Document> documents = new LinkedList<Document>();
		while (documentsCursor.moveToNext()) {
			final Document document = new Document();
			document.setFileName(documentsCursor.getString(documentsCursor.getColumnIndex(DocumentColumns.FILE_NAME)));
			document.setMd5hash(documentsCursor.getString(documentsCursor.getColumnIndex(DocumentColumns.CONTENT_HASH)));
			documents.add(document);
		}
		return documents;
	}

	/**
	 * @param document
	 * @return the content values for the document
	 */
	public static ContentValues convertValuesForDocument(final Document document) {
		final ContentValues contentValues = new ContentValues();
		contentValues.put(DocumentColumns.CONTENT_HASH, document.getMd5hash());
		contentValues.put(DocumentColumns.FILE_NAME, document.getFileName());
		return contentValues;
	}
}
