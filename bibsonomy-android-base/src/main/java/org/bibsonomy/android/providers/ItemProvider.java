package org.bibsonomy.android.providers;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Collections;
import java.util.List;

import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.DocumentColumns;
import org.bibsonomy.android.providers.database.Database.GasColumns;
import org.bibsonomy.android.providers.database.Database.PostColumns;
import org.bibsonomy.android.providers.database.Database.PublicationColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.providers.database.Database.TagColumns;
import org.bibsonomy.android.providers.database.Database.TasColumns;
import org.bibsonomy.android.providers.database.utils.DatabaseHelper;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.comparators.PersonNameComparator;
import org.bibsonomy.model.factories.ResourceFactory;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameUtils;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * database service (can be used by other apps)
 * 
 * XXX: note the authority tag in AndroidManifest must match package name
 * + "android.providers.itemprovider"
 * 
 * @author Alex Plischke
 * @author dzo
 */
public class ItemProvider extends ContentProvider {

	private static final String CONTENT_TYPE_WHERE = "content_type=";

    /**
     * the resource type param
     */
	public static final String RESOUCRE_TYPE_PARAM = "resourcetype";
	
	/**
	 * the tags param
	 */
	public static final String TAGS_PARAM = "tags";
    
    private static final String POST_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.bibsonomy.android.provider.post";
    private static final String TAG_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.bibsonomy.android.provider.tag";
    private static final String DOCUMENT_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.bibsonomy.android.provider.document";
    public static final String DOCUMENTS_SUB_PATH = "documents";
    private static final String SLASH = "/";
    
	private static final String POST_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.bibsonomy.android.provider.post";
	private static final String TAG_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.bibsonomy.android.provider.tag";
	private static final String DOCUMENT_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.bibsonomy.android.provider.document";
	
	/**
	 * @param context the context to use
	 * @return the content uri for posts
	 */
	public static final Uri getPostsContentUri(final Context context) {
		return getContentUri(context, "/posts");
	}
	
	/**
	 * @param context the context to use
	 * @return the content uri for tags
	 */
	public static final Uri getTagsContentUri(final Context context) {
		return getContentUri(context, "/tags");
	}
	
	public static final Uri getDocumentsContentUri(final Context context) {
		return getContentUri(context, SLASH + DOCUMENTS_SUB_PATH);
	}

	private static Uri getContentUri(final Context context, final String path) {
		return Uri.parse("content://" + getAuthority(context) + path);
	}
	
	private static String getAuthority(final Context context) {
		return context.getPackageName() + ".providers.itemprovider";
	}
	
    private static final String POSTS_TABLE_NAME = "posts";
    private static final String DOCUMENT_TABLE_NAME = "documents";
    private static final String TAGS_TABLE_NAME = "tags";
    
    private static final String TAS_TABLE_NAME = "tas";
    private static final String GAS_TABLE_NAME = "gas";

    private static final int POSTS = 1;
    private static final int POST = 2;
    
    private static final int TAGS = 3;
    
    private static final int DOCUMENTS = 5;
    private static final int DOCUMENT = 6;

    private final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private DatabaseHelper helper;
    
    @Override
    public boolean onCreate() {
    	this.helper = new DatabaseHelper(this.getContext());
    	
    	final String authority = getAuthority(this.getContext());
		this.sUriMatcher.addURI(authority, "posts", POSTS);
    	this.sUriMatcher.addURI(authority, "posts/*", POST);
    	
    	this.sUriMatcher.addURI(authority, "posts/*/" + DOCUMENTS_SUB_PATH, DOCUMENTS);
    	this.sUriMatcher.addURI(authority, "posts/*/" + DOCUMENTS_SUB_PATH + "/*", DOCUMENT);
    	
    	this.sUriMatcher.addURI(authority, "tags", TAGS);
    	
    	return true;
    }

    @Override
    public Cursor query(final Uri uri, final String[] projection, final String selection, final String[] selectionArgs, final String sortOrder) {
    	final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch (this.sUriMatcher.match(uri)) {
		case POSTS:
		    final String resourceType = uri.getQueryParameter(RESOUCRE_TYPE_PARAM);
		    
		    final String tagsString = uri.getQueryParameter(TAGS_PARAM);
		    /*
		     * if tags are present produce the neccessary joins with the tas table
		     * FIXME: is there a nicer way to do it?
		     */
		    if (present(tagsString)) {
		    	final StringBuilder builder = new StringBuilder(POSTS_TABLE_NAME).append(" p, (select posts_id from ");
		    	final StringBuilder tagWhere = new StringBuilder(" WHERE ");
		    	final String[] tags = tagsString.split(" ");
		    	final int tagCount = tags.length;
				for (int i = 1; i <= tagCount; i++) {
		    		if (i != 1) {
		    			builder.append(" JOIN ");
		    		}
		    		builder.append(TAS_TABLE_NAME);
		    		final String tableName = "t" + i;
		    		builder.append(" ");
		    		builder.append(tableName);
		    		
		    		if (i != 1) {
		    			builder.append(" USING (posts_id)");
		    		}
		    		
		    		tagWhere.append(tableName).append(".tag_name=\"").append(tags[i - 1]).append("\"");
		    		if (i != tagCount) {
		    			tagWhere.append(" AND ");
		    		}
		    	}
				builder.append(tagWhere);
		    	builder.append(") AS pp");
		    	qb.setTables(builder.toString());
		    	qb.appendWhere("p._id=pp.posts_id");
		    	qb.appendWhere(" AND ");
		    } else {
		    	qb.setTables(POSTS_TABLE_NAME);
		    }
		    if (ResourceFactory.BOOKMARK_CLASS_NAME.equals(resourceType)) {
		    	qb.appendWhere(CONTENT_TYPE_WHERE + Database.BOOKMARK_CONTENT_TYPE);
		    } else if (ResourceFactory.PUBLICATION_CLASS_NAME.equals(resourceType)) {
		    	qb.appendWhere(CONTENT_TYPE_WHERE + Database.PUBLICATION_CONTENT_TYPE);
		    }
		    break;
		case POST:
		    qb.setTables(POSTS_TABLE_NAME);
		    // TODO: nicer
		    final String intrahash = uri.getPathSegments().get(1);
		    qb.appendWhere(ResourceColumns.INTRAHASH + " = \"" + intrahash + "\"");
		    break;
		case TAGS:
			qb.setTables(TAGS_TABLE_NAME);
			break;
		case DOCUMENTS:
		    qb.setTables(DOCUMENT_TABLE_NAME);
		    qb.appendWhere("intrahash=\"" + uri.getPathSegments().get(1) + "\"");
		    break;
		case DOCUMENT:
		    qb.setTables(DOCUMENT_TABLE_NAME);
		    qb.appendWhere("intrahash=" + uri.getPathSegments().get(1));
		    qb.appendWhere("file_hash=" + uri.getPathSegments().get(3));
		    break;
		default:
		    throw new IllegalArgumentException("Unknown URI " + uri);
		}
	
		final SQLiteDatabase db = this.helper.getReadableDatabase();
		final Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		c.setNotificationUri(this.getContext().getContentResolver(), uri);
		return c;
    }

    @Override
    public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
    	final SQLiteDatabase db = this.helper.getWritableDatabase();

    	int count = 0;
		final String segment;

		switch (this.sUriMatcher.match(uri)) {
		case POST:
			segment = uri.getPathSegments().get(1);
			// TODO: nicer
			count = db.delete(POSTS_TABLE_NAME, "intrahash=\"" + segment + "\"", null);
			break;
		case TAGS:
			db.delete(TAGS_TABLE_NAME, null, null);
			break;
		default:
		    throw new IllegalArgumentException("Unknown URI " + uri);
		}
	
		this.getContext().getContentResolver().notifyChange(uri, null);
	
		return count;
    }

    @Override
    public String getType(final Uri uri) {
		switch (this.sUriMatcher.match(uri)) {
		case POSTS:
		    return POST_CONTENT_TYPE;
		case POST:
		    return POST_CONTENT_ITEM_TYPE;
		case DOCUMENTS:
			return DOCUMENT_CONTENT_TYPE;
		case DOCUMENT:
			return DOCUMENT_CONTENT_ITEM_TYPE;
		default:
		    throw new IllegalArgumentException("Unknown URI " + uri);
		}
    }

    @Override
    public Uri insert(final Uri uri, final ContentValues values) {
    	if (values == null) {
    		throw new IllegalArgumentException("values are null");
    	}
    	
		final SQLiteDatabase db = this.helper.getWritableDatabase();
		
		Uri newUri = null;
	
		switch (this.sUriMatcher.match(uri)) {
		case POSTS:
			db.beginTransaction();
			try {
				this.cleanValues(values);
				
			    final String tagStrings = values.getAsString(PostColumns.TAGS);
			    final String groupStrings = values.getAsString(PostColumns.GROUPS);
			    final long postId = db.insert(POSTS_TABLE_NAME, null, values);
			    
			    /*
			     * insert tags 
			     */
			    final String[] tags = tagStrings.split(ModelUtils.SPACE_DELIMITER);
			    final ContentValues postValues = new ContentValues(3);
			    postValues.put(TasColumns.POSTS_ID, postId);
			    for (final String string : tags) {
					if (string != null) {
						postValues.put(TasColumns.TAG_LOWER, string.toLowerCase());
						postValues.put(TasColumns.TAG_NAME, string);
					}
					db.insert(TAS_TABLE_NAME, null, postValues);
				}
			    
			    /*
			     * insert groups
			     */
			    final String[] groups = groupStrings.split(ModelUtils.SPACE_DELIMITER);
			    postValues.clear();
			    postValues.put(GasColumns.POSTS_ID, postId);
			    for (final String group : groups) {
			    	postValues.put(GasColumns.GROUP_NAME, group);
			    	db.insert(GAS_TABLE_NAME, null, postValues);
			    }
			    db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		    final String intrahash = values.getAsString(ResourceColumns.INTRAHASH);
		    newUri = ContentUrisUtils.withAppendedId(uri, intrahash);
		    break;
		case TAGS:
			final String tagName = values.getAsString(TagColumns.TAG_NAME);
			
			if (!present(tagName)) {
				throw new IllegalStateException("tag name can not be empty");
			}
			
			// TODO: only for system tags
			if (!values.containsKey(TagColumns.TAG_NAME_LOWER)) {
				values.put(TagColumns.TAG_NAME_LOWER, tagName.toLowerCase());
			}
			
			db.insert(TAGS_TABLE_NAME, null, values);
			newUri = ContentUrisUtils.withAppendedId(uri, tagName);
			break;
		case DOCUMENTS:
			final String intrahashOfPost = uri.getPathSegments().get(1);
			values.put(DocumentColumns.INTRAHASH, intrahashOfPost);
			db.insert(DOCUMENT_TABLE_NAME, null, values);
			final String fileName = values.getAsString(DocumentColumns.FILE_NAME);
			newUri = getPostsContentUri(this.getContext()).buildUpon().appendPath(intrahashOfPost).appendPath(DOCUMENTS_SUB_PATH).appendPath(fileName).build();
		    break;
		}
	
		if (newUri != null) {
		    this.getContext().getContentResolver().notifyChange(newUri, null);
		    return newUri;
		}
	
		throw new SQLException("Failed to insert row into " + uri);
    }

	/**
	 * @param values
	 */
	protected void cleanValues(final ContentValues values) {
		final Integer contentType = values.getAsInteger(ResourceColumns.CONTENT_TYPE);
		if (contentType == Database.PUBLICATION_CONTENT_TYPE) {
			final String title = values.getAsString(PublicationColumns.TITLE);
			values.put(PublicationColumns.CLEANED_TITLE, BibTexUtils.cleanBibTex(title));
			final String year = values.getAsString(PublicationColumns.YEAR);
			if (present(year)) {
				final String trimmedValue = BibTexUtils.cleanBibTex(year).trim();
				int parseInt = Integer.MAX_VALUE;
				try {
					parseInt = Integer.parseInt(trimmedValue);
				} catch (final NumberFormatException e) {
					// ignore
				}
				values.put(PublicationColumns.CLEANED_YEAR, parseInt);
			}
			
			final String authorsString = values.getAsString(PublicationColumns.AUTHOR);
			final String editorString = values.getAsString(PublicationColumns.EDITOR);
			final String persons;
			if (present(authorsString)) {
				persons = authorsString;
			} else {
				persons = editorString;
			}
			
			final List<PersonName> personList = PersonNameUtils.discoverPersonNamesIgnoreExceptions(persons);
			Collections.sort(personList, new PersonNameComparator());
			values.put(PublicationColumns.SORTED_AUTHORS_EDITORS, PersonNameUtils.serializePersonNames(personList, true));
		}
	}

    @Override
    public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
    	final SQLiteDatabase db = this.helper.getWritableDatabase();
  
    	int count;
	
		switch (this.sUriMatcher.match(uri)) {
		case POST:
			final String intrahash = uri.getPathSegments().get(1);
			this.cleanValues(values);
			count = db.update(POSTS_TABLE_NAME, values, ResourceColumns.INTRAHASH + "=?", new String[]{ intrahash });
		    break;
		default:
		    throw new IllegalArgumentException("Unknown URI " + uri);
		}
	
		this.getContext().getContentResolver().notifyChange(uri, null);
		return count;
    }

}
