package org.bibsonomy.android.providers.database;

import android.provider.BaseColumns;

/**
 * Names of the columns
 * 
 * @author dzo
 */
public interface Database {
	
	/**
	 * descending sort order
	 */
	public static final String DESCENDING = " DESC";
	
	/**
	 * ascending sort order
	 */
	public static final String ASCENDING = " ASC";
	
	/**
	 * the default sort order for bookmarks
	 */
	public static final String BOOKMARKS_DEFAULT_SORT_ORDER = ResourceColumns.TITLE + ASCENDING;
	
	/**
	 * the default sort order for publications
	 */
	public static final String PUBLICATIONS_DEFAULT_SORT_ORDER = PublicationColumns.CLEANED_TITLE + ASCENDING;
	
	/**
	 * the default sort order for tags (for autocompletion)
	 */
	public static final String TAGS_DEFAULT_SORT_ORDER = TagColumns.TAG_USER_COUNT + DESCENDING;
	
	/** the content type of a bookmark */
	public static final int BOOKMARK_CONTENT_TYPE = 1;
	/** the content type of a publication */
	public static final int PUBLICATION_CONTENT_TYPE = 2;
	
	/**
	 * @author dzo
	 */
	public interface PostColumns extends BaseColumns {
		/** user column */
		public static final String USER = "user";
		/** tags column (separated by space) */
		public static final String TAGS = "tags";
		/** description column */
		public static final String DESCRIPTION = "description";
		/** change date column */
		public static final String CHANGEDATE = "changeDate";
		/** posting date column */
		public static final String POSTINGDATE = "postingDate";
		/** groups column (separated by space) */
		public static final String GROUPS = "groups";
		/** the clipboard field */
		public static final String CLIPBOARD = "clipboard";
	}
	
	/**
	 * @author dzo
	 */
	public interface ResourceColumns extends BaseColumns {
		
		/** content type */
		public static final String CONTENT_TYPE = "content_type";

		/** the name of the interhash column */
		public static final String INTERHASH = "interhash";
		
		/**
		 * The intra user hash is relativily strict and takes many fields of this
		 * resource into account.
		 */
		public static final String INTRAHASH = "intrahash";
		
		/** each resource has a title. */
		public static final String TITLE = "title";
	}
	
	/**
	 * @author dzo
	 */
	public interface BookmarkColumns extends ResourceColumns {
		/** the url column name */
		public static final String URL = "url";
	}
	
	/**
	 * @author dzo
	 */
	public interface PublicationColumns extends ResourceColumns {
		/** the authors or editors sorted by author desc */
		public static final String SORTED_AUTHORS_EDITORS = "sortAuthors";
		/** the cleaned title column (cleaned bibtex) */
		public static final String CLEANED_TITLE = "cleaned_title";
		/** the cleaned year column (cleaned bibtex, parsed to int) */
		public static final String CLEANED_YEAR = "cleaned_year";
		
		/** the url column */
		public static final String URL = "url";
		/** the BibTeX key column */
		public static final String BIBTEX_KEY = "bibtexkey";
		/** the key column */
		public static final String KEY = "bkey";
		/** the entrytype column */
		public static final String ENTRYTYPE = "entrytype";
		/** the misc column */
		public static final String MISC = "misc";
		/** the year column */
		public static final String YEAR = "year";
		/** the abstract column */
		public static final String ABSTRACT = "bibtexabstract";
		/** the annote column */
		public static final String ANNOTE = "annote";
		/** the authors column */
		public static final String AUTHOR = "author";
		/** the organization column */
		public static final String ORGANIZATION = "organization";
		/** the book title column */
		public static final String BOOKTITLE = "booktitle";
		/** the address column */
		public static final String ADDRESS = "address";
		/** the chapter column */
		public static final String CHAPTER = "chapter";
		/** the crossref column */
		public static final String CROSSREF = "crossref";
		/** the edition column */
		public static final String EDITION = "edition";
		/** the publisher column */
		public static final String PUBLISHER = "publisher";
		/** the editor column */
		public static final String EDITOR = "editor";
		/** the how published column */
		public static final String HOWPUBLISHED = "howpublished";
		/** the institution column */
		public static final String INSTITUTION = "institution";
		/** the journal column */
		public static final String JOURNAL = "journal";
		/** the note column */
		public static final String NOTE = "note";
		/** the pages column */
		public static final String PAGES = "pages";
		/** the volume column */
		public static final String VOLUME = "volume";
		/** the school column */
		public static final String SCHOOL = "school";
		/** the series column */
		public static final String SERIES = "series";
		/** the number column */
		public static final String NUMBER = "number";
		/** the day column */
		public static final String DAY = "day";
		/** the month column */
		public static final String MONTH = "month";
		/** the type column */
		public static final String TYPE = "type";
		/** the private note column */
		public static final String PRIVNOTE = "privnote";
	}
	
	/**
	 * @author dzo
	 */
	public interface TagColumns {
		/** the tag name column */
		public static String TAG_NAME = "tag_name";
		/** the user tag count column */
		public static String TAG_USER_COUNT = "user_count";
		/** lcase(tag name) column */
		public static String TAG_NAME_LOWER = "tag_lower";
	}
	
	/**
	 * @author dzo
	 */
	public interface TasColumns {
		/** the tag name column */
		public static final String TAG_NAME = "tag_name";
		/** the tag name lower column */
		public static final String TAG_LOWER = "tag_lower";
		/** the id of the post */
		public static final String POSTS_ID = "posts_id";
	}
	
	/**
	 * @author dzo
	 */
	public interface GasColumns {
		/** the group name */
		public static final String GROUP_NAME = "group_name";
		/** the id of the post */
		public static final String POSTS_ID = TasColumns.POSTS_ID;
	}
	
	/**
	 * @author dzo
	 */
	public interface DocumentColumns {
		/** the file name */
		public static final String FILE_NAME = "filename";
		/** the content hash */
		public static final String CONTENT_HASH = "content_hash";
		/** the intrahash of the post the documents belongs to */
		public static final String INTRAHASH = "intrahash";
	}
}
