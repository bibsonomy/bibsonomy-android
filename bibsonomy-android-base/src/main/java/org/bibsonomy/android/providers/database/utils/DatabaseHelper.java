package org.bibsonomy.android.providers.database.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * @author Alex Plischke
 * @author dzo
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {	
	private static final String DATABASE_SCHEMA_SQL = "database/schema.sql";
	private static final String DATABASE_MIGRATION_LOCATION = "database/migrations/";
	
	private static final String DATABASE_NAME = "main.db";
	private static final int DATABASE_VERSION = 27;
	
	private static void executeSQLFile(final Context context, final SQLiteDatabase db, final String fileName) throws IOException {
		final InputStream input = context.getAssets().open(fileName);
		final BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		
		String line;
		while ((line = reader.readLine()) != null) {
			db.execSQL(line);
		}
	}
	
	
    private final Context context;
    
	/**
	 * @param context
	 */
	public DatabaseHelper(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		try {
			executeSQLFile(this.context, db, DATABASE_SCHEMA_SQL);
		} catch (final IOException e) {
			// TODO: ignore?
		}
	}
	
	@Override
	public void onOpen(final SQLiteDatabase db) {
		if (!db.isReadOnly()) {
	      // Enable foreign key constraints
	      db.execSQL("PRAGMA foreign_keys=ON;");
	    }
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, int oldVersion, final int newVersion) {
		final StringBuilder migrationBuilder = new StringBuilder();
		try {
			while (oldVersion != newVersion) {
				migrationBuilder.setLength(0); // clear builder
				migrationBuilder.append(DATABASE_MIGRATION_LOCATION);
				migrationBuilder.append(oldVersion);
				migrationBuilder.append("to");
				migrationBuilder.append(oldVersion);
				migrationBuilder.append(".sql");
				
				executeSQLFile(this.context, db, migrationBuilder.toString());
				oldVersion++;
			}
		} catch (final IOException e) {
			// TODO: handle exception
		}
	}
}
