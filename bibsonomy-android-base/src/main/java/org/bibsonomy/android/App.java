package org.bibsonomy.android;


import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.model.Post;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

/**
 * general information about this app
 * 
 * @author dzo
 */
public class App {
	
	/**
	 * http:// constant
	 */
	public static final String HTTP = "http://";
	
	/**
	 * the path to the beta logging service
	 */
	public static final String LOGGING_PATH = "/scrapingservice/android/logging";
	
	/**
	 * the path to the device support site
	 */
	public static final String DEVICE_SUPPORT_PATH = "/devicesupport";
	
	/**
	 * 
	 * @author dzo
	 *
	 */
	public interface Intents {

		/**
		 * action for adding a new post
		 */
		public static final String ACTION_CREATE_POST = "org.bibsonomy.android.ACTION_ADD";
		
		/**
		 * action for editing an existing post
		 */
		public static final String ACTION_UPDATE_POST = "org.bibsonomy.android.ACTION_EDIT";
		
		/**
		 * This action tells the {@link BibsonomyService} to synchronize with the
		 * server. All newly created/changed {@link Post}s are fetched from the
		 * server or, if they were created on the Android device, uploaded to the
		 * server.
		 */
		public static final String ACTION_SYNC = "org.bibsonomy.android.SYNC";
		
		/**
		 * key for retrieving post in extra bundle
		 */
		public static final String EXTRA_POST = "org.bibsonomy.android.POST";
		
		/**
		 * key for retrieving intrahash in extra bundle
		 */
		public static final String EXTRA_INTRAHASH = "org.bibsonomy.android.INTRAHASH";
		
		/**
		 * key for retrieving bookmark titles in extra bundle
		 */
		public static final String EXTRA_TITLES = "org.bibsonomy.android.TITLES";
		
		/**
		 * key for retrieving bookmark urls in extra bundle
		 */
		public static final String EXTRA_URLS = "org.bibsonomy.android.URLS";
		
		/**
		 * key for retrieving url in extra bundle
		 */
		public static final String EXTRA_URL = "org.bibsonomy.android.URL";

		/**
		 * action for updating a post
		 * the old intrahash must be specified in {@link #EXTRA_INTRAHASH}
		 */
		public static final String ACTION_DELETE_POST = "org.bibsonomy.android.DELETE_POST";

		/**
		 * action for importing bookmarks
		 */
		public static final String ACTION_IMPORT_BOOKMARKS = "org.bibsonomy.android.IMPORT_BOOKMARKS";

		/**
		 * action for delete remote post
		 */
		public static final String ACTION_DELETE_REMOTE_POST = "org.bibsonomy.android.DELETE_REMOTE_POST";

		/**
		 * action for delete locale post
		 */
		public static final String ACTION_DELETE_LOCAL_POST = "org.bibsonomy.android.DELETE_LOCAL_POST";

		/**
		 * if it's a first run
		 */
		public static final String ASK_FOR_SYNC = "org.bibsonomy.android.ASK_FOR_SYNC";
	}
	
	/**
	 * the settings of the app
	 * @author dzo
	 */
	public interface Settings {
		
		/**
		 * the uuid used for syncing
		 */
		public static final String DEVICE_ID = "device_uuid";
		
		/**
		 * the username pref key
		 */
		public static final String USERNAME = "username";
		
		/**
		 * the api key pref key
		 */
		public static final String API_KEY = "api_key";
		
		/**
		 * the bookmark import tag pref key
		 */
		public static final String BOOKMARK_IMPORT_TAG = "bookmark_import_tag";
		
		/**
		 * the groups pref key
		 */
		public static final String GROUPS = "groups";
		
		/**
		 * the user has filter the view with this tags
		 */
		public static final String FILTERED_TAGS = "filtered_tags";

		/**
		 * settings key for sync direction
		 */
		public static final String SYNC_DIRECTION = "sync_direction";
		
		/**
		 * settings key for sync conflict resolution strategy
		 */
		public static final String SYNC_CONFLICT_STATEGY = "sync_conflict_resolution_strategy";
		
		/**
		 * should bookmarks be displayed (and synced) ?
		 */
		public static final String SHOW_BOOKMARKS = "show_bookmarks";
		
		/**
		 * should publicaitons be displayed (and synced) ?
		 */
		public static final String SHOW_PUBLICATIONS = "show_publications";
		
		/**
		 * the date of the last sync
		 */
		public static final String LAST_SYNC_DATE = "last_sync_date";
		
		/**
		 * the last selected resource
		 */
		public static final String SELECTED_RESOURCE = "selected_resource";
	}

	/**
	 * @param context
	 * @return <code>true</code> iff username and api key are set
	 */
	public static boolean validSettings(final Context context) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return present(prefs.getString(Settings.USERNAME, null)) && present(prefs.getString(Settings.API_KEY, null));
	}
	
	/**
	 * informations of third party apps used by this app
	 * @author dzo
	 */
	public static interface ThirdPartyApps {
		
		/**
		 * Barcodes, ISBN
		 * @author dzo
		 */
		public static interface ZXing {
			
			/**
			 * the market uri for ZXing
			 */
			public static final Uri MARKET_URI = Uri.parse("market://search?q=pname:com.google.zxing.client.android");
			
			/**
			 * scan mode
			 */
			public static final String EXTRA_SCAN_MODE = "SCAN_MODE";
			
			/**
			 * qr code mode
			 */
			public static final String QR_CODE = "QR_CODE_MODE";
			
			/**
			 * scan formats
			 */
			public static final String EXTRA_SCAN_FORMATS = "SCAN_FORMATS";
			
			/**
			 * scan format qr code
			 */
			public static final String QR_FORMAT = "QR_CODE";
			
			/**
			 * scan format for isbn
			 */
			public static final String EAN_13_FORMAT = "EAN_13";
			
			/**
			 * package of the app
			 */
			public static final String PACKAGE = "com.google.zxing.client.android";

			/**
			 * intent name of the app
			 */
			public static final String INTENT_NAME = "com.google.zxing.client.android.SCAN";

			/**
			 * key for retrieving the scan result
			 */
			public static final String EXTRA_RESULT = "SCAN_RESULT";
		}
	}
	
	/**
	 * 
	 * @author dzo
	 */
	public static interface Notifications {
		/**
		 * sync notification id
		 */
		public static final int SYNC_NOTIFICATION_ID = 0;
	}

	/**
	 * the host of the scraping service
	 */
	public static final String SCRAPING_SERVICE = "/scrapingservice";
	
	/**
	 * the scraping fail
	 */
	public static final String SCRAPING_FILE_NAME = "supportedScraperSites.json";
}
