package org.bibsonomy.android.activity.util;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.activity.details.AbstractPostDetailsActivity;
import org.bibsonomy.android.activity.details.ShowPublicationDetailsActivity;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditPublicationActivity;
import org.bibsonomy.android.adapter.PublicationAdapter;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.PublicationColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.model.BibTex;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.CursorAdapter;

/**
 * @author dzo
 */
public class PublicationShelfResource implements PostShelfResource<BibTex> {
	private static final String[] PUBLICATION_PROJECTION = { ResourceColumns._ID, PublicationColumns.CLEANED_TITLE, ResourceColumns.INTRAHASH, PublicationColumns.AUTHOR };
	
	
	@Override
	public Class<BibTex> getResourceType() {
		return BibTex.class;
	}
	
	@Override
	public Class<? extends AbstractEditPostActivity<BibTex>> getEditActivityClass() {
		return EditPublicationActivity.class;
	}
	
	@Override
	public String[] getProjection() {
		return PUBLICATION_PROJECTION;
	}
	
	@Override
	public Drawable createDefaultDrawable(final Resources resources) {
		return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, R.drawable.page_blank));
	}
	
	@Override
	public CursorAdapter createAdapter(final Cursor cursor, final Drawable defaultDrawable, final PostShelfActivity activity) {
		return new PublicationAdapter(activity, cursor, defaultDrawable);
	}
	
	@Override
	public String getDefaultSortOrder() {
		return Database.PUBLICATIONS_DEFAULT_SORT_ORDER;
	}
	
	@Override
	public String getSearchRestriction() {
		return ResourceColumns.TITLE + " LIKE ? OR " + PublicationColumns.AUTHOR + " LIKE ?";
	}
	
	@Override
	public Intent getIntent(final PostShelfActivity activity, final Cursor cursor) {
		final Intent details = new Intent(activity, this.getDetailsView());
		details.putExtra(Intents.EXTRA_INTRAHASH, cursor.getString(cursor.getColumnIndex(ResourceColumns.INTRAHASH)));
		return details;
	}
	
	@Override
	public Class<? extends AbstractPostDetailsActivity<BibTex>> getDetailsView() {
		return ShowPublicationDetailsActivity.class;
	}
}
