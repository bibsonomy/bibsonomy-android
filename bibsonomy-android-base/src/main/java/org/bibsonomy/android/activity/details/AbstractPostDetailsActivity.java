package org.bibsonomy.android.activity.details;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.utils.DatabaseUtils;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

/**
 * 
 * TODO: move post specific handling (change date, ...) to this class
 * 
 * @author dzo
 * @param <RESOURCE> 
 */
public abstract class AbstractPostDetailsActivity<RESOURCE extends Resource> extends Activity {	
	protected LayoutInflater inflater;
	
	@Override
	protected final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(this.getDetailsView());
		this.inflater = this.getLayoutInflater();
		
		final String intrahash = this.getIntent().getStringExtra(Intents.EXTRA_INTRAHASH);
		
		final Cursor postCursor = this.managedQuery(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash), null, null, null, null);
		final Post<RESOURCE> post = DatabaseUtils.convertPost(postCursor);
		
		this.loadPostsExtras(intrahash);
		
		// back button
		final ImageButton backButton = (ImageButton) this.findViewById(R.id.back);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				AbstractPostDetailsActivity.this.finish();
			}
		});
		
		// edit button
		final ImageButton editButton = (ImageButton) this.findViewById(R.id.edit);
		editButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				final Intent editIntent = new Intent(AbstractPostDetailsActivity.this, AbstractPostDetailsActivity.this.getEditActivityClass());
				editIntent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
				editIntent.setAction(Intents.ACTION_UPDATE_POST	);
				AbstractPostDetailsActivity.this.startActivity(editIntent);
			}
		});
		
		// delete button
		final ImageButton deleteButton = (ImageButton) this.findViewById(R.id.delete_post);
		deleteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				final Intent deleteIntent = new Intent(AbstractPostDetailsActivity.this, BibsonomyService.class);
				deleteIntent.setAction(Intents.ACTION_DELETE_POST);
				deleteIntent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
				AbstractPostDetailsActivity.this.startService(deleteIntent);
				AbstractPostDetailsActivity.this.finish();
			}
		});
		
		this.setupView(post);
	}
	
	protected void loadPostsExtras(final String intrahash) {
		// noop
	}
	
	protected abstract void setupView(final Post<RESOURCE> post);

	protected abstract int getDetailsView();
	
	protected abstract Class<? extends AbstractEditPostActivity<RESOURCE>> getEditActivityClass();
}
