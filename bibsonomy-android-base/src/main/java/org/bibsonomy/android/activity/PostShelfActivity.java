package org.bibsonomy.android.activity;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.android.App;
import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.App.ThirdPartyApps;
import org.bibsonomy.android.activity.details.ShowPublicationDetailsActivity;
import org.bibsonomy.android.activity.edit.EditBookmarkActivity;
import org.bibsonomy.android.activity.edit.EditPublicationActivity;
import org.bibsonomy.android.activity.service.LoginActivity;
import org.bibsonomy.android.activity.service.SettingsActivity;
import org.bibsonomy.android.activity.util.BookmarkShelfResource;
import org.bibsonomy.android.activity.util.PostShelfResource;
import org.bibsonomy.android.activity.util.PublicationShelfResource;
import org.bibsonomy.android.adapter.holder.ResourceHolder;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.task.ScrapePublicationTask;
import org.bibsonomy.android.utils.AsyncTask.AsyncTaskCallback;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.CustomExceptionHandler;
import org.bibsonomy.android.utils.FileManager;
import org.bibsonomy.android.utils.ItemAdapter;
import org.bibsonomy.android.utils.NetworkUtils;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.android.view.ShelfView;
import org.bibsonomy.common.exceptions.UnsupportedResourceTypeException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.factories.ResourceFactory;
import org.bibsonomy.util.id.ISBNUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

/**
 * Shelf display of posts (publications and bookmarks)
 * 
 * @author dzo
 * @author Alex Plischke
 */
public class PostShelfActivity extends Activity implements OnTouchListener, OnItemClickListener, OnScrollListener, OnItemSelectedListener, OnSharedPreferenceChangeListener {
	
	private static final int LOGIN_REQUEST = 22;
	private static final int QRCODE_REQUEST = 42;
	private static final StringBuilder mBuddy = new StringBuilder();
	private static final String[] selectionArgsRestricted = new String[2];
	
	private static final String PROGRESS_SHOWING = "progressShowing";
	private static final String ISBN_NOT_FOUND_DIALOG = "isbnNotFoundDialog";
	private static final String NO_NETWORK_DIALOG = "noNetworkDialog";
	
	private static final int LOAD_COVERS_MESSAGE_ID = 84;
	private static final int DELAY_SHOW_COVERS = 550;
	
	private Pattern systemPattern;
	
	private int scrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
	private boolean pendingCoverUpdate = false;

	private PostShelfResource<? extends Resource> postShelfResource;
	
	protected Cursor cursor;
	protected CursorAdapter adapter;
	
	private ShelfView shelfView;
	private RadioGroup resourceSwitcher;
	private CoverHandler handler;
	private boolean fingerUp;
	
	private ScrapePublicationTask scrapeTask;    
    private ProgressDialog scrapeProgressDialog;
    private AlertDialog scrapingFailedDialog;
    private AlertDialog noNetworkDialog;
	private Object resourceType;

	private ImageButton syncButton;
	private ImageButton tagButton;
	private View resourceSwitcherContainer;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.systemPattern = Pattern.compile("https?://(www.)?" + this.getString(R.string.webservice) + "/(bibtex|publication)/2([0-9a-fA-F]{32})/(.+)");
		
		final SharedPreferences defaultPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		defaultPreferences.registerOnSharedPreferenceChangeListener(this);
		
		this.setContentView(R.layout.main);
		
		this.shelfView = (ShelfView) this.findViewById(R.id.grid_shelfview);
		this.registerForContextMenu(this.shelfView);
		this.shelfView.setOnTouchListener(this);
		this.shelfView.setOnItemClickListener(this);
		this.handler = new CoverHandler();
		
		this.shelfView.setOnScrollListener(this);
		this.shelfView.setOnItemSelectedListener(this);
		
		this.resourceSwitcher = (RadioGroup) this.findViewById(R.id.resourceswitcher);
		this.resourceSwitcher.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(final RadioGroup group, final int checkedId) {
				final int checkedButtonId = group.getCheckedRadioButtonId();
				final View rb = group.findViewById(checkedButtonId);
				final int index = group.indexOfChild(rb);
				final Class<? extends Resource> resourceClass;
				// XXX: :(
				if (index == 0) {
					resourceClass = Bookmark.class;
				} else {
					resourceClass = BibTex.class;
				}
				PostShelfActivity.this.setResourceType(resourceClass);
			}
		});
    	
		/*
		 * bind some views for later use
		 */
		this.tagButton = (ImageButton) this.findViewById(R.id.tagSelector);
		this.syncButton = (ImageButton) this.findViewById(R.id.sync_button);
		this.resourceSwitcherContainer = this.findViewById(R.id.resource_switcher_container);
		
    	/*
    	 * set the default values for all preferences
    	 */
    	PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    	final SharedPreferences prefs = defaultPreferences;
    	
    	if (!prefs.getBoolean(Settings.SHOW_BOOKMARKS, false) || !prefs.getBoolean(Settings.SHOW_PUBLICATIONS, false)) {
    		this.resourceSwitcherContainer.setVisibility(View.GONE);
    	}
    	
    	final Class<? extends Resource> resourceClass = ResourceFactory.getResourceClass(prefs.getString(Settings.SELECTED_RESOURCE, ResourceFactory.BOOKMARK_CLASS_NAME));
    	this.setResourceType(resourceClass);
	
		/*
		 * For beta testing purpose only.
		 * proguard removes this call while building the release app
		 */
		this.registerUncaughtExceptionHandler();
		
		if (present(savedInstanceState)) {
			this.restoreSavedInstanceState(savedInstanceState);
		}
		
		/*
		 * if credentials are not set, start the login activity.
		 */
		if (!App.validSettings(this)) {
		    final Intent startLogin = new Intent(this, LoginActivity.class);
		    this.startActivityForResult(startLogin, LOGIN_REQUEST);
		}
		
		final Intent intent = this.getIntent();
		if (present(intent)) {
			this.handleIntent(intent);
		}
		
		this.updateView();
	}
	
	private void setResourceType(final Class<? extends Resource> resourceClass) {
		if ((this.resourceType == null) || !this.resourceType.equals(resourceClass)) {
			this.resourceType = resourceClass;
			final RadioButton radioButton;
			if (this.resourceType == Bookmark.class) {
				this.postShelfResource = new BookmarkShelfResource();
				radioButton = (RadioButton) this.resourceSwitcher.getChildAt(0);
			} else if (this.resourceType == BibTex.class) {
				this.postShelfResource = new PublicationShelfResource();
				radioButton = (RadioButton) this.resourceSwitcher.getChildAt(1);
			} else {
				throw new UnsupportedResourceTypeException();
			}
			radioButton.setChecked(true);
			this.updateView();
		}
	}
	
	private void handleIntent(final Intent intent) {
		if (intent.getBooleanExtra(Intents.ASK_FOR_SYNC, false)) {
			intent.removeExtra(Intents.ASK_FOR_SYNC);
			final Builder builder = new Builder(this);
			final String syncTitle = this.getString(R.string.sync);
			builder.setTitle(syncTitle);
			builder.setMessage(R.string.first_sync);
			builder.setPositiveButton(syncTitle, new DialogInterface.OnClickListener() {
			
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					PostShelfActivity.this.startSync(null);
				}
			});
			builder.setNegativeButton(R.string.cancel, null);
			builder.show();
		}
	}

	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
    	this.getMenuInflater().inflate(R.menu.post_shelf_context, menu);

    	final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
    	final TextView titleView = (TextView) info.targetView.findViewById(R.id.title);
    	menu.setHeaderTitle(titleView.getText());
	}

	protected void restoreSavedInstanceState(final Bundle savedInstanceState) {
    	if (savedInstanceState.getBoolean(PROGRESS_SHOWING)) {
			this.scrapeTask = (ScrapePublicationTask) this.getLastNonConfigurationInstance();
			this.showProgressDialog();
	    } else if (savedInstanceState.getBoolean(ISBN_NOT_FOUND_DIALOG)) {
	    	this.scrapingFailedDialog();
	    } else if (savedInstanceState.getBoolean(NO_NETWORK_DIALOG)) {
	    	this.showNoNetworkAlertDialog();
	    }
    }

    private void showProgressDialog() {
    	this.scrapeProgressDialog = ProgressDialog.show(this, this.getText(R.string.fetching_data), this.getText(R.string.in_progress), true, false);
	}

	private void scrapingFailedDialog() {
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.failed);
    	builder.setMessage(R.string.no_entries_found);
    	builder.setPositiveButton(R.string.ok, null);
    	this.scrapingFailedDialog = builder.show();
	}

	private void showNoNetworkAlertDialog() {
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.failed).setMessage(R.string.error_no_network).setPositiveButton(R.string.ok, null);
    	this.noNetworkDialog = builder.show();
	}

    @Override
    public Object onRetainNonConfigurationInstance() {
    	return this.scrapeTask;
    }

	@Override
    protected void onSaveInstanceState(final Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	outState.putBoolean(PROGRESS_SHOWING, (this.scrapeProgressDialog != null) && this.scrapeProgressDialog.isShowing());
    	outState.putBoolean(ISBN_NOT_FOUND_DIALOG, (this.scrapingFailedDialog != null) && this.scrapingFailedDialog.isShowing());
    	outState.putBoolean(NO_NETWORK_DIALOG, (this.noNetworkDialog != null) && this.noNetworkDialog.isShowing());
    }

	private void updateView() {
		if (present(this.cursor)) {
			this.cursor.close();
		}
		
		final android.net.Uri.Builder builder = ItemProvider.getPostsContentUri(this).buildUpon();
		ContentUrisUtils.appendParam(builder, ItemProvider.RESOUCRE_TYPE_PARAM, ResourceFactory.getResourceName(this.postShelfResource.getResourceType()));
		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		final String tags = prefs.getString(Settings.FILTERED_TAGS, null);
		if (present(tags)) {
			this.tagButton.setImageResource(R.drawable.ab_tags_selected);
			ContentUrisUtils.appendParam(builder, ItemProvider.TAGS_PARAM, tags);
		} else {
			this.tagButton.setImageResource(R.drawable.ab_tags);
		}
		this.cursor = this.managedQuery(builder.build(), this.getProjection(), null, null, this.getDefaultSortOrder());
		
		this.adapter = this.postShelfResource.createAdapter(this.cursor, this.getDefaultDrawable(), this);
		this.shelfView.setAdapter(this.adapter);
	}
	
	protected String[] getProjection() {
		return this.postShelfResource.getProjection();
	}

	@Override
    public boolean onTouch(final View v, final MotionEvent event) {    	
    	final int action = event.getAction();
    	this.fingerUp = (action == MotionEvent.ACTION_UP) || (action == MotionEvent.ACTION_CANCEL);
    	
    	if (this.fingerUp && (this.scrollState != SCROLL_STATE_FLING)) {
            this.initCoverLoading();
        }
    	
    	return false;
    }
	
	private void initCoverLoading() {		
		final Handler handler = this.handler;
		final Message message = handler.obtainMessage(LOAD_COVERS_MESSAGE_ID, this);
        handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
        this.pendingCoverUpdate = true;
        handler.sendMessage(message);
	}
	
	private void loadCovers() {
		this.pendingCoverUpdate = false;
		final GridView gridView = this.shelfView;
		
		final int firstVisiblePosition = gridView.getFirstVisiblePosition();
		final int lastVisiblePosition = gridView.getLastVisiblePosition();
		
		for (int i = firstVisiblePosition; i <= lastVisiblePosition; i++) {
			final View view = gridView.getChildAt(i);
			if (!present(view)) {
				// TODO: check me as soon as we have covers
				Log.e("", "position was " + i + " (" + firstVisiblePosition + "-" + lastVisiblePosition + ")");
				continue;
			}
            final ResourceHolder holder = (ResourceHolder) view.getTag();
            if (holder.queryCover) {
            	final String intraHash = holder.intraHash;
            	
            	Drawable cover = FileManager.getCover(this.getResources(), intraHash);
            	if (cover == null) {
            		cover = this.getDefaultDrawable();
            	}
            	
            	holder.image.setImageDrawable(cover);
            	holder.queryCover = false;
            }
		}
	}

	private Drawable getDefaultDrawable() {
		return this.postShelfResource.createDefaultDrawable(this.getResources());
	}

	@Override
    protected void onNewIntent(final Intent intent) {
		super.onNewIntent(intent);
		this.setIntent(intent);
	
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {			
		    final String query = intent.getStringExtra(SearchManager.QUERY);
		    final StringBuilder buddy = mBuddy;
		    buddy.setLength(0);
		    final String selectionRestricted = buddy.append('%').append(query).append('%').toString();
			selectionArgsRestricted[0] = selectionRestricted;
		    selectionArgsRestricted[1] = selectionRestricted;
		    
		    this.cursor.close();
		    this.cursor = this.managedQuery(ItemProvider.getPostsContentUri(this), this.getProjection(), this.postShelfResource.getSearchRestriction(), selectionArgsRestricted, this.getDefaultSortOrder());
		    this.adapter.changeCursor(this.cursor);
		}
    }
	
	@Override
    public boolean onContextItemSelected(final MenuItem item) {
    	final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    	this.cursor.moveToPosition(info.position);
    	final String intrahash = this.cursor.getString(2);
		// FIXME: adapt
    	/* switch (item.getItemId()) {
		
		case R.id.context_item_show_details:
			final Intent intent = new Intent(this, this.postShelfResource.getDetailsView());
			intent.putExtra(App.Intents.EXTRA_INTRAHASH, intrahash);
			this.startActivity(intent);
			return true;
		case R.id.context_item_edit:
			this.editPost(intrahash);
			return true;
		case R.id.context_item_delete:
		    this.deletePost(intrahash);
		    return true;
		}
		*/ 
		return super.onContextItemSelected(item);
    }

	/**
	 * @param intrahash
	 */
	protected void deletePost(final String intrahash) {
		final Intent intent = new Intent(this, BibsonomyService.class);
		intent.setAction(Intents.ACTION_DELETE_POST);
		intent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
		this.startService(intent);
	}

	/**
	 * @param intrahash
	 */
	protected void editPost(final String intrahash) {
		final Intent editIntent = new Intent(this, this.postShelfResource.getEditActivityClass());
		editIntent.setAction(Intents.ACTION_UPDATE_POST);
		editIntent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
		this.startActivity(editIntent);
	}
	
	protected void editPost(final Post<BibTex> result) {
		final Intent intent = new Intent(this, EditPublicationActivity.class);
		intent.setAction(Intents.ACTION_CREATE_POST);
		intent.putExtra(Intents.EXTRA_POST, new PostParcel<BibTex>(result));
		this.startActivity(intent);
	}

	protected String getDefaultSortOrder() {
		return this.postShelfResource.getDefaultSortOrder();
	}

	/**
	 * @return the builder for the dialog
	 * 
	 */
	protected Builder createZXIngDialog() {
		final AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
		downloadDialog.setTitle(R.string.barcode_scanner);
		downloadDialog.setMessage(R.string.barcore_requirement);
		downloadDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(final DialogInterface dialogInterface, final int i) {
		    	final Intent intent = new Intent(Intent.ACTION_VIEW, ThirdPartyApps.ZXing.MARKET_URI);
		    	PostShelfActivity.this.startActivity(intent);
		    }
		});
		downloadDialog.setNegativeButton(R.string.no, null);
		return downloadDialog;
	}
	
	@Override
	public void onScrollStateChanged(final AbsListView view, final int scrollState) {
		/*
		 * scroll fling to other scroll state 
		 */
		final Handler handler = this.handler;
		if ((this.scrollState == SCROLL_STATE_FLING) && (scrollState != SCROLL_STATE_FLING)) {
            final Message message = handler.obtainMessage(LOAD_COVERS_MESSAGE_ID, this);
            handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
            handler.sendMessageDelayed(message, this.fingerUp ? 0 : DELAY_SHOW_COVERS);
            this.pendingCoverUpdate = true;
        } else if (scrollState == SCROLL_STATE_FLING) {
            this.pendingCoverUpdate = false;
            handler.removeMessages(LOAD_COVERS_MESSAGE_ID);
        }
		
		this.scrollState = scrollState;
	}
	
	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
		// nothing to do yet
	}
	
	private class CoverHandler extends Handler {
		@Override
		public void handleMessage(final Message msg) {
			switch (msg.what) {
			case LOAD_COVERS_MESSAGE_ID:
				final PostShelfActivity activity = (PostShelfActivity) msg.obj;
				activity.loadCovers();
				break;
			}
		}
	}
	
	@Override
	public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
		if (this.scrollState != SCROLL_STATE_IDLE) {
            this.scrollState = SCROLL_STATE_IDLE;
            this.initCoverLoading();
        }
	}
	
	@Override
	public void onNothingSelected(final AdapterView<?> adapterView) {
		// nothing to do
	}

	@Override
	public void onItemClick(final AdapterView<?> adapterView, final View view, final int pos, final long id) {
		this.cursor.moveToPosition(pos);
		final Intent intent = this.postShelfResource.getIntent(this, this.cursor);
		this.startActivity(intent);
	}
	
	protected void registerUncaughtExceptionHandler() {
		/*
		 * register only one handler 
		 */
		if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
			Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(App.HTTP + this.getString(R.string.webservice) + App.LOGGING_PATH));
		}
	}
	
	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if ((requestCode == LOGIN_REQUEST) && (resultCode == RESULT_CANCELED)) {
			// no login information terminate this app
			this.finish();
		}
		
		if ((requestCode == 55) && (resultCode == 55)) {
			// sync finished
			this.syncButton.setEnabled(true);
			// this.syncButton.clearAnimation(); // TODO: stop rotating
		}
		
		if ((requestCode == QRCODE_REQUEST) && (resultCode == RESULT_OK)) {
			final String scanResult = data.getStringExtra(ThirdPartyApps.ZXing.EXTRA_RESULT);
			
			if (present(scanResult)) {
				final Matcher matcher = this.systemPattern.matcher(scanResult);
				
				if (matcher.matches()) {
					final String intrahash = matcher.group(3);
					final String username = matcher.group(4);
					
					final Cursor cursor = this.managedQuery(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash), null, null, null, null);
					final int count = cursor.getCount();
					cursor.close();
					
					final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
					final String loggedInUser = prefs.getString(Settings.USERNAME, "");
					final List<String> options = new LinkedList<String>();
					final boolean inDb = count > 0;
					final boolean ownEntry = inDb && loggedInUser.equalsIgnoreCase(username);
					if (inDb) {
						// show, edit and delete
						options.add(this.getString(R.string.show_post_details));
						options.add(this.getString(R.string.edit_post));
						options.add(this.getString(R.string.delete_post));
					} else if (ownEntry) {
						// FIXME: sync 
					} else {
						// copy to own collection
						options.add(this.getString(R.string.add_post));
						
					}
					// TODO: clipboard
					// options.add(this.getString(R.string.add_to_clipboard));
					options.add(this.getString(R.string.cancel));
					
					final Builder dialog = new Builder(this);
				    dialog.setTitle(this.getString(R.string.options));
				    dialog.setItems(options.toArray(new String[options.size()]), new DialogInterface.OnClickListener() {
				        @Override
						public void onClick(final DialogInterface dialog, final int item) {
				            if (ownEntry) {
				            	switch (item) {
								case 0:
									final Intent showPublicationIntent = new Intent(PostShelfActivity.this, ShowPublicationDetailsActivity.class);
									showPublicationIntent.putExtra(Intents.EXTRA_INTRAHASH, intrahash);
									PostShelfActivity.this.startActivity(showPublicationIntent);
									break;
				            	case 1:
				            		PostShelfActivity.this.editPost(intrahash);
				            		break;
				            	case 2:
				            		PostShelfActivity.this.deletePost(intrahash);
				            		break;
				            	}
				            	// TODO: clip board implementation
				            } else {
				            	switch (item) {
								case 0:
									// copy item => first scrape the publication
									PostShelfActivity.this.scrape(scanResult, null);
									break;
								}
				            }
				            // else cancel 
				        }
				    }).show();
				} else {
					final String isbn = ISBNUtils.extractISBN(scanResult);
					final String issn = ISBNUtils.extractISSN(scanResult);
					if (present(isbn) || present(issn)) {
						// try to scrape with isbn or issn
						this.scrape(null, present(isbn) ? isbn : issn);
					} else {
						final Intent intent = new Intent(this, EditBookmarkActivity.class);
					    final Post<Bookmark> post = new Post<Bookmark>();
					    final Bookmark bookmark = new Bookmark();
					    post.setResource(bookmark);
					    bookmark.setUrl(scanResult);
					    intent.setAction(Intents.ACTION_CREATE_POST);
					    intent.putExtra(Intents.EXTRA_POST, new PostParcel<Bookmark>(post));
					    this.startActivity(intent);
					}
				}
			} else {
				// TODO: show error message? canceled
			}
		}
	}
	
	/**
	 * @param url
     * @param selectedText
	 */
	protected void scrape(final String url, final String selectedText) {
		if (NetworkUtils.isNetworkAvailable(this)) {
			/*
			 * show progress dialog
			 */
			this.scrapeTask = new ScrapePublicationTask(this.getString(R.string.webservice));
			this.scrapeTask.setCallback(new AsyncTaskCallback<Post<BibTex>>() {

				@Override
				public void gotResult(final Post<BibTex> result) {
					// dismiss progress dialog
					PostShelfActivity.this.scrapeProgressDialog.dismiss();
					// show error view when nothing scraped
					if (result == null) {
						PostShelfActivity.this.scrapingFailedDialog();
						return;
					}
					// else edit the post
					// TODO: use tags from scraping service for recommendation
					result.getTags().clear();
					PostShelfActivity.this.editPost(result);
				}
			});
			this.scrapeTask.execute(url, selectedText);
			this.showProgressDialog();
		} else {
			this.showNoNetworkAlertDialog();
		}
	}
	
	/**
	 * @return the pendingCoverUpdate
	 */
	public boolean isPendingCoverUpdate() {
		return this.pendingCoverUpdate;
	}
	
	/**
	 * @return the scrollState
	 */
	public int getScrollState() {
		return this.scrollState;
	}
	
	public void restrictTags(final View view) {
		final Intent chooseTagsIntent = new Intent(this, ChooseTagsActivity.class);
		this.startActivity(chooseTagsIntent);
	}
	
	public void search(final View view) {
		this.onSearchRequested();
	}
	
	public void addPost(final View view) {
		final Intent intent = new Intent(this, this.postShelfResource.getEditActivityClass());
		this.startActivity(intent);
	}

	public void startSync(final View view) {
		// TODO: response in the view, spin sync button
		final Animation loadAnimation = AnimationUtils.loadAnimation(PostShelfActivity.this, R.anim.rotate_button);
		this.syncButton.startAnimation(loadAnimation);
		final Intent syncIntent = new Intent(this, BibsonomyService.class);
		syncIntent.setAction(Intents.ACTION_SYNC);
		this.syncButton.setEnabled(false);
		
		final PendingIntent pendingIntent = PendingIntent.getService(PostShelfActivity.this, 55, syncIntent, 0);
		try {
			pendingIntent.send(55);
		} catch (final CanceledException e) {
			Log.e("PostShelfActivity", "sync canceled", e);
		}
	}
	
	public void showMore(final View v) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(PostShelfActivity.this);
	    builder.setTitle("Advanced options")
	    .setAdapter(new ItemAdapter(PostShelfActivity.this, android.R.layout.select_dialog_item, Arrays.asList(
	    		new ItemAdapter.Item(PostShelfActivity.this.getString(R.string.scan), R.drawable.ic_menu_camera),
	    		new ItemAdapter.Item(PostShelfActivity.this.getString(R.string.settings), R.drawable.ic_menu_preferences))), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				final Intent intent;
				switch (which) {
				case 0:
					intent = new Intent(ThirdPartyApps.ZXing.INTENT_NAME);
				    intent.setPackage(ThirdPartyApps.ZXing.PACKAGE);
				    intent.putExtra(ThirdPartyApps.ZXing.EXTRA_SCAN_MODE, ThirdPartyApps.ZXing.EAN_13_FORMAT + "," + ThirdPartyApps.ZXing.QR_FORMAT);
			
				    try {
				    	PostShelfActivity.this.startActivityForResult(intent, QRCODE_REQUEST);
				    } catch (final ActivityNotFoundException e) {
						PostShelfActivity.this.createZXIngDialog().show();
				    }
				    break;
				case 1:
					intent = new Intent(PostShelfActivity.this, SettingsActivity.class);
				    PostShelfActivity.this.startActivity(intent);
				    
				    break;
				default:
					break;
				}
			}
			
		}).show();
	}
	
	@Override
	public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
		if (Settings.FILTERED_TAGS.equals(key)) {
			this.updateView();
		}
		
		if (Settings.SHOW_BOOKMARKS.equals(key) || Settings.SHOW_PUBLICATIONS.equals(key)) {
			final boolean showBookmarks = sharedPreferences.getBoolean(Settings.SHOW_BOOKMARKS, false);
			final boolean showPublications = sharedPreferences.getBoolean(Settings.SHOW_PUBLICATIONS, false);
			if (!showBookmarks || !showPublications) {
				if (showBookmarks) {
					this.setResourceType(Bookmark.class);
				}
				
				if (showPublications) {
					this.setResourceType(BibTex.class);
				}
				// disable resource switcher
				this.resourceSwitcherContainer.setVisibility(View.GONE);
			} else {
				this.resourceSwitcherContainer.setVisibility(View.VISIBLE);
			}
		}
	}
}
