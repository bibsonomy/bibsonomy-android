package org.bibsonomy.android.activity.edit;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.base.R;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * {@link Activity} for adding AND editing {@link Bookmark}s.
 * 
 * @author Alex Plischke
 */
public class EditBookmarkActivity extends AbstractEditPostActivity<Bookmark> {
	private EditText mUrlInput;
	private EditText mTitleInput;
	private EditText mDescriptionInput;

	@Override
	protected void onInternalCreate(final Bundle savedInstanceState) {
		this.mUrlInput = (EditText) this.findViewById(R.id.url_edit);
		this.mTitleInput = (EditText) this.findViewById(R.id.title_edit);
		this.mDescriptionInput = (EditText) this.findViewById(R.id.description_edit);
	}

	@Override
	protected int getContentView() {
		return R.layout.add_bookmark;
	}

	@Override
	protected Bookmark createResource() {
		return new Bookmark();
	}

	@Override
	protected void populatePostWithView(final Post<Bookmark> post) {
		super.populatePostWithView(post);
		post.setDescription(this.mDescriptionInput.getText().toString());
	}

	@Override
	protected void populateResourceWithView(final Bookmark resource) {
		resource.setUrl(this.mUrlInput.getText().toString());
		resource.setTitle(this.mTitleInput.getText().toString());
	}

	@Override
	protected void populateViewWithPost(final Post<Bookmark> post) {
		super.populateViewWithPost(post);

		this.mDescriptionInput.setText(post.getDescription());
	}

	@Override
	protected void populateViewWithResource(final Bookmark bookmark) {
		this.mUrlInput.setText(bookmark.getUrl());
		this.mTitleInput.setText(bookmark.getTitle());
	}

	@Override
	protected boolean validateResource(final Bookmark bookmark) {
		boolean valid = true;
		if (!ModelUtils.isValidURL(bookmark.getUrl())) {
			valid = false;
			this.showToast(this.getText(R.string.not_valid_url), Toast.LENGTH_SHORT);
		}

		if (!present(bookmark.getTitle())) {
			valid = false;
			this.showToast(this.getText(R.string.not_valid_title), Toast.LENGTH_SHORT);
		}

		return valid;
	}

	@Override
	protected int getDuplicateErrorMessageId() {
		return R.string.duplicate_bookmark;
	}
}
