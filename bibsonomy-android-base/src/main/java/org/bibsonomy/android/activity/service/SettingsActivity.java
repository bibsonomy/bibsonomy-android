package org.bibsonomy.android.activity.service;

import java.util.Map.Entry;

import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.utils.ModelUtils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Manages the settings of the app.
 * 
 * @author Alex Plischke
 * 
 */
public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.addPreferencesFromResource(R.xml.preferences);
		
		final SharedPreferences sharedPreferences = this.getPreferenceScreen().getSharedPreferences();
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
		
		for (final Entry<String, ?> preference : sharedPreferences.getAll().entrySet()) {
			final String prefKey = preference.getKey();
			this.setSummary(prefKey);
		}
		
		// add validation for import tag
		final Preference importTagPref = this.findPreference(Settings.BOOKMARK_IMPORT_TAG);
		importTagPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(final Preference preference, final Object newValue) {
				final boolean valid = ModelUtils.isValidTagName((String) newValue);
				
				if (!valid) {
					Toast.makeText(SettingsActivity.this, R.string.invalid_tag, Toast.LENGTH_SHORT).show();
				}
				
				return valid;
			}
		});
		
		final Preference showBookmarks = this.findPreference(Settings.SHOW_BOOKMARKS);
		final Preference showPublications = this.findPreference(Settings.SHOW_PUBLICATIONS);
		
		showBookmarks.setOnPreferenceChangeListener(new ShowResourcePerferenceListener(showPublications));
		showPublications.setOnPreferenceChangeListener(new ShowResourcePerferenceListener(showBookmarks));
    }

	/**
	 * @param prefKey
	 */
	protected void setSummary(final String prefKey) {
		final Preference value = this.findPreference(prefKey);
		
		if (value instanceof EditTextPreference) {
			final EditTextPreference pref = (EditTextPreference) value;
			pref.setSummary(pref.getText());
		} else if (value instanceof ListPreference) {
			final ListPreference listPreference = (ListPreference) value;
			final int selectedIndex = listPreference.findIndexOfValue(listPreference.getValue());
			listPreference.setSummary(listPreference.getEntries()[selectedIndex]);
		}
	}

	@Override
	public void onSharedPreferenceChanged(final SharedPreferences preferences, final String prefKey) {
		this.setSummary(prefKey);
	}

	
	private class ShowResourcePerferenceListener implements OnPreferenceChangeListener {
		private final Preference otherResource;
		
		private ShowResourcePerferenceListener(final Preference otherPreference) {
			this.otherResource = otherPreference;
		}
		
		@Override
		public boolean onPreferenceChange(final Preference preference, final Object newValue) {
			final boolean newBoolean = (Boolean) newValue;
			final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);

			final boolean valid = newBoolean || prefs.getBoolean(this.otherResource.getKey(), false);
			
			if (!valid) {
				Toast.makeText(SettingsActivity.this, R.string.invalid_resources, Toast.LENGTH_SHORT).show();
			}
					
			return valid;
		}
	}
}
