package org.bibsonomy.android.activity.util;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.activity.details.AbstractPostDetailsActivity;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.model.Resource;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.widget.CursorAdapter;

public interface PostShelfResource<R extends Resource> {

	public Class<R> getResourceType();

	public String[] getProjection();

	public Drawable createDefaultDrawable(Resources resources);

	public CursorAdapter createAdapter(Cursor cursor, Drawable defaultDrawable, PostShelfActivity activity);

	public String getDefaultSortOrder();

	public Class<? extends AbstractEditPostActivity<R>> getEditActivityClass();

	public String getSearchRestriction();

	public Intent getIntent(PostShelfActivity postShelfActivity, Cursor cursor);

	public Class<? extends AbstractPostDetailsActivity<R>> getDetailsView();

}
