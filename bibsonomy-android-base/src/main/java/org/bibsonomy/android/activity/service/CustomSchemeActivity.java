package org.bibsonomy.android.activity.service;

import org.bibsonomy.android.App;
import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.activity.PostShelfActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

/**
 * handles all request comming from bibsonomy://* addresses
 * @author dzo
 */
public class CustomSchemeActivity extends Activity {
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final Intent intent = this.getIntent();
		final Uri data = intent.getData();
		if (data != null) {
			final String host = data.getHost();
			
			/*
			 * handle username and api key
			 */
			if ("settings".equals(host)) {
				/*
				 * retrieve username and api key
				 */
				final String username = data.getQueryParameter("username");
				final String apiKey = data.getQueryParameter("apiKey");
				
				/*
				 * check if username or api key have changed
				 */
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				final String oldUsername = prefs.getString(App.Settings.USERNAME, null);
				final String oldApiKey = prefs.getString(App.Settings.API_KEY, null);
				
				if (apiKey != null && !apiKey.equals(oldApiKey) || username != null && !username.equals(oldUsername)) {
					/*
					 * save settings
					 */
					final Editor editor = prefs.edit();
					editor.putString(App.Settings.USERNAME, username);
					editor.putString(App.Settings.API_KEY, apiKey);
					editor.commit();
					
					// TODO: first run: ask user to import existing browser bookmarks
					// after sync?
				}
				
				final Intent startMainView = new Intent(this, PostShelfActivity.class);
				startMainView.putExtra(Intents.ASK_FOR_SYNC, true);
				startActivity(startMainView);
				
				this.finish();
			}
		}
	}
}