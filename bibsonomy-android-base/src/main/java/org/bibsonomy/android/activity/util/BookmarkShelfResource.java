package org.bibsonomy.android.activity.util;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.activity.details.AbstractPostDetailsActivity;
import org.bibsonomy.android.activity.details.ShowBookmarkDetailsActivity;
import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditBookmarkActivity;
import org.bibsonomy.android.adapter.BookmarkAdapter;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.BookmarkColumns;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.model.Bookmark;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.CursorAdapter;

/**
 * 
 * @author dzo
 */
public class BookmarkShelfResource implements PostShelfResource<Bookmark> {
	
	private static final String[] BOOKMARK_PROJECTION = { ResourceColumns._ID, ResourceColumns.TITLE, ResourceColumns.INTRAHASH, BookmarkColumns.URL };

	@Override
	public Class<Bookmark> getResourceType() {
		return Bookmark.class;
	}

	@Override
	public String[] getProjection() {
		return BOOKMARK_PROJECTION;
	}

	@Override
	public Drawable createDefaultDrawable(final Resources resources) {
		return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, R.drawable.web_page));
	}

	@Override
	public CursorAdapter createAdapter(final Cursor cursor, final Drawable defaultDrawable, final PostShelfActivity activity) {
		return new BookmarkAdapter(activity, cursor, defaultDrawable);
	}

	@Override
	public String getDefaultSortOrder() {
		return Database.BOOKMARKS_DEFAULT_SORT_ORDER;
	}

	@Override
	public Class<? extends AbstractEditPostActivity<Bookmark>> getEditActivityClass() {
		return EditBookmarkActivity.class;
	}

	@Override
	public String getSearchRestriction() {
		return ResourceColumns.TITLE + " LIKE ?";
	}

	@Override
	public Intent getIntent(final PostShelfActivity activity, final Cursor cursor) {
		return new Intent(Intent.ACTION_VIEW, Uri.parse(cursor.getString(cursor.getColumnIndex(BookmarkColumns.URL))));
	}
	
	@Override
	public Class<? extends AbstractPostDetailsActivity<Bookmark>> getDetailsView() {
		return ShowBookmarkDetailsActivity.class;
	}
}
