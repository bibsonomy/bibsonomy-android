package org.bibsonomy.android.activity.details;

import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditBookmarkActivity;
import org.bibsonomy.android.base.R;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Tag;

import android.content.res.Resources;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * TODO: implement me
 * 
 * @author Alex Plischke
 * 
 */
public class ShowBookmarkDetailsActivity extends AbstractPostDetailsActivity<Bookmark> {

	@Override
	protected int getDetailsView() {
		return R.layout.show_bookmark_details;
	}
	
	@Override
	protected Class<? extends AbstractEditPostActivity<Bookmark>> getEditActivityClass() {
		return EditBookmarkActivity.class;
	}

	@Override
	protected void setupView(final Post<Bookmark> post) {
		final Bookmark bookmark = post.getResource();

		final TextView urlView = (TextView) this.findViewById(R.id.url);
		urlView.setText(bookmark.getUrl());

		final TextView titleView = (TextView) this.findViewById(R.id.title);
		titleView.setText(bookmark.getTitle());

		final TextView descriptionView = (TextView) this.findViewById(R.id.description);
		descriptionView.setText(post.getDescription());

		final LinearLayout tagsContainer = (LinearLayout) this.findViewById(R.id.tags_container);
		for (final Tag tag : post.getTags()) {
			final LinearLayout tagLayout = (LinearLayout) this.getLayoutInflater().inflate(R.layout.tag, tagsContainer, false);
			final TextView tagView = ((TextView) tagLayout.findViewById(R.id.tag_name));
			tagView.setText(tag.getName());
			tagsContainer.addView(tagLayout);
		}

		final Resources res = this.getResources();

		final String[] visOptionsInternal = res
				.getStringArray(R.array.visibility_options_internal);
		final String[] visOptionsForDisplay = res
				.getStringArray(R.array.visibility_options);
		final int length = visOptionsInternal.length;
		final String group = post.getGroups().iterator().next().getName(); // TODO:
																			// adapt
																			// mulitple
																			// groups
		boolean found = false;

		final TextView visibilityView = (TextView) this.findViewById(R.id.visibility);

		for (int i = 0; i < length; i++) {
			if (visOptionsInternal[i].equals(group)) {
				visibilityView.setText(visOptionsForDisplay[i]);
				found = true;
				break;
			}
		}
		if (!found) {
			visibilityView.setText(group);
		}
	}
}
