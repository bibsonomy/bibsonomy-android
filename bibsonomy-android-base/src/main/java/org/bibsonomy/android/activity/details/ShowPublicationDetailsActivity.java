package org.bibsonomy.android.activity.details;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.List;

import org.bibsonomy.android.activity.edit.AbstractEditPostActivity;
import org.bibsonomy.android.activity.edit.EditPublicationActivity;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.utils.DatabaseUtils;
import org.bibsonomy.android.utils.FileManager;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameUtils;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * This class was supposed to display details of a given {@link BibTex}. Due to
 * time contraints, its use could not be implemented.
 * 
 * @author Alex Plischke
 */
public class ShowPublicationDetailsActivity extends AbstractPostDetailsActivity<BibTex> {

	private List<Document> documents;
	
	@Override
	protected void setupView(final Post<BibTex> post) {
		final String intraHash = post.getResource().getIntraHash();
		// handle clipboard button
		final ImageButton clipboardButton = (ImageButton) this.findViewById(R.id.clipboard_button);
		final boolean picked = post.isPicked();
		if (picked) {
			clipboardButton.setImageResource(R.drawable.ab_clipboard_selected);
		}
		
		clipboardButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(final View v) {
				final boolean currentlyPicked = post.isPicked();
				if (currentlyPicked) {
					// TODO: unpick it
					clipboardButton.setImageResource(R.drawable.ab_clipboard);
				} else {
					// TODO: pick it
					clipboardButton.setImageResource(R.drawable.ab_clipboard_selected);
				}
				
				post.setPicked(!currentlyPicked);
			}
		});
		
		final BibTex publication = post.getResource();
		final TextView title = (TextView) this.findViewById(R.id.title);
		title.setText(BibTexUtils.cleanBibTex(publication.getTitle()));
		
		final TextView abstractTextView = (TextView) this.findViewById(R.id.abstract_text);
		abstractTextView.setText(BibTexUtils.cleanBibTex(publication.getAbstract()));
		
		final TextView authorsView = (TextView) this.findViewById(R.id.authors);
		
		final List<PersonName> persons;
		final List<PersonName> authors = publication.getAuthor();
		if (present(authors)) {
			persons = authors;
		} else {
			persons = publication.getEditor();
		}
		
		authorsView.setText(PersonNameUtils.serializePersonNames(persons, (String) this.getText(R.string.and)));
		
		if (present(this.documents)) {
			final LinearLayout documentsContainer = (LinearLayout) this.findViewById(R.id.documents_container);
			for (final Document document : this.documents) {
				final LinearLayout documentsView = (LinearLayout) this.getLayoutInflater().inflate(R.layout.document, documentsContainer, false);
				final TextView tagView = ((TextView) documentsView.findViewById(R.id.document_name));
				tagView.setText(document.getFileName());
				documentsContainer.addView(documentsView);
				documentsContainer.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(final View v) {
						try {
							final Uri path = Uri.parse(FileManager.getPathToDocument(ShowPublicationDetailsActivity.this, intraHash, document));
							final Intent viewIntent = new Intent(Intent.ACTION_VIEW, path);
							viewIntent.setType("application/pdf"); // FIXME: also images and so on
							ShowPublicationDetailsActivity.this.startActivity(viewIntent);
						} catch (final ActivityNotFoundException e) {
							// TODO: show info
							Log.e("TODO", "");
						}
					}
				});
			}
		}
		
		// TODO: all other fields
	}
	
	@Override
	protected Class<? extends AbstractEditPostActivity<BibTex>> getEditActivityClass() {
		return EditPublicationActivity.class;
	}

	@Override
	protected int getDetailsView() {
		return R.layout.show_publication_details;
	}
	
	@Override
	protected void loadPostsExtras(final String intrahash) {
		final Cursor documentsCursor = this.managedQuery(ItemProvider.getPostsContentUri(this).buildUpon().appendPath(intrahash).appendPath(ItemProvider.DOCUMENTS_SUB_PATH).build(), null, null, null, null);
		this.documents = DatabaseUtils.convertDocuments(documentsCursor);
	}
}
