package org.bibsonomy.android.activity.edit;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Set;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.adapter.TagAutoCompleteCursorAdapter;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.utils.DatabaseUtils;
import org.bibsonomy.android.service.BibsonomyService;
import org.bibsonomy.android.utils.ContentUrisUtils;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.android.utils.tokenizer.SpaceTokenizer;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.util.GroupUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * 
 * @author dzo
 * 
 * @param <RESOURCE>
 */
public abstract class AbstractEditPostActivity<RESOURCE extends Resource> extends Activity {

	private String[] mVisibilityOptionsWithGroupsInternal;
	private String[] mVisibilityOptionsInternal;

	protected LayoutInflater layoutInflator;

	private Spinner mVisibility;

	protected MultiAutoCompleteTextView tagsView;

	/**
	 * model
	 */
	private Post<RESOURCE> post;
	private String intrahashToUpdate;

	@Override
	protected final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setContentView(this.getContentView());
		final Resources res = this.getResources();

		this.layoutInflator = this.getLayoutInflater();

		this.mVisibilityOptionsInternal = res.getStringArray(R.array.visibility_options_internal);
		final String[] visOptionsForDisplay = res.getStringArray(R.array.visibility_options);

		this.tagsView = (MultiAutoCompleteTextView) this.findViewById(R.id.tagsAutocomplete);
		this.tagsView.setTokenizer(new SpaceTokenizer());
		
		final Cursor c = this.managedQuery(ItemProvider.getTagsContentUri(this), null, null, null, Database.TAGS_DEFAULT_SORT_ORDER);
		this.tagsView.setAdapter(new TagAutoCompleteCursorAdapter(this, c));
		this.tagsView.setThreshold(1); // one charactor for autocompletion

		/*
		 * cache views
		 */
		this.mVisibility = (Spinner) this.findViewById(R.id.visibility_spinner);

		final String[] visOptionsInternal = this.mVisibilityOptionsInternal;

		/*
		 * setup group chooser
		 */
		final String additionalGroups = PreferenceManager.getDefaultSharedPreferences(this).getString(Settings.GROUPS, null);
		if (additionalGroups != null) {
			final String[] groups = TextUtils.split(additionalGroups, ModelUtils.SPACE_DELIMITER);
			final int combinedLength = groups.length + visOptionsForDisplay.length;

			final String[] combinedForDisplay = new String[combinedLength];
			final String[] combinedInternal = new String[combinedLength];
			this.mVisibilityOptionsWithGroupsInternal = combinedInternal;

			System.arraycopy(visOptionsForDisplay, 0, combinedForDisplay, 0, visOptionsForDisplay.length);
			System.arraycopy(visOptionsInternal, 0, combinedInternal, 0, visOptionsInternal.length);
			System.arraycopy(groups, 0, combinedForDisplay, visOptionsForDisplay.length, groups.length);
			System.arraycopy(groups, 0, combinedInternal, visOptionsInternal.length, groups.length);

			final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, combinedForDisplay);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			this.mVisibility.setAdapter(adapter);
		} else {
			this.mVisibilityOptionsWithGroupsInternal = visOptionsInternal;

			final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, visOptionsForDisplay);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			this.mVisibility.setAdapter(adapter);
		}

		final Button cancelButton = (Button) this.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				AbstractEditPostActivity.this.finish();
			}
		});

		final Button saveButton = (Button) this.findViewById(R.id.save);
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				AbstractEditPostActivity.this.savePost();
			}
		});

		this.onInternalCreate(savedInstanceState);

		this.post = this.retrievePost();
		final Set<Group> groups = this.post.getGroups();
		if (!present(groups)) {
			groups.add(GroupUtils.getPublicGroup());
		}

		this.populateViewWithPost(this.post);
	}

	protected void onInternalCreate(final Bundle savedInstanceState) {
		// noop
	}

	protected void populateViewWithPost(final Post<RESOURCE> post) {
		/*
		 * tags
		 */
		this.tagsView.setText(ModelUtils.convertTags(post.getTags()));

		/* 
		 * TODO: mulitple groups
		 * groups
		 */
		final String[] visOptionsInternal = this.mVisibilityOptionsWithGroupsInternal;
		final int length = this.mVisibility.getCount();

		final String group = post.getGroups().iterator().next().getName();
		for (int i = 0; i < length; i++) {
			if (visOptionsInternal[i].equals(group)) {
				this.mVisibility.setSelection(i);
				break;
			}
		}

		/*
		 * resource
		 */
		this.populateViewWithResource(post.getResource());
	}

	protected abstract void populateViewWithResource(final RESOURCE resource);

	protected boolean validatePost() {
		boolean valid = true;

		// tags
		if (this.post.getTags().isEmpty()) {
			valid = false;
			this.showToast(this.getText(R.string.at_least_one_tag), Toast.LENGTH_SHORT);
		}

		// TODO: groups

		// resource
		return valid && this.validateResource(this.post.getResource());
	}

	protected abstract boolean validateResource(final RESOURCE resource);

	protected Post<RESOURCE> retrievePost() {
		final Intent intent = this.getIntent();
		final String action = intent.getAction();

		if (Intents.ACTION_UPDATE_POST.equals(action)) {
			final String intrahash = intent.getStringExtra(Intents.EXTRA_INTRAHASH);

			final Cursor postCursor = this.managedQuery(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash), null, null, null, null);
			final Post<RESOURCE> post = DatabaseUtils.convertPost(postCursor);

			// TODO: :(
			this.intrahashToUpdate = intrahash;

			return post;
		} else if (Intents.ACTION_CREATE_POST.equals(action)) {
			// TODO: extract tags for recommendation
			@SuppressWarnings("unchecked")
			final PostParcel<RESOURCE> postParcel = (PostParcel<RESOURCE>) intent.getParcelableExtra(Intents.EXTRA_POST);
			return postParcel.getPost();
		}

		/*
		 * else create an empty post
		 */
		final Post<RESOURCE> post = new Post<RESOURCE>();
		post.setResource(this.createResource());
		post.getGroups().add(GroupUtils.getPublicGroup());
		return post;
	}

	/**
	 * Shows a {@link Toast}.
	 * 
	 * @param msg
	 *            the message to appear in the {@link Toast}
	 * @param duration
	 *            how long the {@link Toast} should stay visible
	 */
	protected void showToast(final CharSequence msg, final int duration) {
		Toast.makeText(this, msg, duration).show();
	}

	protected abstract RESOURCE createResource();

	private void savePost() {
		this.populatePostWithView(this.post);

		final boolean validatePost = this.validatePost();

		if (!validatePost) {
			return;
		}

		final Resource resource = this.post.getResource();
		resource.recalculateHashes();

		final String newIntraHash = resource.getIntraHash();

		final Intent intent = new Intent(this, BibsonomyService.class);
		intent.putExtra(Intents.EXTRA_POST, new PostParcel<RESOURCE>(this.post));
		if (this.intrahashToUpdate == null) {
			/*
			 * create post
			 */
			if (this.hasDuplicate(newIntraHash)) {
				this.showToast(this.getText(this.getDuplicateErrorMessageId()), Toast.LENGTH_SHORT);
				return;
			}
			intent.setAction(Intents.ACTION_CREATE_POST);
		} else {
			/*
			 * update post
			 */
			if (!this.intrahashToUpdate.equals(newIntraHash) && this.hasDuplicate(newIntraHash)) {
				this.showToast(this.getText(this.getDuplicateErrorMessageId()), Toast.LENGTH_SHORT);
				return;
			}

			intent.putExtra(Intents.EXTRA_INTRAHASH, this.intrahashToUpdate);
			intent.setAction(Intents.ACTION_UPDATE_POST);
		}
		/*
		 * execute service and finish activity
		 */
		this.startService(intent);
		this.finish();
	}

	protected abstract int getDuplicateErrorMessageId();

	private boolean hasDuplicate(final String intrahash) {
		final Cursor cursor = this.managedQuery(ContentUrisUtils.withAppendedId(ItemProvider.getPostsContentUri(this), intrahash), null, null, null, null);
		final int count = cursor.getCount();
		cursor.close();
		return count > 0;
	}

	protected void populatePostWithView(final Post<RESOURCE> post) {
		// tags
		post.setTags(ModelUtils.convertToTags(this.tagsView.getText().toString()));

		// group
		post.getGroups().clear();
		post.getGroups().add(new Group(this.mVisibilityOptionsWithGroupsInternal[this.mVisibility.getSelectedItemPosition()]));

		// resource
		this.populateResourceWithView(post.getResource());
	}

	protected abstract void populateResourceWithView(final RESOURCE resource);

	protected abstract int getContentView();
}
