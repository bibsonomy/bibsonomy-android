package org.bibsonomy.android.activity.service;

import org.bibsonomy.android.App;
import org.bibsonomy.android.base.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Is the application started for the very first time or the user settings are
 * not set, then this {@link Activity} will be displayed, so that the user can
 * login to the service and send the apikey to the app
 * {@link CustomSchemeActivity}
 * 
 * @author Alex Plischke
 * @author dzo
 */
public class LoginActivity extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.setContentView(R.layout.login);
    	
    	final Button simpleButton = (Button) this.findViewById(R.id.simpleConfig);
    	simpleButton.setOnClickListener(new View.OnClickListener() {
    		@Override
    		public void onClick(final View v) {
    			final Intent openBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(App.HTTP + LoginActivity.this.getString(R.string.webservice) + App.DEVICE_SUPPORT_PATH));
				LoginActivity.this.startActivity(openBrowserIntent);
				
				LoginActivity.this.finish();
    		}
    	});
    	
    	// TODO: register link
    }
}
