package org.bibsonomy.android.activity.edit;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.android.base.R;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Use this class for creating a new {@link BibTex} or editing an
 * already existing one.
 * 
 * @author Alex Plischke
 * 
 */
public class EditPublicationActivity extends AbstractEditPostActivity<BibTex> {
    private static final String LOG_TAG = "EditPublicationActivity";
	private static final String SYSTEM_TAG_MYOWN = "myown";
    private static final Pattern SYSTEM_TAG_MYOWN_PATTERN = Pattern.compile("(^|\\s+)" + SYSTEM_TAG_MYOWN + "(\\s+|$)");
    
    private static enum OptionalField {
    	ABSTRACT_FIELD(R.string.abstract_field),
    	ADDRESS(R.string.address),
    	ANNOTE(R.string.annote),
    	BOOK_TITLE(R.string.book_title),
    	BIBTEX_KEY(R.string.bibtex_key),
    	CHAPTER(R.string.chapter),
    	CROSSREF(R.string.crossref),
    	DAY(R.string.day),
    	DESCRIPTION(R.string.description),
    	EDITION(R.string.edition),
    	HOW_PUBLISHED(R.string.how_published),
    	INSTITUTION(R.string.institution),
    	JOURNAL(R.string.journal),
    	KEY(R.string.key),
    	MONTH(R.string.month),
    	NOTE(R.string.note),
    	NUMBER(R.string.number),
    	ORGANIZATION(R.string.organization),
    	PAGES(R.string.pages),
    	PRIVATE_NOTE(R.string.private_note),
    	PUBLISHER(R.string.publisher),
    	SCHOOL(R.string.school),
    	SERIES(R.string.series),
    	TYPE(R.string.type),
    	URL(R.string.url),
    	VOLUME(R.string.volume);
    	
    	private final int resourceId;
    	
    	private OptionalField(final int resourceId) {
    		this.resourceId = resourceId;
    	}

		/**
		 * @return the resourceId
		 */
		public int getResourceId() {
			return this.resourceId;
		}
    }
    
    private ViewGroup optionalFieldsContainer;
    private ViewGroup miscFieldsContainer;

    private Spinner entryTypeSpinner;
    private EditText title;
    private EditText year;
    private EditText author;
    private EditText editor;
    private CheckBox myownCheckBox;
    
    private Map<OptionalField, EditText> optionalFields;

    @Override
    protected void onInternalCreate(final Bundle savedInstanceState) {    	
    	/*
    	 * setup entrytype spinner
    	 */
    	this.entryTypeSpinner = (Spinner) this.findViewById(R.id.entry_type);
    	final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, BibTexUtils.ENTRYTYPES);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.entryTypeSpinner.setAdapter(adapter);
    	
    	this.optionalFieldsContainer = (ViewGroup) this.findViewById(R.id.optional_fields_container);
    	this.miscFieldsContainer = (ViewGroup) this.findViewById(R.id.misc_fields_container);
    	
    	this.title = (EditText) this.findViewById(R.id.title);
    	this.year = (EditText) this.findViewById(R.id.year);
    	this.author = (EditText) this.findViewById(R.id.authorEditText);
    	this.editor = (EditText) this.findViewById(R.id.editorEditText);

    	this.myownCheckBox = (CheckBox) this.findViewById(R.id.author_toggle);
    	this.myownCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
    		
    		private final StringBuilder builder = new StringBuilder();

		    @Override
		    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
				final Editable text = EditPublicationActivity.this.tagsView.getText();
				if (isChecked && !SYSTEM_TAG_MYOWN_PATTERN.matcher(text).find()) {
					this.builder.setLength(0);
				   	this.builder.append(text);
				   	if (this.builder.length() != 0) {
				   		this.builder.append(" ");
				   	}
				   	this.builder.append(SYSTEM_TAG_MYOWN);
				   	EditPublicationActivity.this.tagsView.setText(this.builder.toString());
				} else {
					final Matcher matcher = SYSTEM_TAG_MYOWN_PATTERN.matcher(text);
					if (matcher.find()) {
						EditPublicationActivity.this.tagsView.setText(matcher.replaceFirst(matcher.group(1)));
					}
				}
		    }
    	});

		final ImageButton addOptFieldButton = (ImageButton) this.findViewById(R.id.add_optional_field_button);
		addOptFieldButton.setOnClickListener(new View.OnClickListener() {
	
		    @Override
		    public void onClick(final View v) {
		    	EditPublicationActivity.this.showAddOptionalFieldDialog();
		    }
		});

		final ImageButton addMiscFieldButton = (ImageButton) this.findViewById(R.id.add_misc_field_button);
		addMiscFieldButton.setOnClickListener(new View.OnClickListener() {
	
		    @Override
		    public void onClick(final View v) {
		    	EditPublicationActivity.this.showAddMiscFieldDialog();
		    }
		});
    }
    
    @Override
    protected int getContentView() {
    	return R.layout.add_publication;
    }
    
    @Override
    protected BibTex createResource() {
    	return new BibTex();
    }
    
    @Override
    protected void populatePostWithView(final Post<BibTex> post) {
    	super.populatePostWithView(post);
    	
    	// optional fields
    	this.setOptionalFields(post);
    }

	@Override
	protected void populateResourceWithView(final BibTex resource) {
		resource.setEntrytype((String) this.entryTypeSpinner.getSelectedItem());
		resource.setTitle(this.title.getText().toString());
		resource.setYear(this.year.getText().toString());
		resource.setAuthor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(this.author.getText().toString()));
		resource.setEditor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(this.editor.getText().toString()));

		this.setMiscFields(resource);
	}
    
    @Override
    protected void populateViewWithPost(final Post<BibTex> post) {
    	super.populateViewWithPost(post);
    	
    	// optional fields
    	this.setOptionalFieldsInContainer(post);
    	
    	/*
    	 * if no BibTeX key was provided generate it
    	 */
    	final BibTex publication = post.getResource();
		if (!present(publication.getBibtexKey())) {
    		publication.setBibtexKey(BibTexUtils.generateBibtexKey(publication));
    	}
    }
    
    @Override
	protected void populateViewWithResource(final BibTex publication) {
    	// entry type
    	final String[] entryOptions = BibTexUtils.ENTRYTYPES;
	    final int length = entryOptions.length;
	    for (int i = 0; i < length; i++) {
	    	if (entryOptions[i].equals(publication.getEntrytype())) {
	    		this.entryTypeSpinner.setSelection(i);
	    		break;
	    	}
	    }
	    
	    // title
	    this.title.setText(publication.getTitle());
	    
	    // author and editor
	    this.author.setText(PersonNameUtils.serializePersonNames(publication.getAuthor(), "\n"));
	    this.editor.setText(PersonNameUtils.serializePersonNames(publication.getEditor(), "\n"));
	    
	    this.year.setText(publication.getYear());
	    
	    // misc fields
	    publication.parseMiscField();
	    final Map<String, String> map = publication.getMiscFields();
	    for (final Entry<String, String> entry : map.entrySet()) {
	    this.addMiscField(entry.getKey(), entry.getValue());
	    }
    }
    
    @Override
    protected boolean validateResource(final BibTex publication) {
    	boolean valid = true;    	
    	if (!present(publication.getTitle())) {
    	    valid = false;
    	   this.showToast(this.getText(R.string.not_valid_title), Toast.LENGTH_SHORT);
    	}
    	
    	final String year = publication.getYear();
		if (!present(year)) {
    	   valid = false;
    	   this.showToast(this.getText(R.string.not_valid_year), Toast.LENGTH_SHORT);
    	}
		
    	if (!present(publication.getAuthor()) && !present(publication.getEditor())) {
    	   valid = false;
    	   this.showToast(this.getText(R.string.not_valid_person), Toast.LENGTH_SHORT);
    	}
    	
    	return valid;
    }
    
    @Override
    protected int getDuplicateErrorMessageId() {
    	return R.string.duplicate_publication;
    }

    /**
     * Sets the misc input fields in the {@link BibTex}.
     * 
     * @param post
     *            the {@link Post} that the {@link BibTex} belongs to
     */
    private void setMiscFields(final BibTex publication) {
    	final ViewGroup container = this.miscFieldsContainer;
    	final int numChildren = container.getChildCount();
    	final StringBuilder buddy = new StringBuilder(" ");
    	
    	final List<String> combinedList = new ArrayList<String>(numChildren);

    	for (int i = 0; i < numChildren; i++) {
    		final ViewGroup layout = (ViewGroup) container.getChildAt(i);
    		final TextView fieldName = (TextView) layout.findViewById(R.id.field_name);
    		final EditText fieldInput = (EditText) layout.findViewById(R.id.field_edit);
    		final String input = fieldInput.getText().toString();
    		
    		if (!present(input) || ModelUtils.isValidMiscFieldName(fieldName.getText().toString())) {
    			continue;
    		}
    		
    		publication.addMiscField(fieldName.getText().toString(), input);
    		combinedList.add(buddy.toString());
    		buddy.setLength(0);
    	}
    }

    /**
     * Sets the optional input fields in the {@link BibTex}.
     * 
     * @param post
     *            the {@link Post} that the {@link BibTex} belongs to
     */
    private void setOptionalFields(final Post<BibTex> post) {
    	final BibTex publication = post.getResource();
    	
    	for (final OptionalField optionalField : OptionalField.values()) {
			String input = null;
    		if ((this.optionalFields != null) && this.optionalFields.containsKey(optionalField)) {
    			final EditText editText = this.optionalFields.get(optionalField);
    			input = editText.getText().toString();
			}
    		
    		switch (optionalField) {
			case ABSTRACT_FIELD:
				publication.setAbstract(input);
				break;
			case ADDRESS:
				publication.setAddress(input);
				break;
			case ANNOTE:
				publication.setAnnote(input);
				break;
			case BIBTEX_KEY:
				publication.setBibtexKey(input);
				break;
			case BOOK_TITLE:
				publication.setBooktitle(input);
				break;
			case CHAPTER:
				publication.setChapter(input);
				break;
			case CROSSREF:
				publication.setCrossref(input);
				break;
			case DAY:
				publication.setDay(input);
				break;
			case DESCRIPTION:
				post.setDescription(input);
				break;
			case EDITION:
				publication.setEdition(input);
				break;
			case HOW_PUBLISHED:
				publication.setHowpublished(input);
				break;
			case INSTITUTION:
				publication.setInstitution(input);
				break;
			case JOURNAL:
				publication.setJournal(input);
				break;
			case KEY:
				publication.setKey(input);
				break;
			case MONTH:
				publication.setMonth(input);
				break;
			case NOTE:
				publication.setNote(input);
				break;
			case NUMBER:
				publication.setNumber(input);
				break;
			case ORGANIZATION:
				publication.setOrganization(input);
				break;
			case PAGES:
				publication.setPages(input);
				break;
			case PRIVATE_NOTE:
				publication.setPrivnote(input);
				break;
			case PUBLISHER:
				publication.setPublisher(input);
				break;
			case SCHOOL:
				publication.setSchool(input);
				break;
			case SERIES:
				publication.setSeries(input);
				break;
			case TYPE:
				publication.setType(input);
				break;
			case URL:
				publication.setUrl(input);
				break;
			case VOLUME:
				publication.setVolume(input);
				break;
			default:
				Log.e(LOG_TAG, "unkown optional field " + optionalField);
				break;
			}
		}
    }

    /**
     * Sets the optional fields according to the values of the {@link BibTex}.
     * The {@link BibTex} must already exist for that and is usually given along
     * with the {@link Intent} that started this {@link Activity}.
     */
    private void setOptionalFieldsInContainer(final Post<BibTex> post) {
    	final BibTex publication = post.getResource();
	this.addOptionalField(OptionalField.ABSTRACT_FIELD, publication.getAbstract());
	this.addOptionalField(OptionalField.ADDRESS, publication.getAddress());
	this.addOptionalField(OptionalField.ANNOTE, publication.getAnnote());
	this.addOptionalField(OptionalField.BIBTEX_KEY, publication.getBibtexKey());
	this.addOptionalField(OptionalField.BOOK_TITLE, publication.getBooktitle());
	this.addOptionalField(OptionalField.CHAPTER, publication.getChapter());
	this.addOptionalField(OptionalField.CROSSREF, publication.getCrossref());
	this.addOptionalField(OptionalField.DAY, publication.getDay());
	this.addOptionalField(OptionalField.DESCRIPTION, post.getDescription());
	this.addOptionalField(OptionalField.EDITION, publication.getEdition());
	this.addOptionalField(OptionalField.HOW_PUBLISHED, publication.getHowpublished());
	this.addOptionalField(OptionalField.INSTITUTION, publication.getInstitution());
	this.addOptionalField(OptionalField.JOURNAL, publication.getJournal());
	this.addOptionalField(OptionalField.KEY, publication.getKey());
	this.addOptionalField(OptionalField.MONTH, publication.getMonth());
	this.addOptionalField(OptionalField.NOTE, publication.getNote());
	this.addOptionalField(OptionalField.NUMBER, publication.getNumber());
	this.addOptionalField(OptionalField.ORGANIZATION, publication.getOrganization());
	this.addOptionalField(OptionalField.PAGES, publication.getPages());
	this.addOptionalField(OptionalField.PRIVATE_NOTE, publication.getPrivnote());
	this.addOptionalField(OptionalField.PUBLISHER, publication.getPublisher());
	this.addOptionalField(OptionalField.SCHOOL, publication.getSchool());
	this.addOptionalField(OptionalField.SERIES, publication.getSeries());
	this.addOptionalField(OptionalField.TYPE, publication.getType());
	this.addOptionalField(OptionalField.URL, publication.getUrl());
	this.addOptionalField(OptionalField.VOLUME, publication.getVolume());
	}

	/**
	 * Shows the dialog for adding a new misc field.
	 */
	private void showAddMiscFieldDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final ViewGroup addMiscFieldLayout = (ViewGroup) this.layoutInflator.inflate(R.layout.add_misc_field, null, false);
		builder.setView(addMiscFieldLayout);

		final EditText inputField = (EditText) addMiscFieldLayout.findViewById(R.id.input);

		builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				final String name = inputField.getText().toString().trim();
				final View view = EditPublicationActivity.this.miscFieldsContainer.findViewWithTag(name);
				// TODO: reimplemt me
				// if (StringUtils.isValidMiscFieldName(name) && (view
				// == null)) {
				// addMiscField(name, null);
				// }
			}
		}).setNegativeButton(R.string.cancel, null).show();
	}

    /**
     * Adds the misc field to the layout.
     * 
     * @param name
     *            the name of the misc field
     * @param content
     *            the actual content of the misc field
     */
    private void addMiscField(final CharSequence name, final CharSequence content) {
    	final ViewGroup miscFieldLayout = (ViewGroup) this.layoutInflator.inflate(R.layout.optional_field_for_edit,this.miscFieldsContainer, false);
    	miscFieldLayout.setTag(name);
    	final TextView fieldName = (TextView) miscFieldLayout.findViewById(R.id.field_name);
    	fieldName.setText(name);
    	if (content != null) {
    		final EditText fieldContent = (EditText) miscFieldLayout.findViewById(R.id.field_edit);
    		fieldContent.setText(content);
    	}
    	this.miscFieldsContainer.addView(miscFieldLayout);

    	final ImageButton removeButton = (ImageButton) miscFieldLayout.findViewById(R.id.remove_button);
    	removeButton.setOnClickListener(new View.OnClickListener() {

    		@Override
    		public void onClick(final View v) {
    			EditPublicationActivity.this.miscFieldsContainer.removeView(miscFieldLayout);
    		}
    	});
    }

    /**
     * Shows the dialog for adding a new optional field to the layout.
     */
    private void showAddOptionalFieldDialog() {
    	final Set<OptionalField> allOptinalFields = new HashSet<OptionalField>(Arrays.asList(OptionalField.values()));
    	
    	final Map<OptionalField, EditText> optionalFields = this.optionalFields;
		if (optionalFields != null) {
			allOptinalFields.removeAll(optionalFields.keySet());
    	}
    	
    	
    	final Map<String, OptionalField> optionalFieldNames = new LinkedHashMap<String, OptionalField>();
    	for (final OptionalField optionalField : allOptinalFields) {
			optionalFieldNames.put(this.getString(optionalField.getResourceId()), optionalField);
		}
    	
    	final Set<String> set = new TreeSet<String>(optionalFieldNames.keySet());
    	final String[] fields = new String[set.size()];
    	set.toArray(fields);
    	
    	final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setItems(fields, new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(final DialogInterface dialog, final int which) {
		    	final String selected = fields[which];
		    	final OptionalField optionalField = optionalFieldNames.get(selected);

		    EditPublicationActivity.this.addOptionalField(optionalField, null, true);
		    	dialog.dismiss();
		    }

			
		}).setPositiveButton(R.string.cancel, null).show();
    }
    
    private void addOptionalField(final OptionalField optionalField, final String value) {
    	this.addOptionalField(optionalField, value, false);
    }
    
	/**
	 * Adds the optional field to the layout.
	 * 
	 * @param name
	 *            name of the optional field
	 * @param value
	 *            actual content of the optional field
	 * @param fieldNumber
	 *            the field number for easier extraction
	 */
	private void addOptionalField(final OptionalField optionalField, final String value, final boolean newOptinalField) {
		if (!newOptinalField && !present(value)) {
			return;
		}
		
		final ViewGroup container = this.optionalFieldsContainer;
		final ViewGroup optFieldLayout = (ViewGroup) this.layoutInflator.inflate(R.layout.optional_field_for_edit, container, false);
		
		/*
		 * set field name
		 */
		final TextView fieldName = (TextView) optFieldLayout.findViewById(R.id.field_name);
		fieldName.setText(this.getString(optionalField.getResourceId()));
		
		final EditText inputField = (EditText) optFieldLayout.findViewById(R.id.field_edit);
		inputField.setText(value);

		/*
		 * add to map
		 * lazy load map
		 */
		if (this.optionalFields == null) {
			this.optionalFields = new LinkedHashMap<OptionalField, EditText>();
		}
		
		final Map<OptionalField, EditText> optionalFields = this.optionalFields;
		
		optionalFields.put(optionalField, inputField);
		
		/*
		 * add to view container
		 */
		container.addView(optFieldLayout);

		final ImageButton removeButton = (ImageButton) optFieldLayout.findViewById(R.id.remove_button);
		removeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				container.removeView(optFieldLayout);
				optionalFields.remove(optionalField);
			}
		});
	}
}
