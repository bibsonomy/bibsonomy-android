package org.bibsonomy.android.activity;

import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.adapter.TagAutoCompleteCursorAdapter;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.utils.ModelUtils;
import org.bibsonomy.model.Tag;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * activity for choosing tags
 * 
 * @author dzo
 */
public class ChooseTagsActivity extends Activity {
	
	private SortedSet<Tag> tags;
	private LinearLayout tagFilterContainer;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.choose_tags);
		
		this.tagFilterContainer = (LinearLayout) this.findViewById(R.id.tagFilterContainer);
		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	this.tags = new TreeSet<Tag>(ModelUtils.convertToTags(prefs.getString(Settings.FILTERED_TAGS, null)));
    	
    	for (final Tag tag : this.tags) {
			this.addTag(tag);
		}
		
		final Cursor c = this.managedQuery(ItemProvider.getTagsContentUri(this), null, null, null, Database.TAGS_DEFAULT_SORT_ORDER);
		final AutoCompleteTextView tagInput = (AutoCompleteTextView) this.findViewById(R.id.tagAutocomplete);
		tagInput.setAdapter(new TagAutoCompleteCursorAdapter(this, c));
		tagInput.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
				if (view instanceof TextView) {
					final TextView textView = (TextView) view;
					
					final Tag tag = new Tag(textView.getText().toString().trim());					
					
					/*
					 * only add tag view if tag is new
					 */
					if (ChooseTagsActivity.this.tags.add(tag)) {
						ChooseTagsActivity.this.addTag(tag);
					}
					
					tagInput.setText("");
				}
			}
		});
		
		// back button
		final ImageButton backButton = (ImageButton) this.findViewById(R.id.back);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				ChooseTagsActivity.super.finish(); // don't save new selection!
			}
		});
		
		final ImageButton clearTags = (ImageButton) this.findViewById(R.id.clear_tags);
		clearTags.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				ChooseTagsActivity.this.tags = new TreeSet<Tag>();
				ChooseTagsActivity.this.finish();
			}
		});
		
		// done button
		final ImageButton selectTagsButton = (ImageButton) this.findViewById(R.id.select_tags);
		selectTagsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				ChooseTagsActivity.this.finish();
			}
		});
	}

	/**
	 * @param tag
	 */
	protected void addTag(final Tag tag) {
		final String tagName = tag.getName();
		final ViewGroup tagContainer = (ViewGroup) this.getLayoutInflater().inflate(R.layout.tag_field, this.tagFilterContainer, false);
		final TextView tagTextView = (TextView) tagContainer.findViewById(R.id.tagNameTextView);
		tagTextView.setText(tagName);
		
		final ImageButton removeButton = (ImageButton) tagContainer.findViewById(R.id.removeButton);
		removeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				ChooseTagsActivity.this.tags.remove(tag);
				ChooseTagsActivity.this.tagFilterContainer.removeView(tagContainer);
			}
		});
		
		this.tagFilterContainer.addView(tagContainer);
	}
	
	@Override
	public void finish() {
		/*
		 * save seleted tags in pref
		 */
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		final Editor editor = prefs.edit();
		editor.putString(Settings.FILTERED_TAGS, ModelUtils.convertTags(this.tags));
		editor.commit();
		super.finish();
	}
}
