package org.bibsonomy.android.activity.edit;


import java.util.Collections;

import org.bibsonomy.android.App.Intents;
import org.bibsonomy.android.App.Settings;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.task.ScrapePublicationTask;
import org.bibsonomy.android.utils.AsyncTask.AsyncTaskCallback;
import org.bibsonomy.android.utils.NetworkUtils;
import org.bibsonomy.android.utils.ScraperRepository;
import org.bibsonomy.android.utils.parcel.PostParcel;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.util.GroupUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * 
 * @author dzo
 */
public class AddUrlActivity extends Activity {
	
	private ScrapePublicationTask scrapeTask;
	
	private Button postPublButton;
	private TextView infoText;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		final Intent intent = this.getIntent();
		
		if ((intent != null) && Intent.ACTION_SEND.equals(intent.getAction())) {
			final String url = intent.getStringExtra(Intent.EXTRA_TEXT);
			final String title = intent.getStringExtra(Intent.EXTRA_SUBJECT);
			final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			
			/*
			 * only check for scraping urls if the user has enabled publications
			 */
			if (prefs.getBoolean(Settings.SHOW_PUBLICATIONS, false) && ScraperRepository.getInstance().supportsUrl(this, url)) {
				this.setContentView(R.layout.scraping_view);
				
				this.postPublButton = (Button) this.findViewById(R.id.scrapingPostPublication);
				this.infoText = (TextView) this.findViewById(R.id.scrapingInfoText);
				
				if (!NetworkUtils.isNetworkAvailable(this)) {
					// TODO: network error icon for visual feedback?
					// TODO: save request
					this.scrapingFailed(R.string.scrapingFailureNetwork);
				} else {
					this.scrapeTask = new ScrapePublicationTask(this.getString(R.string.webservice));
					this.scrapeTask.setCallback(new AsyncTaskCallback<Post<BibTex>>() {							
						@Override
						public void gotResult(final Post<BibTex> result) {
							final ProgressBar progressbar = (ProgressBar) AddUrlActivity.this.findViewById(R.id.scrapingProgressBar);
							progressbar.setVisibility(View.GONE);
							
							if (result == null) {
								AddUrlActivity.this.scrapingFailed(R.string.scrapingFailure);
								return;
							}
							
							AddUrlActivity.this.infoText.setText(AddUrlActivity.this.getResources().getString(R.string.scrapingSuccess));						
							AddUrlActivity.this.postPublButton.setClickable(true);
							AddUrlActivity.this.postPublButton.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(final View paramView) {
									AddUrlActivity.this.openPublicationEditor(result);
								}
							});
						}
					});
					
					this.scrapeTask.execute(url);
				}
			} else {
				this.openBookmarkEditor(this.createBookmarkPost(url, title));
			}
		}
		
		// TODO: show error message
		super.onCreate(savedInstanceState);
	}
	
	private void scrapingFailed(final int errorTextId) {
		this.infoText.setText(this.getResources().getString(errorTextId));
		this.postPublButton.setVisibility(View.GONE);
	}

	protected Post<Bookmark> createBookmarkPost(final String url, final String title) {
		final Bookmark bookmark = new Bookmark();
		bookmark.setTitle(title);
		bookmark.setUrl(url);
		final Post<Bookmark> post = new Post<Bookmark>();
		post.setResource(bookmark);
		return post;
	}
	
	protected <RESOURCE extends Resource> void openResourceEditor(final Class<? extends AbstractEditPostActivity<?>> clazz, final Post<RESOURCE> post) {
		final Intent intent = new Intent(this, clazz);
		if (post.getGroups().isEmpty()) {
			post.setGroups(Collections.singleton(GroupUtils.getPublicGroup()));
		}
		intent.putExtra(Intents.EXTRA_POST, new PostParcel<RESOURCE>(post));
		intent.setAction(Intents.ACTION_CREATE_POST);
		this.startActivity(intent);
		this.finish();
	}

	protected void openBookmarkEditor(final Post<Bookmark> post) {
		this.openResourceEditor(EditBookmarkActivity.class, post);		
	}
	
	protected void openPublicationEditor(final Post<BibTex> post) {
		this.openResourceEditor(EditPublicationActivity.class, post);
	}
}
