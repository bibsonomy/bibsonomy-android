package org.bibsonomy.android.utils;


/**
 * 
 * @author dzo
 *
 * @param <Params>
 * @param <Progress>
 * @param <Result>
 */
public abstract class AsyncTask<Params, Progress, Result> extends android.os.AsyncTask<Params, Progress, Result> {
	
	/**
	 * @author dzo
	 *
	 * @param <T>
	 */
	public static interface AsyncTaskCallback<T> {
		
		/**
		 * TODO: improve documentation
		 * @param result
		 */
		public void gotResult(final T result);
	}
	
	private AsyncTaskCallback<Result> callback;
	
	/**
	 * @param callback the callback to set
	 */
	public void setCallback(final AsyncTaskCallback<Result> callback) {
		this.callback = callback;
	}

	@Override
	protected void onPostExecute(final Result result) {
		if (this.callback != null) {
			this.callback.gotResult(result);
		}
		
		super.onPostExecute(result);
	}
}
