package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.User;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 */
public class UserParcel implements Parcelable {
	
	/**
	 * the creator
	 */
	public static final Parcelable.Creator<UserParcel> CREATOR = new Parcelable.Creator<UserParcel>() {
		@Override
		public UserParcel createFromParcel(final Parcel in) {
			return new UserParcel(in);
		}

		@Override
		public UserParcel[] newArray(final int size) {
			return new UserParcel[size];
		}
	};
	
	
	private final User user;
	
	/**
	 * @param user the user to write
	 */
	public UserParcel(final User user) {
		this.user = user;
	}
	
	private UserParcel(final Parcel in) {
		this.user = new User(in.readString());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		if (this.user == null) {
			dest.writeString(null);
			return;
		}
		
		dest.writeString(this.user.getName());
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}
}
