package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.Resource;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 *
 * @param <R>
 */
public abstract class ResourceParcel<R extends Resource> implements Parcelable {

	protected final R resource;
	
	/**
	 * @param resource
	 */
	public ResourceParcel(final R resource) {
		this.resource = resource;
	}
	
	protected ResourceParcel(final R resource, final Parcel in) {
		this.resource = resource;
		
		this.resource.setInterHash(in.readString());
		this.resource.setIntraHash(in.readString());
		this.resource.setTitle(in.readString());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeString(this.resource.getInterHash());
	    dest.writeString(this.resource.getIntraHash());
	    dest.writeString(this.resource.getTitle());
	}

	/**
	 * @return the resource
	 */
	public R getResource() {
		return this.resource;
	}
}
