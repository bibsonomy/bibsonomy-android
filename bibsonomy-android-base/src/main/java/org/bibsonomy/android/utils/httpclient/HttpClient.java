package org.bibsonomy.android.utils.httpclient;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.bibsonomy.android.utils.IOUtils;

/**
 * 
 * @author dzo
 */
public class HttpClient {
	/**
	 * the accept encoding header
	 */
	public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
	
	/**
	 * gzip
	 */
	public static final String GZIP = "gzip";	
	
	private static final int DEFAULT_TIMEOUT = 20 * 1000;

	
	private static final HttpClient INSTANCE = new HttpClient();
	
	/**
	 * only one instance of this client for the whole application
	 * @return the instance of HttpClient
	 */
	public static HttpClient getInstance() {
		return INSTANCE;
	}
	
	private final DefaultHttpClient client;
	
	private HttpClient() {
		final HttpParams params = new BasicHttpParams();
    	HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    	HttpProtocolParams.setContentCharset(params, "UTF-8");

    	HttpConnectionParams.setStaleCheckingEnabled(params, false);
		HttpConnectionParams.setConnectionTimeout(params, DEFAULT_TIMEOUT);
		HttpConnectionParams.setSoTimeout(params, 60 * 1000);
		HttpConnectionParams.setSocketBufferSize(params, 8192);
		ConnManagerParams.setTimeout(params, 2000);
		HttpClientParams.setRedirecting(params, false);

		final SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

		final ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);
		client = new DefaultHttpClient(manager, params);
		
    	/*
    	 * response interceptor for gzip content
    	 */
    	client.addResponseInterceptor(new GZIPResponseInterceptor());
    	
    	client.addRequestInterceptor(new CredentialsRequestInterceptor(), 0);
    	client.addRequestInterceptor(new GZIPRequestInterceptor());
	}
	
	
	/**
     * closes the content stream if neccessary
     * 
     * @param response
     * @throws IOException 
     * @throws IllegalStateException 
     */
	public static void closeContentStream(final HttpResponse response) throws IllegalStateException, IOException {
		if (response == null) {
			return;
		}
		final HttpEntity entity = response.getEntity();
		if (entity == null) {
			return;
		}
		
		if (entity.isStreaming()) { 
			IOUtils.close(entity.getContent());
		}
	}
	
	/**
	 * @param username the username
     * @param password the password
     * @param scope the scope
     */
    public void setCredentials(final String username, final String password, final String scope) {
    	final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
    	client.getCredentialsProvider().setCredentials(new AuthScope(scope, -1), credentials);
    }
    
    /**
     * the content as stream
     * 
     * @param host
     * @param url
     * @return @see getContentAsStream
     * @throws IOException
     */
    public InputStream getContentAsStream(final HttpHost host, final String url) throws IOException {
    	final HttpGet get = new HttpGet(url);
		return getContentAsStream(host, get);
    }

	/**
	 * @param host
	 * @param request
	 * @return the stream of the response, <code>null</code> iff response not successfull
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public InputStream getContentAsStream(final HttpHost host, final HttpRequestBase request) throws IOException, ClientProtocolException {
		final HttpResponse response = host != null ? client.execute(host, request) : client.execute(request);
		
		final int statusCode = response.getStatusLine().getStatusCode();
		if (isSuccessStatusCode(statusCode)) {
			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				return entity.getContent();
			}
		}
		
		return null;
	}

	/**
	 * @param statusCode
	 * @return <code>true</code> iff status ok or created
	 */
	public static boolean isSuccessStatusCode(final int statusCode) {
		return statusCode == HttpStatus.SC_OK || statusCode == HttpStatus.SC_CREATED;
	}

	/**
	 * 
	 * @param host
	 * @param request
	 * @return <code>true</code> iff the request was successful
	 * @throws IOException
	 */
	public boolean executeAndCheckForSuccess(final HttpHost host, final HttpRequestBase request) throws IOException {
		final HttpResponse response = client.execute(host, request);
		closeContentStream(response);
		return isSuccessStatusCode(response.getStatusLine().getStatusCode());
	}
	
	/**
	 * only executes the provided request with the default http client
	 * @param host
	 * @param request
	 * @return the response
	 * @throws IOException
	 */
	public HttpResponse execute(final HttpHost host, final HttpRequestBase request) throws IOException {
		return client.execute(host, request);
	}
}
