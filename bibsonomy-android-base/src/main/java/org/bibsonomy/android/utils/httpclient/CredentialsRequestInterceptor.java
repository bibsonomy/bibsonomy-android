package org.bibsonomy.android.utils.httpclient;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;

/**
 * sets credentials based on authscope
 * 
 * @author dzo
 */
public class CredentialsRequestInterceptor implements HttpRequestInterceptor {
	
	@Override
	public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
		final AuthState authState = (AuthState) context.getAttribute(ClientContext.TARGET_AUTH_STATE);
        final CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(ClientContext.CREDS_PROVIDER);
        final HttpHost targetHost = (HttpHost) context.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
        
        if (authState.getAuthScheme() == null) {
            final AuthScope authScope = new AuthScope(targetHost.getHostName(), targetHost.getPort());
            final Credentials creds = credsProvider.getCredentials(authScope);
            if (creds != null) {
                authState.setAuthScheme(new BasicScheme());
                authState.setCredentials(creds);
            }
        }
	}
}
