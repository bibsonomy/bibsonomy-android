package org.bibsonomy.android.utils.gesture;

import android.content.Context;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

/**
 * Detects a back swipe, aka swiping from left to right.
 * 
 * @author Alex Plischke
 * 
 */
public class BackSwipeListener extends SimpleOnGestureListener {

    private static final int X_THRESHOLD = 80;
    private static final int Y_THRESHOLD = 113;
    private static final int VELOCITY_THRESHOLD = 166;
    
    /**
     * Creates the {@link BackSwipeListener} with the given {@link Context} used
     * for density calculations.
     * @param context
     * @return the configurated {@link BackSwipeListener}
     */
    public static final BackSwipeListener createBackSwipeListener(final Context context) {
    	final float density = context.getResources().getDisplayMetrics().density;
    	
    	return new BackSwipeListener(X_THRESHOLD * density, Y_THRESHOLD * density);
    }
    
    private final float thresholdX;
    private final float thresholdY;
    
    private BackSwipeListener(final float thresholdX, final float thresholdY) {
    	this.thresholdX = thresholdX;
    	this.thresholdY = thresholdY;
    }

    @Override
    public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX,
	    final float velocityY) {
	if (e2.getX() - e1.getX() > this.thresholdX
		&& velocityX > VELOCITY_THRESHOLD
		&& Math.abs(e2.getY() - e1.getY()) < this.thresholdY) {
	    return true;
	}

	return super.onFling(e1, e2, velocityX, velocityY);
    }

}
