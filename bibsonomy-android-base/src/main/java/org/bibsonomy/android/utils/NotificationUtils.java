package org.bibsonomy.android.utils;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.base.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * 
 * @author Alex Plischke
 */
public class NotificationUtils {
	
	/**
	 * @param context
	 * @param title
	 * @param contentText
	 * @param notificationId
	 */
	public static void sendNotification(final Context context, final CharSequence title, final CharSequence contentText, final int notificationId) {
		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		final Notification notification = new Notification(R.drawable.icon, contentText, System.currentTimeMillis());

		final Intent notificationIntent = new Intent(context, PostShelfActivity.class);
		final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, title, contentText, contentIntent);

		notificationManager.notify(notificationId, notification);
	}
	
	/**
	 * 
	 * @param context
	 * @param id
	 */
	public static void cancelNotification(final Context context, final int id) {
		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(id);
	}
}
