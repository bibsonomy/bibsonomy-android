package org.bibsonomy.android.utils;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.bibsonomy.android.App;
import org.bibsonomy.common.Pair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.util.Log;

/**
 * 
 * @author dzo
 */
public final class ScraperRepository {
	private static final ScraperRepository INSTANCE = new ScraperRepository();

	/**
	 * @return the @{link:ScraperRepository} instance
	 */
	public static ScraperRepository getInstance() {
		return INSTANCE;
	}

	private ScraperRepository() {}
	
	private static final Pattern EMPTY_PATTERN = null;
	
	private List<Pair<Pattern, Pattern>> sites = null;
	
	
	private final void loadPatterns(final Context context) {
		this.sites = new LinkedList<Pair<Pattern, Pattern>>();
		final InputStream inputStream;
		try {
			inputStream = context.openFileInput(App.SCRAPING_FILE_NAME);
			
			final JSONTokener json = new JSONTokener(IOUtils.toString(inputStream));
			
			final JSONObject patterns = (JSONObject) json.nextValue();
			
			final JSONArray jsonArray = patterns.getJSONArray("patterns");
			
			for (int i = 0; i < jsonArray.length(); i++) {
				final JSONObject jsonObject = jsonArray.getJSONObject(i);
				
				final String host = jsonObject.getString("host");
				final String path = jsonObject.getString("path");
				
				final Pattern hostPattern = Pattern.compile(host);
				final Pattern pathPattern;
				
				if ("null".equals(path)) {
					pathPattern = EMPTY_PATTERN;
				} else {
					pathPattern = Pattern.compile(path);
				}
				
				this.sites.add(new Pair<Pattern, Pattern>(hostPattern, pathPattern));
			}
		
		} catch (final Exception e) {
			Log.e("ScrapingUtils", "reading scaping list failed", e);
		}
	}
	
	/**
	 * mark the host patterns that they must be reloaded
	 */
	public void markForReload() {
		this.sites = null;
	}
	
	/**
	 * 
	 * @param context
	 * @param urlString
	 * @return	<code>true</code> iff the url is supported by bibsonomy's
	 * 			scraping service
	 */
	public boolean supportsUrl(final Context context, final String urlString) {
		if (!present(urlString)) {
			return false;
		}
		try {
			final URL url = new URL(urlString);
			if (this.sites == null) {
				loadPatterns(context);
			}
			
			for (final Pair<Pattern, Pattern> site : this.sites) {
				final Pattern hostPattern = site.getFirst();
				final Pattern pathPattern = site.getSecond();
				
				final boolean match1 = hostPattern == EMPTY_PATTERN || hostPattern.matcher(url.getHost()).find();
				final boolean match2 = pathPattern == EMPTY_PATTERN || pathPattern.matcher(url.getPath()).find();
				if (match1 && match2) {
					return true;
				}
			}
		} catch (final Exception e) {
			// ignore
		}
		
		return false;
	}
}
