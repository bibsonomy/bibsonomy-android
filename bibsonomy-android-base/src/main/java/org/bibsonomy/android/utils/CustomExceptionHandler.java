package org.bibsonomy.android.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.bibsonomy.android.utils.httpclient.HttpClient;

import android.os.Build;
import android.os.Build.VERSION;

/**
 * 
 * @author Alex Plischke
 * 
 */
public class CustomExceptionHandler implements UncaughtExceptionHandler {
	private static final String LINE_BREAK = "\n";
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSS");


	private final UncaughtExceptionHandler defaultUEH;

	private final String url;

	/**
	 * if any of the parameters is null, the respective functionality will not
	 * be used
	 * @param url 
	 */
	public CustomExceptionHandler(final String url) {
		this.url = url;
		this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
	}

	@Override
	public void uncaughtException(final Thread thread, final Throwable throwable) {
		if (this.url == null) {
			this.rethrowException(thread, throwable);
			return;
		}
		
		final Date now = new Date();
		final String timestamp = DATE_FORMAT.format(now);
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		printWriter.append(timestamp);
		printWriter.append(LINE_BREAK);
		printWriter.append("api level: ");
		printWriter.append(VERSION.SDK);
		printWriter.append(" model: ");
		printWriter.append(Build.MODEL);
		printWriter.append(" product: ");
		printWriter.append(Build.PRODUCT);
		printWriter.append(LINE_BREAK);
		
		throwable.printStackTrace(printWriter);
		sendToServer(result.toString());
		printWriter.close();
		this.rethrowException(thread, throwable);
	}

	/**
	 * @param t
	 * @param e
	 */
	protected void rethrowException(final Thread t, final Throwable e) {
		this.defaultUEH.uncaughtException(t, e);
	}

	private void sendToServer(final String stacktrace) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(this.url);
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(Collections.singletonList(new BasicNameValuePair("message", stacktrace)), HTTP.UTF_8));
			final HttpResponse response = httpClient.execute(httpPost);
			HttpClient.closeContentStream(response);
		} catch (final IOException e) {
			// ignore exception
		}
	}

}
