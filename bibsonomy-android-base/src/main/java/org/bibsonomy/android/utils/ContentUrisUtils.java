package org.bibsonomy.android.utils;

import android.net.Uri;
import android.net.Uri.Builder;

/**
 * 
 * @author dzo
 *
 */
public final class ContentUrisUtils {
	private ContentUrisUtils() {}
	
	/**
	 * appends an unescaped string to the builder
	 * @param builder
	 * @param id
	 * @return the builder
	 */
	public static Builder appendId(final Builder builder, final String id) {
		return builder.appendPath(id);
	}
	
	/**
	 * appends an unescaped string to the uri
	 * @param contentUri the content uri
	 * @param id
	 * @return the contentUri + id
	 */
	public static Uri withAppendedId (final Uri contentUri, final String id) {
		final Builder builder = contentUri.buildUpon();
		return appendId(builder, id).build();
	}
	
	/**
	 * @param builder the uri builder
	 * @param paramName the param name
	 * @param paramValue the param value
	 * @return the builder
	 */
	public static Builder appendParam(final Builder builder, final String paramName, final String paramValue) {
		return builder.appendQueryParameter(paramName, paramValue);
	}
	
	/**
	 * creates a new uri with param name and value
	 * @param contentUri
	 * @param paramName
	 * @param paramValue
	 * @return a new uri
	 */
	public static Uri withAppendedParam(final Uri contentUri, final String paramName, final String paramValue) {
		final Builder builder = contentUri.buildUpon();
		return appendParam(builder, paramName, paramValue).build();
	}
}
