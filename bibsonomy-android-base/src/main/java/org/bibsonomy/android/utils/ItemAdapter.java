package org.bibsonomy.android.utils;

import java.util.List;

import org.bibsonomy.android.utils.ItemAdapter.Item;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * 
 * @author dzo
 */
public class ItemAdapter extends ArrayAdapter<Item> {
	
	public ItemAdapter(final Context context, final int textViewResourceId, final List<Item> objects) {
		super(context, textViewResourceId, objects);
	}

	public static class Item {
		private final String text;
		private final int icon;
		
		public Item(final String text, final int icon) {
			this.text = text;
			this.icon = icon;
		}
		
		@Override
		public String toString() {
			return this.text;
		}
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		// user super class to create the view
        final View v = super.getView(position, convertView, parent);
        final TextView tv = (TextView) v.findViewById(android.R.id.text1);

        // put the image on the textView
        tv.setCompoundDrawablesWithIntrinsicBounds(this.getItem(position).icon, 0, 0, 0);

        // add margin between image and text (support various screen densities)
        final int dp5 = (int) (5 * this.getContext().getResources().getDisplayMetrics().density + 0.5f);
        tv.setCompoundDrawablePadding(dp5);

        return v;
	}
}
