package org.bibsonomy.android.utils.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.protocol.HttpContext;

/**
 * {@link HttpResponseInterceptor} for gzip encoded content
 * 
 * @author dzo
 */
public class GZIPResponseInterceptor implements HttpResponseInterceptor {

	@Override
	public void process(final HttpResponse response, final HttpContext context) throws HttpException, IOException {
		/*
		 * inflate any responses compressed with gzip
		 */
	    final HttpEntity entity = response.getEntity();
	    final Header encoding = entity.getContentEncoding();
	    if (encoding != null) {
	      for (final HeaderElement element : encoding.getElements()) {
	        if (element.getName().equalsIgnoreCase(HttpClient.GZIP)) {
	          response.setEntity(new InflatingEntity(response.getEntity()));
	          break;
	        }
	      }
	   }
	}
	
	
	/**
	 * XXX: GzipDecompressingEntity is missing in the Android API
	 * 
	 * @author dzo
	 */
	private static class InflatingEntity extends HttpEntityWrapper {
		
		protected InflatingEntity(final HttpEntity wrapped) {
			super(wrapped);
		}

		@Override
		public InputStream getContent() throws IOException {
			return new GZIPInputStream(this.wrappedEntity.getContent());
		}

		@Override
		public long getContentLength() {
			return -1;
		}
	}
}