package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.Tag;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 *
 */
public class TagParcel implements Parcelable {
	
	/**
	 * the creator for this parcel
	 */
	public static final Creator<TagParcel> CREATOR = new Creator<TagParcel>() {

		@Override
		public TagParcel createFromParcel(final Parcel source) {
			return new TagParcel(source);
		}

		@Override
		public TagParcel[] newArray(final int size) {
			return new TagParcel[size];
		}
		
	};
	
	
	private final Tag tag;
	
	private TagParcel(final Parcel in) {
		this.tag = new Tag(in.readString());
	}
	
	/**
	 * @param tag
	 */
	public TagParcel(final Tag tag) {
		this.tag = tag;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeString(this.tag.getName());
	}

	/**
	 * @return the tag
	 */
	public Tag getTag() {
		return this.tag;
	}
}
