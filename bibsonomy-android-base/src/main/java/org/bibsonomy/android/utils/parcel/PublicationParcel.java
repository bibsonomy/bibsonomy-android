package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.PersonNameUtils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 *
 */
public class PublicationParcel extends ResourceParcel<BibTex> {
	
	/**
	 * the creator
	 */
	public static final Parcelable.Creator<PublicationParcel> CREATOR = new Parcelable.Creator<PublicationParcel>() {
		@Override
		public PublicationParcel createFromParcel(final Parcel in) {
			return new PublicationParcel(in);
		}

		@Override
		public PublicationParcel[] newArray(final int size) {
			return new PublicationParcel[size];
		}
	};
	
	/**
	 * @param publication
	 */
	public PublicationParcel(final BibTex publication) {
		super(publication);
	}
	
	private PublicationParcel(final Parcel in) {
		super(new BibTex(), in);
		
		this.resource.setAuthor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(in.readString()));
		this.resource.setEditor(PersonNameUtils.discoverPersonNamesIgnoreExceptions(in.readString()));
		
		this.resource.setBibtexKey(in.readString());
		this.resource.setKey(in.readString());
		this.resource.setMisc(in.readString());
		this.resource.setAbstract(in.readString());
		this.resource.setEntrytype(in.readString());
		this.resource.setAddress(in.readString());
		this.resource.setAnnote(in.readString());
		this.resource.setBooktitle(in.readString());
		this.resource.setChapter(in.readString());
		this.resource.setCrossref(in.readString());
		
		this.resource.setHowpublished(in.readString());
		this.resource.setInstitution(in.readString());
		this.resource.setOrganization(in.readString());
		this.resource.setJournal(in.readString());
		this.resource.setNote(in.readString());
		this.resource.setNumber(in.readString());
		this.resource.setPages(in.readString());
		this.resource.setPublisher(in.readString());
		this.resource.setSchool(in.readString());
		this.resource.setSeries(in.readString());
		this.resource.setVolume(in.readString());
		this.resource.setDay(in.readString());
		this.resource.setMonth(in.readString());
		this.resource.setYear(in.readString());
		
		this.resource.setUrl(in.readString());
		this.resource.setPrivnote(in.readString());
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		super.writeToParcel(dest, flags);
		
		// TODO: test performance
		dest.writeString(PersonNameUtils.serializePersonNames(this.resource.getAuthor()));
		dest.writeString(PersonNameUtils.serializePersonNames(this.resource.getEditor()));
	    
	    dest.writeString(this.resource.getBibtexKey());
	    dest.writeString(this.resource.getKey());
	    
	    // TODO: 
	    dest.writeString(this.resource.getMisc());
	    dest.writeString(this.resource.getAbstract());
	    dest.writeString(this.resource.getEntrytype());
	    dest.writeString(this.resource.getAddress());
	    dest.writeString(this.resource.getAnnote());
	    dest.writeString(this.resource.getBooktitle());
	    dest.writeString(this.resource.getChapter());
	    dest.writeString(this.resource.getCrossref());
	    
	    dest.writeString(this.resource.getHowpublished());
	    dest.writeString(this.resource.getInstitution());
	    dest.writeString(this.resource.getOrganization());
	    dest.writeString(this.resource.getJournal());
	    dest.writeString(this.resource.getNote());
	    dest.writeString(this.resource.getNumber());
	    dest.writeString(this.resource.getPages());
	    dest.writeString(this.resource.getPublisher());
	    dest.writeString(this.resource.getSchool());
	    dest.writeString(this.resource.getSeries());
	    dest.writeString(this.resource.getVolume());
	    dest.writeString(this.resource.getDay());
	    dest.writeString(this.resource.getMonth());
	    dest.writeString(this.resource.getYear());
	    dest.writeString(this.resource.getType());
	    dest.writeString(this.resource.getUrl());
	    dest.writeString(this.resource.getPrivnote());
	    
	    // TODO: missing documents
	}

}
