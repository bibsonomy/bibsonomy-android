package org.bibsonomy.android.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;


/**
 * TODO: move to bibsonomy-common module?
 * 
 * @author dzo
 */
public final class IOUtils {
	private IOUtils() {}
	
	/**
	 * @param stream
	 * @param output
	 * @throws IOException
	 */
	public static void writeAndClose(final InputStream stream, final Writer output) throws IOException {
		final BufferedInputStream bufferedStream = new BufferedInputStream(stream);
		final InputStreamReader input = new InputStreamReader(bufferedStream);
		final char[] buffer = new char[4096];
        int length = 0;
        while ((length = input.read(buffer)) != -1) {
            output.write(buffer, 0, length);
        }
        output.flush();
        output.close();
        input.close();
	}
	
	/**
	 * 
	 * @param stream
	 * @return the string of the stream
	 * @throws IOException
	 */
	public static String toString(final InputStream stream) throws IOException {
		final StringWriter writer = new StringWriter();
		
		/*
		 * write it to string writer
		 */
		writeAndClose(stream, writer);
		
		return writer.toString();
	}

	/**
	 * closes a closeable
	 * 
	 * @param in the closeable to close
	 */
	public static void close(final Closeable in) {
		if (in != null) {
			try {
				in.close();
			} catch (final IOException io) {
				// TODO: log
			}
		}
	}

	// TODO: move to IOUtils
	public static InputStream copyStream(final InputStream in) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
	    final byte[] buffer = new byte[1024];
	    int len;
	    while ((len = in.read(buffer)) > 0 ) {
	        baos.write(buffer, 0, len);
	    }
	    baos.flush();
	    
	    return new ByteArrayInputStream(baos.toByteArray()); 
	}
}
