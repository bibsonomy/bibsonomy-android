package org.bibsonomy.android.utils.parcel;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.exceptions.UnsupportedResourceTypeException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * note: post is seralizable but {@link Parcel#writeSerializable(java.io.Serializable)}
 * overhead is to large
 * 
 * @author dzo
 * @param <R> 
 */
public class PostParcel<R extends Resource> implements Parcelable {

	/**
	 * the creator for this parcel
	 */
	public static final Parcelable.Creator<PostParcel<?>> CREATOR = new Parcelable.Creator<PostParcel<?>>() {
		@Override
		public PostParcel<?> createFromParcel(final Parcel in) {
			return new PostParcel<Resource>(in);
		}

		@Override
		public PostParcel<?>[] newArray(final int size) {
			return new PostParcel[size];
		}
	};
	
	private static List<Parcelable> convertTags(final Set<Tag> tags) {
		final List<Parcelable> pTags = new LinkedList<Parcelable>();
		
		for (final Tag tag : tags) {
			pTags.add(new TagParcel(tag));
		}
		
		return pTags;
	}
	
	private static Set<Tag> convertTags(final List<TagParcel> pTags) {
		final Set<Tag> tags = new HashSet<Tag>();
		
		for (final TagParcel pTag : pTags) {
			tags.add(pTag.getTag());
		}
		
		return tags;
	}

	private static Parcelable convertResource(final Resource resource) {
		if (resource instanceof Bookmark) {
			return new BookmarkParcel((Bookmark) resource);
		}
		
		if (resource instanceof BibTex) {
			return new PublicationParcel((BibTex) resource);
		}
		throw new UnsupportedResourceTypeException();
	}
	
	private static void convertGroups(final Set<Group> groups, final List<GroupParcel> pGroups) {
		for (final GroupParcel parcel : pGroups) {
			if (present(parcel)) {
				groups.add(parcel.getGroup());
			}
		}
	}

	private static List<Parcelable> convertGroups(final Set<Group> groups) {
		final List<Parcelable> pGroups = new LinkedList<Parcelable>();
		
		for (final Group group : groups) {
			pGroups.add(new GroupParcel(group));
		}
		
		return pGroups;
	}
	

	private final Post<R> post;
	
	private PostParcel(final Parcel in) {
		post = new Post<R>();
		
		final UserParcel userParcel = in.readParcelable(this.getClass().getClassLoader());
		post.setUser(userParcel.getUser());
		
		final List<GroupParcel> groups = new LinkedList<GroupParcel>();
		in.readTypedList(groups, GroupParcel.CREATOR);		
		convertGroups(this.post.getGroups(), groups);
		
		this.post.setDescription(in.readString());
		
		this.post.setDate(new Date(in.readLong()));
		this.post.setChangeDate(new Date(in.readLong()));
		
		final LinkedList<TagParcel> tags = new LinkedList<TagParcel>();
		in.readTypedList(tags, TagParcel.CREATOR);
		this.post.setTags(convertTags(tags));
		
		final ResourceParcel<R> resourceParcelable = in.readParcelable(this.getClass().getClassLoader());
		this.post.setResource(resourceParcelable.getResource());
	}

	/**
	 * @param post
	 */
	public PostParcel(final Post<R> post) {
		this.post = post;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeParcelable(new UserParcel(this.post.getUser()), flags);
		dest.writeTypedList(convertGroups(this.post.getGroups()));
	    
	    dest.writeString(this.post.getDescription());
	    
	    final Date date = this.post.getDate();
		if (present(date)) {
			dest.writeLong(date.getTime());
	    } else {
	    	dest.writeLong(0);
	    }
	    
	    final Date changeDate = this.post.getChangeDate();
	    if (present(changeDate)) {
	    	dest.writeLong(changeDate.getTime());
	    } else {
	    	dest.writeLong(0);
	    }
	    dest.writeTypedList(convertTags(this.post.getTags()));
	    dest.writeParcelable(convertResource(this.post.getResource()), flags);
	}

	/**
	 * @return the post
	 */
	public Post<R> getPost() {
		return this.post;
	}
}
