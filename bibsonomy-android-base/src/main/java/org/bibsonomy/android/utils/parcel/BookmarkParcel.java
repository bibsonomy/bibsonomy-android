package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.Bookmark;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 *
 */
public class BookmarkParcel extends ResourceParcel<Bookmark> {
	
	/**
	 * the creator
	 */
	public static final Parcelable.Creator<BookmarkParcel> CREATOR = new Parcelable.Creator<BookmarkParcel>() {
		@Override
		public BookmarkParcel createFromParcel(final Parcel in) {
			return new BookmarkParcel(in);
		}

		@Override
		public BookmarkParcel[] newArray(final int size) {
			return new BookmarkParcel[size];
		}
	};
	
	
	/**
	 * @param bookmark
	 */
	public BookmarkParcel(final Bookmark bookmark) {
		super(bookmark);
	}
	
	private BookmarkParcel(final Parcel in) {
		super(new Bookmark(), in);
		
		this.resource.setUrl(in.readString());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		super.writeToParcel(dest, flags);
	    dest.writeString(this.resource.getUrl());
	}

}
