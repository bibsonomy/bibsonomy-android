package org.bibsonomy.android.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.http.HttpEntity;
import org.bibsonomy.model.Document;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

/**
 * Allows downloading Images (e.g. as a Book cover) or fetching them from the
 * external storage.
 * 
 * @author Alex Plischke
 */
public class FileManager {
	private static final String LOG_TAG = "FileManager";
	private static final ConcurrentMap<String, SoftReference<Drawable>> COVER_CACHE = new ConcurrentHashMap<String, SoftReference<Drawable>>();

	private static Drawable getCoverFromCache(final String intrahash) {
		final SoftReference<Drawable> softCover = COVER_CACHE.get(intrahash);

		if (softCover == null) {
			return null;
		}

		final Drawable drawable = softCover.get();
		if (drawable != null) {
			return drawable;
		}
		/*
		 * remove soft reference
		 */
		COVER_CACHE.remove(intrahash);
		return null;
	}

	private static void addToCoverCache(final String intrahash, final Drawable image) {
		COVER_CACHE.put(intrahash, new SoftReference<Drawable>(image));
	}

	/**
	 * @param res
	 * @param intrahash
	 * @return the cover for the provided intrahash
	 */
	public static Drawable getCover(final Resources res, final String intrahash) {
		final Drawable cachedCover = getCoverFromCache(intrahash);

		if (cachedCover != null) {
			return cachedCover;
		}

		if (!storageAvailable()) {
			return null;
		}

		final File cover = getCover(intrahash);
		if (cover.exists()) {
			InputStream in = null;
			try {
				in = new FileInputStream(cover);
				final BitmapDrawable drawable = new BitmapDrawable(res, BitmapFactory.decodeStream(in));
				addToCoverCache(intrahash, drawable);
				return drawable;
			} catch (final FileNotFoundException e) {
				// we have checked this earlier
			} finally {
				IOUtils.close(in);
			}
		}
		return null;
	}

	protected static File getCover(final String intrahash) {
		// TODO: use resource folder
		return new File("cover");
	}

	private static boolean storageAvailable() {
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}
	
	/**
	 * 
	 * @param context 
	 * @param intrahash
	 * @param bitmapDrawable
	 */
	public static void saveCover(final Context context, final String intrahash, final BitmapDrawable bitmapDrawable) {
		if (!storageAvailable()) {
			return;
		}	

		FileOutputStream fileOut = null;
		try {
			final File dir = getDirectory(context, intrahash);
			final File imageFile = new File(dir, "cover");
			imageFile.createNewFile();
			fileOut = new FileOutputStream(imageFile);
			
			bitmapDrawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, fileOut);
			addToCoverCache(intrahash, bitmapDrawable);
		} catch (final FileNotFoundException e) {
			// we checked that
		} catch (final IOException e) {
			Log.w(LOG_TAG, "creating cover for " + intrahash + " failed", e);
		} finally {
			IOUtils.close(fileOut);
		}
	}

	/**
	 * moves all data from one folder to the other
	 * @param oldIntrahash
	 * @param newIntrahash
	 * @param context 
	 */
	public static void moveAll(final Context context, final String oldIntrahash, final String newIntrahash) {
		if (storageAvailable()) {
			final File folder = getDirectory(context, oldIntrahash);
			if (folder.exists()) {
				final File newFolder = getDirectory(context, newIntrahash, false);
				if (newFolder.exists()) {
					// TODO: what do we want to do in that case
					// deleteAll(newIntrahash);
					throw new IllegalStateException("intrahash has already docs");
				}
				
				newFolder.mkdir();
				for (final File file : folder.listFiles()) {
					file.renameTo(new File(newFolder, file.getName()));
				}
				/*
				 * folder must be empty now, so we can delete it
				 */
				folder.delete();
			}
		}
	}

	/**
	 * deletes all data for a post (specified by intrahash)
	 * @param context 
	 * @param intrahash
	 */
	public static void deleteAll(final Context context, final String intrahash) {
		if (storageAvailable()) {
			final File folder = getDirectory(context, intrahash);
			if (folder.exists()) {
				for (final File file : folder.listFiles()) {
					file.delete();
				}
				folder.delete();
			}
		}
	}

	/**
	 * saves the document to the file system
	 * @param context 
	 * @param intraHash
	 * @param document
	 * @param input
	 */
	public static void saveDocument(final Context context, final String intraHash, final Document document, final HttpEntity input) {
		if (storageAvailable()) {
			final String fileName = document.getFileName();
			try {
				final File file = new File(getPathToDocument(context, intraHash, document));
				file.delete();
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
				input.writeTo(bufferedOutputStream);
			} catch (final IOException e) {
				Log.e(LOG_TAG, "error writing file " + fileName + " for post " + intraHash, e);
			}
		}
	}
	
	private static File getDirectory(final Context context, final String intraHash) {
		return getDirectory(context, intraHash, true);
	}

	private static File getDirectory(final Context context, final String intraHash, final boolean create) {
		final File dir = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), context.getApplicationInfo().name + "/" + intraHash);
		if (create && !dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	public static String getPathToDocument(final Context context, final String intraHash, final Document document) {
		return new File(getDirectory(context, intraHash), document.getFileName()).toURI().toString();
	}
}
