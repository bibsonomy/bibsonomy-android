package org.bibsonomy.android.utils.parcel;

import org.bibsonomy.model.Group;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author dzo
 */
public class GroupParcel implements Parcelable {

	/**
	 * creator for this Parcel
	 */
	public static final Creator<GroupParcel> CREATOR = new Creator<GroupParcel>() {
		@Override
		public GroupParcel createFromParcel(final Parcel source) {
			return new GroupParcel(source);
		}
		
		@Override
		public GroupParcel[] newArray(final int size) {
			return new GroupParcel[size];
		}
	};
	
	private final Group group;
	
	private GroupParcel(final Parcel in) {
		this.group = new Group(in.readString());
	}
	
	/**
	 * @param group the group to write
	 */
	public GroupParcel(final Group group) {
		this.group = group;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeString(this.group.getName());
	}

	/**
	 * @return the group
	 */
	public Group getGroup() {
		return group;
	}
}
