package org.bibsonomy.android.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import org.xmlpull.v1.XmlSerializer;

/**
 * 
 * @author dzo
 *
 */
public class XMLSerializer implements XmlSerializer {

	private final XmlSerializer xmlSerializer;
	
	/**
	 * @param xmlSerializer
	 */
	public XMLSerializer(final XmlSerializer xmlSerializer) {
		this.xmlSerializer = xmlSerializer;
	}
	
	@Override
	public void setFeature(final String name, final boolean state) throws IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.setFeature(name, state);
	}

	@Override
	public boolean getFeature(final String name) {
		return this.xmlSerializer.getFeature(name);
	}

	@Override
	public void setProperty(final String name, final Object value) throws IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.setProperty(name, value);
	}

	@Override
	public Object getProperty(final String name) {
		return this.xmlSerializer.getProperty(name);
	}

	@Override
	public void setOutput(final OutputStream os, final String encoding) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.setOutput(os, encoding);
	}

	@Override
	public void setOutput(final Writer writer) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.setOutput(writer);
	}

	@Override
	public void startDocument(final String encoding, final Boolean standalone) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.startDocument(encoding, standalone);
	}

	@Override
	public void endDocument() throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.endDocument();
	}

	@Override
	public void setPrefix(final String prefix, final String namespace) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.setPrefix(prefix, namespace);
	}

	@Override
	public String getPrefix(final String namespace, final boolean generatePrefix) throws IllegalArgumentException {
		return this.xmlSerializer.getPrefix(namespace, generatePrefix);
	}

	@Override
	public int getDepth() {
		return this.xmlSerializer.getDepth();
	}

	@Override
	public String getNamespace() {
		return this.xmlSerializer.getNamespace();
	}

	@Override
	public String getName() {
		return this.xmlSerializer.getName();
	}

	@Override
	public XmlSerializer startTag(final String namespace, final String name) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.startTag(namespace, name);
		return this;
	}

	@Override
	public XmlSerializer attribute(final String namespace, final String name, final String value) throws IOException, IllegalArgumentException, IllegalStateException {
		if (value != null) {
			this.xmlSerializer.attribute(namespace, name, value);
		}
		return this;
	}

	@Override
	public XmlSerializer endTag(final String namespace, final String name) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.endTag(namespace, name);
		return this;
	}

	@Override
	public XmlSerializer text(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.text(text);
		return this;
	}

	@Override
	public XmlSerializer text(final char[] buf, final int start, final int len) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.text(buf, start, len);
		return this;
	}

	@Override
	public void cdsect(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.cdsect(text);
	}

	@Override
	public void entityRef(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.entityRef(text);
	}

	@Override
	public void processingInstruction(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.processingInstruction(text);
	}

	@Override
	public void comment(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.comment(text);
	}

	@Override
	public void docdecl(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.docdecl(text);
	}

	@Override
	public void ignorableWhitespace(final String text) throws IOException, IllegalArgumentException, IllegalStateException {
		this.xmlSerializer.ignorableWhitespace(text);
	}

	@Override
	public void flush() throws IOException {
		this.xmlSerializer.flush();
	}

}
