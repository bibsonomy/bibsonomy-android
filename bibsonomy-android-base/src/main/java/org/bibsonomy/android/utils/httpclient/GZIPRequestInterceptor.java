package org.bibsonomy.android.utils.httpclient;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;

/**
 * sets the GZIP encoding header
 * 
 * @author dzo
 */
public class GZIPRequestInterceptor implements HttpRequestInterceptor {
	private static final Header ACCEPT_GZIP = new BasicHeader(HttpClient.HEADER_ACCEPT_ENCODING, HttpClient.GZIP);
	
	@Override
	public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
		// add header to accept gzip content
	    if (!request.containsHeader(HttpClient.HEADER_ACCEPT_ENCODING)) {
	      request.addHeader(ACCEPT_GZIP);
	    }
	}

}
