package org.bibsonomy.android.utils;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Tag;
import org.bibsonomy.util.UrlUtils;

import android.text.TextUtils;

/**
 * 
 * @author dzo
 */
public class ModelUtils {
	
	/**
	 * space string
	 */
	public static final String SPACE_DELIMITER = " ";
	
	private static final Pattern MISC_FIELD_NAME_PATTERN = Pattern.compile("[^{\\s}=]*");
	private static final Pattern STRING_PATTERN = Pattern.compile("\\s");
	private static final Pattern TAG_VALID_PATTERN = Pattern.compile("^\\S+$");

	/**
	 * convert a list of groups
	 * @param groups
	 * @return the groups seperated by space
	 */
	public static String convertGroups(final List<Group> groups) {
		final StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (final Group group : groups) {
			if (first) {
				first = false;
			} else {
				builder.append(SPACE_DELIMITER);
			}
			builder.append(group.getName());
		}
		return builder.toString();
	}
	
	/**
	 * @param tags the tags to convert
	 * @return the string representation
	 */
	public static String convertTags(final Set<Tag> tags) {
		final StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (final Tag tag : tags) {
			if (first) {
				first = false;
			} else {
				builder.append(SPACE_DELIMITER);
			}
			builder.append(tag.getName());
		}
		return builder.toString();
	}

	/**
	 * convert a string to a set
	 * @param string
	 * @return the tag set representation
	 */
	public static Set<Tag> convertToTags(String string) {
		final Set<Tag> tags = new HashSet<Tag>();
		if (!present(string)) {
			return tags;
		}
		string = string.trim();
		
		final String[] tagNames = TextUtils.split(string, STRING_PATTERN);
		for (final String tagName : tagNames) {
			tags.add(new Tag(tagName));
		}
		return tags;
	}
	
	/**
	 * Validates the misc field name (used in a {@link BibTex})
	 * 
	 * @param name		field name
	 * @return <code>true</code> iff valid
	 */
	public static boolean isValidMiscFieldName(final String name) {
		if (!present(name)) {
			return false;
		}
		final Matcher matcher = MISC_FIELD_NAME_PATTERN.matcher(name);
		return matcher.matches();
	}
	
	/**
	 * @param tagName
	 * @return <code>true</code> iff tag name is valid
	 */
	public static boolean isValidTagName(final String tagName) {
		if (!present(tagName)) {
			return false;
		}
		
		return TAG_VALID_PATTERN.matcher(tagName).find();
	}
	
	/**
	 * Check whether given url is valid.
	 * 
	 * @param url
	 *            The url to test
	 * @return <true> iff the given url is valid
	 */
	public static boolean isValidURL(final String url) {
		/*
		 * clean url
		 */
		return UrlUtils.cleanUrl(url) != null;
	}
}
