package org.bibsonomy.android.adapter.holder;

import android.widget.ImageView;

/**
 * 
 * @author dzo
 */
public class ResourceHolder {
	public String intraHash;
	public ImageView image;
	public boolean queryCover;
}
