package org.bibsonomy.android.adapter;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.adapter.holder.BookmarkHolder;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.model.Bookmark;

import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A {@link CursorAdapter} used to bind actual data to the {@link View}s.
 * 
 * @author Alex Plischke
 * 
 */
public class BookmarkAdapter extends ResourceAdapter<Bookmark> {
    private final int titleIndex;    
    
    /**
     * @param activity
     * @param c
     * @param defaultDrawable 
     */
    public BookmarkAdapter(final PostShelfActivity activity, final Cursor c, final Drawable defaultDrawable) {
    	super(activity, c, true, defaultDrawable);
    	
    	this.titleIndex = c.getColumnIndexOrThrow(ResourceColumns.TITLE);
    }

    @Override
    public View newView(final Context context, final Cursor cursor, final ViewGroup parent) {
    	final ViewGroup layout = (ViewGroup) this.inflater.inflate(R.layout.bookmark_shelf_item, parent, false);
    	final BookmarkHolder holder = new BookmarkHolder();
    	holder.title = (TextView) layout.findViewById(R.id.title);
    	holder.image = (ImageView) layout.findViewById(R.id.image);
    	layout.setTag(holder);
    	return layout;
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
    	final BookmarkHolder holder = (BookmarkHolder) view.getTag();

    	final CharArrayBuffer buffer = holder.buffer;
    	cursor.copyStringToBuffer(this.titleIndex, buffer);
    	final int size = buffer.sizeCopied;
		if (size != 0) {
		    holder.title.setText(buffer.data, 0, size);
		}
	
		final String intraHash = cursor.getString(this.intraHashIndex);
		this.loadCover(intraHash, holder);
    }
}
