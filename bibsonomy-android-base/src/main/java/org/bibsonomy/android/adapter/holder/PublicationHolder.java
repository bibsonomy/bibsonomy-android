package org.bibsonomy.android.adapter.holder;

import android.database.CharArrayBuffer;
import android.widget.TextView;

/**
 * Used as a content holder.
 * 
 * @author Alex Plischke
 * 
 */
public class PublicationHolder extends ResourceHolder {
	public TextView title;
	public TextView authors;
	public final CharArrayBuffer titleBuffer = new CharArrayBuffer(128);
	public final CharArrayBuffer authorBuffer = new CharArrayBuffer(64);
}
