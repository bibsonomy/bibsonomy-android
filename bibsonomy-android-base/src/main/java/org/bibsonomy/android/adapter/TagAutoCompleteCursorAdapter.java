package org.bibsonomy.android.adapter;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.android.providers.ItemProvider;
import org.bibsonomy.android.providers.database.Database;
import org.bibsonomy.android.providers.database.Database.TagColumns;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * {@link CursorAdapter} for tag autocompletion
 * 
 * @author dzo
 */
public class TagAutoCompleteCursorAdapter extends CursorAdapter {
	private static final String TAG_SEARCH = TagColumns.TAG_NAME_LOWER + " GLOB ?";

	
	private final int nameIdx;
	private final ContentResolver resolver;
	private final Context context;
	private TextView tagName;
	
	/**
	 * 
	 * @param context
	 * @param c
	 */
	public TagAutoCompleteCursorAdapter(final Context context, final Cursor c) {
		super(context, c);
		this.context = context;
		this.nameIdx = c.getColumnIndexOrThrow(TagColumns.TAG_NAME);
		this.resolver = context.getContentResolver();
	}

	@Override
	public void bindView(final View parent, final Context context, final Cursor cursor) {
		this.tagName.setText(cursor.getString(this.nameIdx));
	}
	
	@Override
	public CharSequence convertToString(final Cursor cursor) {
		return cursor.getString(this.nameIdx);
	}

	@Override
	public View newView(final Context context, final Cursor cursor, final ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(context);
		
		this.tagName = (TextView) inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
		return this.tagName;
	}
	
	@Override
	public Cursor runQueryOnBackgroundThread(final CharSequence constraint) {
		if (!present(constraint)) {
			return super.runQueryOnBackgroundThread(constraint);
		}
		
		final String[] args = new String[] { constraint.toString().toLowerCase() + "*" };
		return this.resolver.query(ItemProvider.getTagsContentUri(this.context), null, TAG_SEARCH, args, Database.TAGS_DEFAULT_SORT_ORDER);
	}

}
