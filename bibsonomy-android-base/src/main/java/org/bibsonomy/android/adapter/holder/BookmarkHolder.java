package org.bibsonomy.android.adapter.holder;

import android.database.CharArrayBuffer;
import android.widget.TextView;

/**
 * Used as a container.
 * 
 * @author Alex Plischke
 * 
 */
public class BookmarkHolder extends ResourceHolder {
	public TextView title;
	public final CharArrayBuffer buffer = new CharArrayBuffer(64);
}