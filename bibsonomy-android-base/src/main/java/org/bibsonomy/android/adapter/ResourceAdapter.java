package org.bibsonomy.android.adapter;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.adapter.holder.ResourceHolder;
import org.bibsonomy.android.providers.database.Database.ResourceColumns;
import org.bibsonomy.android.utils.FileManager;
import org.bibsonomy.model.Resource;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.AbsListView;
import android.widget.CursorAdapter;

/**
 * 
 * @author dzo
 *
 * @param <RESOURCE>
 */
public abstract class ResourceAdapter<RESOURCE extends Resource> extends CursorAdapter {

	protected final PostShelfActivity activity;
	protected final Resources resources;
	protected final LayoutInflater inflater;
	protected final Drawable defaultDrawable;
	
	protected final int intraHashIndex;
	
	
	/**
	 * 
	 * @param activity
	 * @param c
	 * @param autoRequery
	 * @param defaultDrawable
	 */
	public ResourceAdapter(final PostShelfActivity activity, final Cursor c, final boolean autoRequery, final Drawable defaultDrawable) {
		super(activity, c, autoRequery);
		
		this.intraHashIndex = c.getColumnIndexOrThrow(ResourceColumns.INTRAHASH);
		this.resources = activity.getResources();
		
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.activity = activity;
		

		this.defaultDrawable = defaultDrawable;
	}


	/**
	 * @param intraHash
	 * @param holder
	 */
	protected void loadCover(final String intraHash, final ResourceHolder holder) {
		final PostShelfActivity activity = this.activity;
		if (activity.getScrollState() == AbsListView.OnScrollListener.SCROLL_STATE_FLING || activity.isPendingCoverUpdate()) {
			holder.image.setImageDrawable(this.defaultDrawable);
			holder.intraHash = intraHash;
			holder.queryCover = true;
		} else {
			Drawable cover = FileManager.getCover(this.resources, intraHash);
			if (cover == null) {
			    cover = this.defaultDrawable;
			}
			
			holder.image.setImageDrawable(cover);
			holder.queryCover = false;
		}
	}

}
