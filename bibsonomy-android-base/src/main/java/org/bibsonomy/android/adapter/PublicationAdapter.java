package org.bibsonomy.android.adapter;

import org.bibsonomy.android.activity.PostShelfActivity;
import org.bibsonomy.android.adapter.holder.PublicationHolder;
import org.bibsonomy.android.base.R;
import org.bibsonomy.android.providers.database.Database.PublicationColumns;
import org.bibsonomy.model.BibTex;

import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A {@link CursorAdapter} used to bind the actual data to the {@link View}s.
 * 
 * @author Alex Plischke
 * 
 */
public class PublicationAdapter extends ResourceAdapter<BibTex> {

    private final int titleIndex;
    private final int authorIndex;

    /**
     * @param context
     * @param c
     * @param defaultDrawable 
     */
    public PublicationAdapter(final PostShelfActivity context, final Cursor c, final Drawable defaultDrawable) {
		super(context, c, true, defaultDrawable);
		this.titleIndex = c.getColumnIndexOrThrow(PublicationColumns.CLEANED_TITLE);
		this.authorIndex = c.getColumnIndexOrThrow(PublicationColumns.AUTHOR);
    }

    @Override
    public View newView(final Context context, final Cursor cursor, final ViewGroup parent) {
		final ViewGroup layout = (ViewGroup) this.inflater.inflate(R.layout.shelf_item, parent, false);
		final PublicationHolder holder = new PublicationHolder();
		holder.title = (TextView) layout.findViewById(R.id.title);
		holder.authors = (TextView) layout.findViewById(R.id.author);
		holder.image = (ImageView) layout.findViewById(R.id.image);
	
		layout.setTag(holder);
	
		return layout;
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
		final PublicationHolder holder = (PublicationHolder) view.getTag();
	
		final CharArrayBuffer titleBuffer = holder.titleBuffer;
		cursor.copyStringToBuffer(this.titleIndex, titleBuffer);
		int size = titleBuffer.sizeCopied;
		if (size != 0) {
		    holder.title.setText(titleBuffer.data, 0, size);
		}
	
		final CharArrayBuffer authorBuffer = holder.authorBuffer;
		cursor.copyStringToBuffer(this.authorIndex, authorBuffer);
		size = authorBuffer.sizeCopied;
		if (size != 0) {
		    holder.authors.setText(authorBuffer.data, 0, size);
		}
	
		final String intraHash = cursor.getString(this.intraHashIndex);
		this.loadCover(intraHash, holder);
    }
}
